package app.transcriber.generation.context.creation.input;

import app.transcriber.api.model.FieldsMapperDefinition;
import app.transcriber.api.model.MappingOperatorDefinition;
import app.transcriber.api.model.TypesMapperDefinition;
import app.transcriber.generation.creation.input.FieldsMapperGenerationInputFactory;
import app.transcriber.generation.model.MappingOperatorsGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.function.Function;

@RunWith(MockitoJUnitRunner.class)
public class FieldsMapperGenerationInputFactoryTest {

    private FieldsMapperGenerationInputFactory fieldsMapperGenerationInputFactory;

    @Mock
    private Function<String, ClassAdapter> sourceTypeNameToTypeMapper;

    @Mock
    private Function<String, ClassAdapter> targetTypeNameToTypeMapper;

    @Mock
    private Function<MappingOperatorDefinition, MappingOperatorsGenerationInput> mappingOperatorsGenerationInputFactory;

    @Mock
    private TypesMapperDefinition typesMapperDefinition;

    @Mock
    private FieldsMapperDefinition fieldsMapperDefinition;

    @Before
    public void setup() {
        Mockito.when(sourceTypeNameToTypeMapper.andThen(Mockito.any())).thenCallRealMethod();
        Mockito.when(targetTypeNameToTypeMapper.andThen(Mockito.any())).thenCallRealMethod();

        Answer<ClassAdapter> mockAnswer = invocation -> ClassAdapter.of(Object.class);

        Mockito.when(sourceTypeNameToTypeMapper.apply(Mockito.any())).thenAnswer(mockAnswer);
        Mockito.when(targetTypeNameToTypeMapper.apply(Mockito.any())).thenAnswer(mockAnswer);

        fieldsMapperGenerationInputFactory = new FieldsMapperGenerationInputFactory(sourceTypeNameToTypeMapper, targetTypeNameToTypeMapper, mappingOperatorsGenerationInputFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void allArgsConstructor_onNullTargetTypeNameToTypeMapper_throwsIllegalArgumentException() {
        new FieldsMapperGenerationInputFactory(sourceTypeNameToTypeMapper, null, mappingOperatorsGenerationInputFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void allArgsConstructor_onNullMappingOperatorsGenerationInputFactory_throwsIllegalArgumentException() {
        new FieldsMapperGenerationInputFactory(sourceTypeNameToTypeMapper, targetTypeNameToTypeMapper, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onNullTypesMapperDefinition_throwsIllegalArgumentException() {
        fieldsMapperGenerationInputFactory.apply(null, fieldsMapperDefinition);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onNullFieldsMapperDefinition_throwsIllegalArgumentException() {
        fieldsMapperGenerationInputFactory.apply(typesMapperDefinition, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onNullSourceClassSource_throwsIllegalArgumentException() {
        Mockito.when(sourceTypeNameToTypeMapper.apply(Mockito.any())).thenReturn(null);
        fieldsMapperGenerationInputFactory.apply(typesMapperDefinition, fieldsMapperDefinition);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onNullTargetClassSource_throwsIllegalArgumentException() {
        Mockito.when(targetTypeNameToTypeMapper.apply(Mockito.any())).thenReturn(null);
        fieldsMapperGenerationInputFactory.apply(typesMapperDefinition, fieldsMapperDefinition);
    }
}