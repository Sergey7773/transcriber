package app.transcriber.generation.context.creation.input;

import app.transcriber.api.model.ConstantMappingDefinition;
import app.transcriber.api.model.TypesMapperDefinition;
import app.transcriber.generation.creation.input.ConstantMappingGenerationInputFactory;
import app.transcriber.generation.model.ConstantMappingGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import app.transcriber.generation.model.adapters.FieldAdapter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.function.Function;

@RunWith(MockitoJUnitRunner.class)
public class ConstantMappingGenerationInputFactoryTest {

    private ConstantMappingGenerationInputFactory constantMappingGenerationInputFactory;

    @Mock
    private Function<String, ClassAdapter> targetSourceNameToTypeMapper;

    @Mock
    private TypesMapperDefinition typesMapperDefinition;

    @Mock
    private ConstantMappingDefinition constantMappingDefinition;

    @Before
    public void setup() {
        constantMappingGenerationInputFactory = new ConstantMappingGenerationInputFactory(targetSourceNameToTypeMapper);
    }

    @Test(expected = IllegalArgumentException.class)
    public void allArgsConstructor_onNullTargetSourceNameToTypeMapper_throwsIllegalArgumentException() {
        new ConstantMappingGenerationInputFactory(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onNullTypesMapperDefinition_throwsIllegalArgumentException() {
        constantMappingGenerationInputFactory.apply(null, constantMappingDefinition);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onNullConstantMappingDefinition_throwsIllegalArgumentException() {
        constantMappingGenerationInputFactory.apply(typesMapperDefinition, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onMissingTargetJavaSource_throwIllegalArgumentException() {
        Mockito.when(targetSourceNameToTypeMapper.apply(Mockito.any())).thenReturn(null);
        constantMappingGenerationInputFactory.apply(typesMapperDefinition, constantMappingDefinition);
    }

    @Test
    public void apply_onValidInput_setsValueFromConstantMappingDefinition() {
        Object mockValue = Mockito.mock(Object.class);
        FieldAdapter fieldAdapter = Mockito.mock(FieldAdapter.class);
        setupValidInput(mockValue, fieldAdapter);

        ConstantMappingGenerationInput result = constantMappingGenerationInputFactory.apply(typesMapperDefinition, constantMappingDefinition);

        Assert.assertEquals(mockValue, result.getValue());
    }

    @Test
    public void apply_onValidInput_setsTheFieldReturnedByTheTargetType() {
        Object mockValue = Mockito.mock(Object.class);
        FieldAdapter fieldAdapter = Mockito.mock(FieldAdapter.class);
        setupValidInput(mockValue, fieldAdapter);

        ConstantMappingGenerationInput result = constantMappingGenerationInputFactory.apply(typesMapperDefinition, constantMappingDefinition);

        Assert.assertEquals(fieldAdapter, result.getTargetField());
    }

    private void setupValidInput(Object mockConstantValue, FieldAdapter mockField) {
        String targetFieldName = "targetFieldName";
        Mockito.when(constantMappingDefinition.getValue()).thenReturn(mockConstantValue);
        Mockito.when(constantMappingDefinition.getTargetFieldName()).thenReturn(targetFieldName);
        String targetClassName = "targetClassName";
        Mockito.when(typesMapperDefinition.getTargetClassName()).thenReturn(targetClassName);
        ClassAdapter targetClassAdapter = Mockito.mock(ClassAdapter.class);
        Mockito.when(targetSourceNameToTypeMapper.apply(targetClassName)).thenAnswer(invocation -> targetClassAdapter);
        Mockito.when(targetClassAdapter.getDeclaredField(targetFieldName)).thenReturn(mockField);
    }
}