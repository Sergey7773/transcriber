package app.transcriber.generation.context.creation.input;

import app.transcriber.api.model.ConstantMappingDefinition;
import app.transcriber.api.model.FieldsMapperDefinition;
import app.transcriber.api.model.FieldsValueProducerDefinition;
import app.transcriber.api.model.TypesMapperDefinition;
import app.transcriber.generation.creation.input.TypesMapperGenerationInputFactory;
import app.transcriber.generation.model.ConstantMappingGenerationInput;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.FieldsValueProducerGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class TypesMapperGenerationInputFactoryTest {

    private TypesMapperGenerationInputFactory typesMapperGenerationInputFactory;

    @Mock
    private Function<String, ClassAdapter> sourceTypeNameToTypeMapper;

    @Mock
    private Function<String, ClassAdapter> targetTypeNameToTypeMapper;

    @Mock
    private BiFunction<TypesMapperDefinition, FieldsMapperDefinition, FieldsMapperGenerationInput> fieldsMapperGenerationInputFactory;

    @Mock
    private BiFunction<TypesMapperDefinition, FieldsValueProducerDefinition, FieldsValueProducerGenerationInput> fieldsValueProducerGenerationInputFactory;

    @Mock
    private BiFunction<TypesMapperDefinition, ConstantMappingDefinition, ConstantMappingGenerationInput> constantMappingGenerationInputFactory;

    @Mock
    private TypesMapperDefinition typesMapperDefinition;

    @Before
    public void setup() {

        Answer<ClassAdapter> mockAnswer = invocation -> ClassAdapter.of(Object.class);

        Mockito.when(sourceTypeNameToTypeMapper.apply(Mockito.any())).thenAnswer(mockAnswer);
        Mockito.when(targetTypeNameToTypeMapper.apply(Mockito.any())).thenAnswer(mockAnswer);

        typesMapperGenerationInputFactory = new TypesMapperGenerationInputFactory(sourceTypeNameToTypeMapper, targetTypeNameToTypeMapper, fieldsMapperGenerationInputFactory, fieldsValueProducerGenerationInputFactory, constantMappingGenerationInputFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void allArgsConstructor_onNullSourceTypeNameToSourceMapper_throwsIllegalArgumentException() {
        new TypesMapperGenerationInputFactory(null, targetTypeNameToTypeMapper, fieldsMapperGenerationInputFactory, fieldsValueProducerGenerationInputFactory, constantMappingGenerationInputFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void allArgsConstructor_onNullTargetTypeNameToSourceMapper_throwsIllegalArgumentException() {
        new TypesMapperGenerationInputFactory(sourceTypeNameToTypeMapper, null, fieldsMapperGenerationInputFactory, fieldsValueProducerGenerationInputFactory, constantMappingGenerationInputFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void allArgsConstructor_onNullFieldsMapperGenerationInputFactory_throwsIllegalArgumentException() {
        new TypesMapperGenerationInputFactory(sourceTypeNameToTypeMapper, targetTypeNameToTypeMapper, null, fieldsValueProducerGenerationInputFactory, constantMappingGenerationInputFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void allArgsConstructor_onNullFieldsValueProducerGenerationInputFactory_throwsIllegalArgumentException() {
        new TypesMapperGenerationInputFactory(sourceTypeNameToTypeMapper, targetTypeNameToTypeMapper, fieldsMapperGenerationInputFactory, null, constantMappingGenerationInputFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void allArgsConstructor_onNullConstantMappingGenerationInputFactory_throwsIllegalArgumentException() {
        new TypesMapperGenerationInputFactory(sourceTypeNameToTypeMapper, targetTypeNameToTypeMapper, fieldsMapperGenerationInputFactory, fieldsValueProducerGenerationInputFactory, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onNullTypesMapperDefinition_throwsIllegalArgumentException() {
        typesMapperGenerationInputFactory.apply(null);
    }

    @Test
    public void apply_onExistingFieldsMappersGenerationInputList_applyFieldsMapperGenerationInputFactoryForEachFieldsMapperDefinition() {

        FieldsMapperGenerationInput mockFieldsMapperGenerationInput1 = Mockito.mock(FieldsMapperGenerationInput.class);
        FieldsMapperGenerationInput mockFieldsMapperGenerationInput2 = Mockito.mock(FieldsMapperGenerationInput.class);

        FieldsMapperDefinition mockFieldsMapperDefinition1 = Mockito.mock(FieldsMapperDefinition.class);
        FieldsMapperDefinition mockFieldsMapperDefinition2 = Mockito.mock(FieldsMapperDefinition.class);

        Mockito.when(fieldsMapperGenerationInputFactory.apply(typesMapperDefinition, mockFieldsMapperDefinition1)).thenReturn(mockFieldsMapperGenerationInput1);
        Mockito.when(fieldsMapperGenerationInputFactory.apply(typesMapperDefinition, mockFieldsMapperDefinition2)).thenReturn(mockFieldsMapperGenerationInput2);

        Mockito.when(typesMapperDefinition.getFieldMappersDefinitions()).thenReturn(Arrays.asList(mockFieldsMapperDefinition1, mockFieldsMapperDefinition2));


        TypesMapperGenerationInput result = typesMapperGenerationInputFactory.apply(typesMapperDefinition);

        Mockito.verify(fieldsMapperGenerationInputFactory, Mockito.times(1)).apply(typesMapperDefinition, mockFieldsMapperDefinition1);
        Mockito.verify(fieldsMapperGenerationInputFactory, Mockito.times(1)).apply(typesMapperDefinition, mockFieldsMapperDefinition2);

        assertEquals(Arrays.asList(mockFieldsMapperGenerationInput1, mockFieldsMapperGenerationInput2), result.getFieldsMappersGenerationInputs());

    }

    @Test
    public void apply_onExistingFieldsValueProducersGenerationInputList_applyFieldsValueProducerGenerationInputFactoryForEachFieldsValueProducerDefinition() {

        FieldsValueProducerGenerationInput mockFieldsValueProducerGenerationInput1 = Mockito.mock(FieldsValueProducerGenerationInput.class);
        FieldsValueProducerGenerationInput mockFieldsValueProducerGenerationInput2 = Mockito.mock(FieldsValueProducerGenerationInput.class);

        FieldsValueProducerDefinition mockFieldsValueProducerDefinition1 = Mockito.mock(FieldsValueProducerDefinition.class);
        FieldsValueProducerDefinition mockFieldsValueProducerDefinition2 = Mockito.mock(FieldsValueProducerDefinition.class);

        Mockito.when(fieldsValueProducerGenerationInputFactory.apply(typesMapperDefinition, mockFieldsValueProducerDefinition1)).thenReturn(mockFieldsValueProducerGenerationInput1);
        Mockito.when(fieldsValueProducerGenerationInputFactory.apply(typesMapperDefinition, mockFieldsValueProducerDefinition2)).thenReturn(mockFieldsValueProducerGenerationInput2);

        Mockito.when(typesMapperDefinition.getFieldsValueProducerDefinitions()).thenReturn(Arrays.asList(mockFieldsValueProducerDefinition1, mockFieldsValueProducerDefinition2));

        TypesMapperGenerationInput result = typesMapperGenerationInputFactory.apply(typesMapperDefinition);

        Mockito.verify(fieldsValueProducerGenerationInputFactory, Mockito.times(1)).apply(typesMapperDefinition, mockFieldsValueProducerDefinition1);
        Mockito.verify(fieldsValueProducerGenerationInputFactory, Mockito.times(1)).apply(typesMapperDefinition, mockFieldsValueProducerDefinition2);

        assertEquals(Arrays.asList(mockFieldsValueProducerGenerationInput1, mockFieldsValueProducerGenerationInput2), result.getFieldsValueProducersGenerationInputs());

    }

    @Test
    public void apply_onExistingConstantMappingsGenerationInputList_applyConstantMappingGenerationInputFactoryForEachConstantMappingDefinition() {
        {

            ConstantMappingGenerationInput mockConstantMappingGenerationInput1 = Mockito.mock(ConstantMappingGenerationInput.class);
            ConstantMappingGenerationInput mockConstantMappingGenerationInput2 = Mockito.mock(ConstantMappingGenerationInput.class);

            ConstantMappingDefinition mockConstantMappingDefinition1 = Mockito.mock(ConstantMappingDefinition.class);
            ConstantMappingDefinition mockConstantMappingDefinition2 = Mockito.mock(ConstantMappingDefinition.class);

            Mockito.when(constantMappingGenerationInputFactory.apply(typesMapperDefinition, mockConstantMappingDefinition1)).thenReturn(mockConstantMappingGenerationInput1);
            Mockito.when(constantMappingGenerationInputFactory.apply(typesMapperDefinition, mockConstantMappingDefinition2)).thenReturn(mockConstantMappingGenerationInput2);

            Mockito.when(typesMapperDefinition.getConstantMappingMetadata()).thenReturn(Arrays.asList(mockConstantMappingDefinition1, mockConstantMappingDefinition2));

            TypesMapperGenerationInput result = typesMapperGenerationInputFactory.apply(typesMapperDefinition);

            Mockito.verify(constantMappingGenerationInputFactory, Mockito.times(1)).apply(typesMapperDefinition, mockConstantMappingDefinition1);
            Mockito.verify(constantMappingGenerationInputFactory, Mockito.times(1)).apply(typesMapperDefinition, mockConstantMappingDefinition2);

            assertEquals(Arrays.asList(mockConstantMappingGenerationInput1, mockConstantMappingGenerationInput2), result.getConstantMappingsGenerationInputs());
        }
    }
}