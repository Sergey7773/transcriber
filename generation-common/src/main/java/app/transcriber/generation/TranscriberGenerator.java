package app.transcriber.generation;

import app.transcriber.generation.model.MapperGenerationContext;
import com.squareup.javapoet.JavaFile;

import java.util.List;

public interface TranscriberGenerator {

    List<JavaFile> generate(MapperGenerationContext generationContext);

}
