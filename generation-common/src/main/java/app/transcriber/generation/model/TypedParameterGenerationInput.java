package app.transcriber.generation.model;

import app.transcriber.generation.model.adapters.ClassAdapter;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class TypedParameterGenerationInput {

    private final ClassAdapter parameterType;
    private final Object parameterValue;

}
