package app.transcriber.generation.model;

import app.transcriber.generation.model.adapters.ClassAdapter;
import lombok.Builder;
import lombok.Getter;

import java.util.Collection;
import java.util.Map;

@Builder
@Getter
public class MapperGenerationContext {

    private String outputPackage;
    private Map<String, ClassAdapter> sourceClasses;
    private Map<String, ClassAdapter> targetClasses;
    private Collection<TypesMapperGenerationInput> typesMapperGenerationInputs;

    private NamingConventionsContext namingConventionsContext;

}
