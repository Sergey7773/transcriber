package app.transcriber.generation.model.adapters;

import app.transcriber.generation.creation.exception.NoFieldFoundException;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.lang.reflect.Method;
import java.lang.reflect.Type;

@AllArgsConstructor(staticName = "of", access = AccessLevel.PUBLIC)
@EqualsAndHashCode
public class ClassAdapter {

    @NonNull
    private final Class<?> clz;

    public String getName() {
        return clz.getName();
    }

    public String getSimpleName() {
        return clz.getSimpleName();
    }

    public String getTypeName() {
        return clz.getTypeName();
    }

    public Type getWrappedType() {
        return clz;
    }

    public FieldAdapter getDeclaredField(String name) {
        try {
            return new FieldAdapter(clz.getDeclaredField(name));
        } catch (NoSuchFieldException e) {
            throw new NoFieldFoundException(e);
        }
    }

    public Method[] getMethods() {
        return clz.getMethods();
    }

    public int getTypeParametersSize() {
        return clz.getTypeParameters().length;
    }

    public boolean isAssignableFrom(ClassAdapter classAdapter) {
        return this.clz.isAssignableFrom(classAdapter.clz);
    }
}
