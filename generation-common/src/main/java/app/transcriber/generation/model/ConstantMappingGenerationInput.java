package app.transcriber.generation.model;

import app.transcriber.generation.model.adapters.FieldAdapter;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ConstantMappingGenerationInput {

    private FieldAdapter targetField;
    private Object value;

}
