package app.transcriber.generation.model;

import app.transcriber.generation.model.adapters.ClassAdapter;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class MappingOperatorsGenerationInput {

    private final String operatorName;
    private final List<TypedParameterGenerationInput> creationParametersGenerationInputs;
    private final ClassAdapter mapperSourceType;
    private final ClassAdapter mapperTargetType;
}
