package app.transcriber.generation.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class NamingConventionsContext {

    private final Map<Class<?>, Object> conventionsMap = new ConcurrentHashMap<>();

    public <T> NamingConventionsContext addConvention(T convention) {
        conventionsMap.put(convention.getClass(), convention);
        return this;
    }

    public <T> T getConvention(Class<T> convention) {
        return (T) conventionsMap.get(convention);
    }
}
