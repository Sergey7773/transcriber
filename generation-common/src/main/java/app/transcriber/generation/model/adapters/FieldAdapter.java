package app.transcriber.generation.model.adapters;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.lang.reflect.Field;
import java.lang.reflect.Type;

/**
 * Represents the information about a field of a class, which is required by the generation process.
 * The main purpose of this class is to decouple the generation process from any underlying data structures and models, <br>
 * and make it rely only on the {@link app.transcriber.generation.model} package for the code creation.
 */
@AllArgsConstructor
@EqualsAndHashCode
public class FieldAdapter {

    @NonNull
    private final Field field;

    public String getName() {
        return field.getName();
    }

    public ClassAdapter getType() {
        return ClassAdapter.of(field.getType());
    }

    public ClassAdapter getDeclaringClass() {
        return ClassAdapter.of(field.getDeclaringClass());
    }

    public Type getGenericType() {
        return field.getGenericType();
    }
}
