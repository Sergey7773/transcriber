package app.transcriber.generation.model;

import app.transcriber.generation.model.adapters.FieldAdapter;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class FieldsValueProducerGenerationInput {

    private FieldAdapter targetField;
    private String valueProducerName;
    private List<TypedParameterGenerationInput> creationParametersGenerationInputs;
}
