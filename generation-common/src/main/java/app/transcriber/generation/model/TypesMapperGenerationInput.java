package app.transcriber.generation.model;

import app.transcriber.generation.model.adapters.ClassAdapter;
import lombok.Builder;
import lombok.Getter;

import java.util.Collection;

@Builder
@Getter
public class TypesMapperGenerationInput {

    private final ClassAdapter sourceClass;
    private final ClassAdapter targetClass;
    private final Collection<FieldsMapperGenerationInput> fieldsMappersGenerationInputs;
    private final Collection<FieldsValueProducerGenerationInput> fieldsValueProducersGenerationInputs;
    private final Collection<ConstantMappingGenerationInput> constantMappingsGenerationInputs;

}
