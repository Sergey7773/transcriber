package app.transcriber.generation.model;

import app.transcriber.generation.model.adapters.FieldAdapter;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class FieldsMapperGenerationInput {

    private List<FieldAdapter> sourceFields;
    private FieldAdapter targetField;
    private List<MappingOperatorsGenerationInput> mappingOperatorsGenerationInputs;
}
