package app.transcriber.generation.execution;

import app.transcriber.api.exceptions.GenerationInputVerificationException;
import app.transcriber.api.model.GenerationProcessPackages;
import app.transcriber.api.model.TypesMapperDefinition;
import app.transcriber.generation.TranscriberGenerator;
import app.transcriber.generation.creation.MapperGenerationContextFactory;
import app.transcriber.generation.model.MapperGenerationContext;
import com.squareup.javapoet.JavaFile;
import lombok.Builder;
import lombok.NonNull;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

@Builder
public class TranscriberGenerationProcess {

    @NonNull
    private final GenerationProcessPackages generationProcessPackages;

    @NonNull
    private final MapperGenerationContextFactory mapperGenerationContextFactory;

    @NonNull
    private final TranscriberGenerator mapperGenerator;

    public Collection<JavaFile> executeGeneration(Collection<TypesMapperDefinition> typesMapperDefinitions) throws GenerationInputVerificationException {

        if (Objects.isNull(typesMapperDefinitions) || typesMapperDefinitions.isEmpty()) {
            return Collections.emptyList();
        }

        MapperGenerationContext context = mapperGenerationContextFactory.create(typesMapperDefinitions, generationProcessPackages);

        return mapperGenerator.generate(context);
    }
}
