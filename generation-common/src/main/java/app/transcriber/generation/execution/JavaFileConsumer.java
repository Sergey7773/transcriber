package app.transcriber.generation.execution;

import com.squareup.javapoet.JavaFile;

import java.util.function.Consumer;

public interface JavaFileConsumer extends Consumer<JavaFile> {
}
