package app.transcriber.generation.execution;

import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.io.File;
import java.util.function.UnaryOperator;

@AllArgsConstructor
public class OutputFilePathCalculator implements UnaryOperator<String> {

    @NonNull
    private final String mappersOutputDirectory;

    @Override
    public String apply(String javaTypeSpecName) {
        return mappersOutputDirectory + File.separator + javaTypeSpecName + ".java";
    }
}
