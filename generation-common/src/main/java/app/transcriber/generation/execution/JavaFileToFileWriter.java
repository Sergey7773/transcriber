package app.transcriber.generation.execution;

import com.squareup.javapoet.JavaFile;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@AllArgsConstructor
@Slf4j
public class JavaFileToFileWriter implements JavaFileConsumer {

    @NonNull
    private OutputFilePathCalculator outputFilePathCalculator;

    @Override
    public void accept(JavaFile javaFile) {
        try {
            String outputFilepath = outputFilePathCalculator.apply(javaFile.typeSpec.name);
            try (FileWriter writer = new FileWriter(new File(outputFilepath))) {
                writer.write(javaFile.toString());
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }
}
