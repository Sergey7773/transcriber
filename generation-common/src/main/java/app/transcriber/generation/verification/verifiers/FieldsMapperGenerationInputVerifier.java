package app.transcriber.generation.verification.verifiers;

import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MappingOperatorsGenerationInput;
import app.transcriber.api.model.verification.FieldsMapperGenerationInputVerificationError;
import app.transcriber.api.model.verification.MappingOperatorsGenerationInputVerificationError;
import app.transcriber.tools.OptionalMapperCollector;
import lombok.AllArgsConstructor;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;
import java.util.function.Function;

@AllArgsConstructor
public class FieldsMapperGenerationInputVerifier implements Function<FieldsMapperGenerationInput, Optional<FieldsMapperGenerationInputVerificationError>> {

    private Function<MappingOperatorsGenerationInput, Optional<MappingOperatorsGenerationInputVerificationError>> mappingOperatorsVerifier;

    @Override
    public Optional<FieldsMapperGenerationInputVerificationError> apply(FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        Collection<String> messages = verifyFieldsMapperGenerationInputFields(fieldsMapperGenerationInput);

        Collection<MappingOperatorsGenerationInputVerificationError> mappingOperatorsGenerationInputVerificationErrors =
                OptionalMapperCollector.mapCollection(fieldsMapperGenerationInput.getMappingOperatorsGenerationInputs(), mappingOperatorsVerifier);

        if (!messages.isEmpty() || !mappingOperatorsGenerationInputVerificationErrors.isEmpty()) {
            return Optional.of(FieldsMapperGenerationInputVerificationError.builder()
                    .mappingOperatorsVerificationErrors(mappingOperatorsGenerationInputVerificationErrors)
                    .messages(messages)
                    .build());
        }

        return Optional.empty();
    }

    private Collection<String> verifyFieldsMapperGenerationInputFields(FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        Collection<String> result = new LinkedList<>();

        if (fieldsMapperGenerationInput.getSourceFields() == null ||
                fieldsMapperGenerationInput.getSourceFields().isEmpty()) {
            result.add("Source fields list must contain at least one element");
        }

        if (fieldsMapperGenerationInput.getTargetField() == null) {
            result.add("Target field must be non null");
        }

        if (fieldsMapperGenerationInput.getMappingOperatorsGenerationInputs() == null) {
            result.add("Mapping operators must be a non null collection");
        }

        return result;
    }
}
