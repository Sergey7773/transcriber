package app.transcriber.generation.verification.verifiers;

import app.transcriber.generation.model.MappingOperatorsGenerationInput;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.api.model.verification.MappingOperatorsGenerationInputVerificationError;
import app.transcriber.api.model.verification.TypedParameterGenerationInputVerificationError;
import app.transcriber.tools.OptionalMapperCollector;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;
import java.util.function.Function;

@AllArgsConstructor
public class MappingOperatorsGenerationInputVerifier implements Function<MappingOperatorsGenerationInput, Optional<MappingOperatorsGenerationInputVerificationError>> {

    private Function<TypedParameterGenerationInput, Optional<TypedParameterGenerationInputVerificationError>> typedParameterVerifier;

    @Override
    public Optional<MappingOperatorsGenerationInputVerificationError> apply(MappingOperatorsGenerationInput mappingOperatorsGenerationInput) {
        Collection<String> messages = verifyMappingOperatorGenerationInputFields(mappingOperatorsGenerationInput);

        Collection<TypedParameterGenerationInputVerificationError> typedParameterGenerationInputVerificationErrors =
                OptionalMapperCollector.mapCollection(mappingOperatorsGenerationInput.getCreationParametersGenerationInputs(), typedParameterVerifier);

        if(!messages.isEmpty() || !typedParameterGenerationInputVerificationErrors.isEmpty()) {
            return Optional.of(MappingOperatorsGenerationInputVerificationError.builder()
                    .messages(messages)
                    .typedParameterGenerationInputVerificationErrors(typedParameterGenerationInputVerificationErrors)
                    .build());
        }

        return Optional.empty();
    }

    private Collection<String> verifyMappingOperatorGenerationInputFields(MappingOperatorsGenerationInput mappingOperatorsGenerationInput) {
        Collection<String> result = new LinkedList<>();

        if(mappingOperatorsGenerationInput.getMapperSourceType() == null) {
            result.add("Source type must be non null");
        }

        if(mappingOperatorsGenerationInput.getMapperTargetType() == null) {
            result.add("Target type must be non null");
        }

        if(mappingOperatorsGenerationInput.getCreationParametersGenerationInputs() == null) {
            result.add("Creation parameters must be a non null list");
        }

        if(StringUtils.isEmpty(mappingOperatorsGenerationInput.getOperatorName())) {
            result.add("Operator name must be a non empty string");
        }

        return result;
    }
}
