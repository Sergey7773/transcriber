package app.transcriber.generation.verification.verifiers;

import app.transcriber.generation.model.FieldsValueProducerGenerationInput;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.api.model.verification.FieldsValueProducerGenerationInputVerificationError;
import app.transcriber.api.model.verification.TypedParameterGenerationInputVerificationError;
import app.transcriber.tools.OptionalMapperCollector;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;
import java.util.function.Function;

@AllArgsConstructor
public class FieldsValueProducerGenerationInputVerifier implements Function<FieldsValueProducerGenerationInput, Optional<FieldsValueProducerGenerationInputVerificationError>> {

    private Function<TypedParameterGenerationInput, Optional<TypedParameterGenerationInputVerificationError>> typedParameterVerifier;

    @Override
    public Optional<FieldsValueProducerGenerationInputVerificationError> apply(FieldsValueProducerGenerationInput fieldsValueProducerGenerationInput) {
        Collection<String> messages = verifyFieldValueProducerFields(fieldsValueProducerGenerationInput);

        Collection<TypedParameterGenerationInputVerificationError> typedParameterGenerationInputVerificationErrors =
                OptionalMapperCollector.mapCollection(fieldsValueProducerGenerationInput.getCreationParametersGenerationInputs(), typedParameterVerifier);

        if (!messages.isEmpty() || !typedParameterGenerationInputVerificationErrors.isEmpty()) {
            return Optional.of(FieldsValueProducerGenerationInputVerificationError.builder()
                    .message(messages)
                    .typedParameterGenerationInputVerificationErrors(typedParameterGenerationInputVerificationErrors)
                    .build());
        }

        return Optional.empty();
    }

    private Collection<String> verifyFieldValueProducerFields(FieldsValueProducerGenerationInput fieldsValueProducerGenerationInput) {
        Collection<String> result = new LinkedList<>();

        if (fieldsValueProducerGenerationInput.getTargetField() == null) {
            result.add("Target field must be non null");
        }

        if (StringUtils.isEmpty(fieldsValueProducerGenerationInput.getValueProducerName())) {
            result.add("Producer name must be a non empty string");
        }

        if (fieldsValueProducerGenerationInput.getCreationParametersGenerationInputs() == null) {
            result.add("Creation parameters list must be non null");
        }

        return result;
    }
}
