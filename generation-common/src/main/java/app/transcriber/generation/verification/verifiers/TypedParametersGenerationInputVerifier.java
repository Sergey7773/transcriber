package app.transcriber.generation.verification.verifiers;

import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.api.model.verification.TypedParameterGenerationInputVerificationError;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;
import java.util.function.Function;

public class TypedParametersGenerationInputVerifier implements Function<TypedParameterGenerationInput, Optional<TypedParameterGenerationInputVerificationError>> {
    @Override
    public Optional<TypedParameterGenerationInputVerificationError> apply(TypedParameterGenerationInput typedParameterGenerationInput) {
        Collection<String> messages = verifyTypedParameterFields(typedParameterGenerationInput);

        if (!messages.isEmpty()) {
            return Optional.of(TypedParameterGenerationInputVerificationError.builder()
                    .messages(messages).build());
        }

        return Optional.empty();
    }

    private Collection<String> verifyTypedParameterFields(TypedParameterGenerationInput typedParameterGenerationInput) {
        Collection<String> result = new LinkedList<>();

        if (typedParameterGenerationInput.getParameterType() == null) {
            result.add("Parameter type must be non null");
        }

        return result;
    }
}
