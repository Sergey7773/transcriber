package app.transcriber.generation.verification.verifiers;

import app.transcriber.generation.model.ConstantMappingGenerationInput;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.FieldsValueProducerGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.api.model.verification.ConstantMappingGenerationInputVerificationError;
import app.transcriber.api.model.verification.FieldsMapperGenerationInputVerificationError;
import app.transcriber.api.model.verification.FieldsValueProducerGenerationInputVerificationError;
import app.transcriber.api.model.verification.TypesMapperGenerationInputVerificationError;
import app.transcriber.tools.OptionalMapperCollector;
import lombok.AllArgsConstructor;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;
import java.util.function.Function;


@AllArgsConstructor
public class TypesMapperGenerationInputVerifier implements Function<TypesMapperGenerationInput, Optional<TypesMapperGenerationInputVerificationError>> {

    private Function<FieldsMapperGenerationInput, Optional<FieldsMapperGenerationInputVerificationError>> fieldsMapperVerifier;

    private Function<FieldsValueProducerGenerationInput, Optional<FieldsValueProducerGenerationInputVerificationError>> fieldsValueProducerVerifier;

    private Function<ConstantMappingGenerationInput, Optional<ConstantMappingGenerationInputVerificationError>> constantMappingVerifier;

    @Override
    public Optional<TypesMapperGenerationInputVerificationError> apply(TypesMapperGenerationInput typesMapperGenerationInput) {

        Collection<String> messages = verifyOwnFields(typesMapperGenerationInput);

        Collection<FieldsMapperGenerationInputVerificationError> fieldsMappersVerificationErrors =
                OptionalMapperCollector.mapCollection(typesMapperGenerationInput.getFieldsMappersGenerationInputs(), fieldsMapperVerifier);

        Collection<FieldsValueProducerGenerationInputVerificationError> fieldsValueProducerVerificationErrors =
                OptionalMapperCollector.mapCollection(typesMapperGenerationInput.getFieldsValueProducersGenerationInputs(), fieldsValueProducerVerifier);

        Collection<ConstantMappingGenerationInputVerificationError> constantMappingVerificationErrors =
                OptionalMapperCollector.mapCollection(typesMapperGenerationInput.getConstantMappingsGenerationInputs(), constantMappingVerifier);

        if (!messages.isEmpty() || !fieldsMappersVerificationErrors.isEmpty() ||
                !fieldsValueProducerVerificationErrors.isEmpty() || !constantMappingVerificationErrors.isEmpty()) {
            TypesMapperGenerationInputVerificationError error = TypesMapperGenerationInputVerificationError.builder()
                    .messages(messages)
                    .fieldValueProducersVerificationErrors(fieldsValueProducerVerificationErrors)
                    .fieldMappersVerificationErrors(fieldsMappersVerificationErrors)
                    .constantMappingVerificationErrors(constantMappingVerificationErrors)
                    .build();
            return Optional.of(error);
        }

        return Optional.empty();

    }

    private Collection<String> verifyOwnFields(TypesMapperGenerationInput typesMapperGenerationInput) {
        Collection<String> messages = new LinkedList<>();

        if (typesMapperGenerationInput.getSourceClass() == null) {
            messages.add("Source class must be non null");
        }

        if (typesMapperGenerationInput.getTargetClass() == null) {
            messages.add("Target class must be non null");
        }

        return messages;
    }

}
