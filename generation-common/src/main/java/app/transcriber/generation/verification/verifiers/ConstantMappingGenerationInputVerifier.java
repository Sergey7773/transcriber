package app.transcriber.generation.verification.verifiers;

import app.transcriber.generation.model.ConstantMappingGenerationInput;
import app.transcriber.api.model.verification.ConstantMappingGenerationInputVerificationError;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;
import java.util.function.Function;

public class ConstantMappingGenerationInputVerifier implements Function<ConstantMappingGenerationInput, Optional<ConstantMappingGenerationInputVerificationError>> {
    @Override
    public Optional<ConstantMappingGenerationInputVerificationError> apply(ConstantMappingGenerationInput constantMappingGenerationInput) {
        Collection<String> messages = new LinkedList<>();

        if (constantMappingGenerationInput.getTargetField() == null) {
            messages.add("Target field must be non null");
        }

        if (!messages.isEmpty()) {
            return Optional.of(ConstantMappingGenerationInputVerificationError.builder().messages(messages).build());
        }

        return Optional.empty();
    }
}
