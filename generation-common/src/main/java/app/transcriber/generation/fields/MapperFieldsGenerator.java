package app.transcriber.generation.fields;

import app.transcriber.api.interfaces.*;
import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.tools.ListAccessUtils;
import com.squareup.javapoet.FieldSpec;

import javax.lang.model.element.Modifier;
import java.lang.annotation.Annotation;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class MapperFieldsGenerator {

    public List<FieldSpec> buildFieldsSpecForClassFields(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, Collection<Class<? extends Annotation>> memberAnnotations) {
        List<FieldSpec.Builder> fieldSpecBuilders = new LinkedList<>();

        FieldSpec.Builder targetModelObjectFactoryFieldBuilder = FieldSpec.builder(TargetModelObjectFactory.class, NamingConvention.TARGET_MODEL_OBJECT_FACTORY_MEMBER.toString(), Modifier.PRIVATE)
                .addJavadoc("Used to create instances of the target model objects");

        fieldSpecBuilders.add(targetModelObjectFactoryFieldBuilder);

        if (isMapperFactoryMemberRequired(typesMapperGenerationInput)) {
            FieldSpec.Builder mapperFactoryFieldSpecBuilder = FieldSpec.builder(MapperFactory.class, NamingConvention.MAPPER_FACTORY_MEMBER.toString(), Modifier.PRIVATE)

                    .addJavadoc("Used to create required {@code $T} instances in <i>map</i> methods which convert sub-entity types", Mapper.class);
            fieldSpecBuilders.add(mapperFactoryFieldSpecBuilder);
        }

        if (isMappingOperatorFactoryMemberRequired(mapperGenerationContext, typesMapperGenerationInput)) {
            FieldSpec.Builder mappingOperatorFactoryFieldSpecBuilder = FieldSpec.builder(MappingOperatorFactory.class, NamingConvention.MAPPING_OPERATOR_FACTORY_MEMBER.toString(), Modifier.PRIVATE)

                    .addJavadoc("Used to create required {@code $T} instances in <i>map</i> methods which use mapping operators", Function.class);
            fieldSpecBuilders.add(mappingOperatorFactoryFieldSpecBuilder);
        }

        if (isValueProducerFactoryMemberRequired(typesMapperGenerationInput)) {
            FieldSpec.Builder valueProducerFactoryFieldSpecBuilder = FieldSpec.builder(ValueProducerFactory.class, NamingConvention.VALUE_PRODUCER_FACTORY_MEMBER.toString(), Modifier.PRIVATE)

                    .addJavadoc("Used to create required {@code $T} instances in <i>produce</i> methods", Supplier.class);
            fieldSpecBuilders.add(valueProducerFactoryFieldSpecBuilder);
        }

        Optional.ofNullable(memberAnnotations)
                .orElse(Collections.emptyList())
                .forEach(fieldAnnotation ->
                        fieldSpecBuilders.forEach(builder -> builder.addAnnotation(fieldAnnotation)));

        return fieldSpecBuilders.stream().map(FieldSpec.Builder::build).collect(Collectors.toList());

    }

    private boolean isMapperFactoryMemberRequired(TypesMapperGenerationInput typesMapperGenerationInput) {
        return typesMapperGenerationInput.getFieldsMappersGenerationInputs().stream()
                .map(FieldsMapperGenerationInput::getMappingOperatorsGenerationInputs)
                .filter(Objects::nonNull)
                .anyMatch(mappingOperatorDefinitions -> !mappingOperatorDefinitions.isEmpty());
    }

    private boolean isMappingOperatorFactoryMemberRequired(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        return typesMapperGenerationInput.getFieldsMappersGenerationInputs()
                .stream()
                .map(fieldsMapperGenerationInput -> ListAccessUtils.getLastElementOf(fieldsMapperGenerationInput.getSourceFields()))
                .filter(Objects::nonNull)
                .anyMatch(field -> mapperGenerationContext.getSourceClasses().containsKey(field.getType().getSimpleName()));
    }

    private boolean isValueProducerFactoryMemberRequired(TypesMapperGenerationInput typesMapperGenerationInput) {
        return !typesMapperGenerationInput.getFieldsMappersGenerationInputs().isEmpty();
    }
}
