package app.transcriber.generation.creation.input;

import app.transcriber.api.model.FieldsValueProducerDefinition;
import app.transcriber.api.model.TypedParameterDefinition;
import app.transcriber.api.model.TypesMapperDefinition;
import app.transcriber.generation.model.FieldsValueProducerGenerationInput;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import lombok.AllArgsConstructor;

import java.util.Collections;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

@AllArgsConstructor
public class FieldsValueProducerGenerationInputFactory implements BiFunction<TypesMapperDefinition, FieldsValueProducerDefinition, FieldsValueProducerGenerationInput> {

    private Function<String, ClassAdapter> targetTypeNameToTypeMapper;

    private Function<TypedParameterDefinition, TypedParameterGenerationInput> typedParameterGeerationInputFactory;

    @Override
    public FieldsValueProducerGenerationInput apply(TypesMapperDefinition typesMapperDefinition, FieldsValueProducerDefinition fieldsValueProducerDefinition) {
        ClassAdapter targetFieldType = targetTypeNameToTypeMapper.apply(typesMapperDefinition.getTargetClassName());

        return FieldsValueProducerGenerationInput.builder()
                .targetField(targetFieldType.getDeclaredField(fieldsValueProducerDefinition.getTargetFieldName()))
                .valueProducerName(fieldsValueProducerDefinition.getValueProducerName())
                .creationParametersGenerationInputs(
                        Optional.ofNullable(fieldsValueProducerDefinition.getCreationParameters())
                                .orElse(Collections.emptyList())
                                .stream()
                                .map(typedParameterGeerationInputFactory)
                                .collect(Collectors.toList()))
                .build();
    }
}
