package app.transcriber.generation.creation.exception;

public class NoFieldFoundException extends RuntimeException {

    public NoFieldFoundException(Exception cause) {
        super(cause);
    }

}
