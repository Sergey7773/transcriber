package app.transcriber.generation.creation.input;

import app.transcriber.api.model.ConstantMappingDefinition;
import app.transcriber.api.model.FieldsMapperDefinition;
import app.transcriber.api.model.FieldsValueProducerDefinition;
import app.transcriber.api.model.TypesMapperDefinition;
import app.transcriber.generation.model.ConstantMappingGenerationInput;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.FieldsValueProducerGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

@AllArgsConstructor
public class TypesMapperGenerationInputFactory implements Function<TypesMapperDefinition, TypesMapperGenerationInput> {

    @NonNull
    private Function<String, ClassAdapter> sourceTypeNameToTypeMapper;

    @NonNull
    private Function<String, ClassAdapter> targetTypeNameToTypeMapper;

    @NonNull
    private BiFunction<TypesMapperDefinition, FieldsMapperDefinition, FieldsMapperGenerationInput> fieldsMapperGenerationInputFactory;

    @NonNull
    private BiFunction<TypesMapperDefinition, FieldsValueProducerDefinition, FieldsValueProducerGenerationInput> fieldsValueProducerGenerationInputFactory;

    @NonNull
    private BiFunction<TypesMapperDefinition, ConstantMappingDefinition, ConstantMappingGenerationInput> constantMappingGenerationInputFactory;

    @Override
    public TypesMapperGenerationInput apply(@NonNull TypesMapperDefinition typesMapperDefinition) {
        ClassAdapter sourceJavaType = sourceTypeNameToTypeMapper.apply(typesMapperDefinition.getSourceClassName());
        ClassAdapter targetJavaType = targetTypeNameToTypeMapper.apply(typesMapperDefinition.getTargetClassName());

        Collection<FieldsMapperGenerationInput> fieldsMapperGenerationInputCollection =
                Optional.ofNullable(typesMapperDefinition.getFieldMappersDefinitions()).orElse(Collections.emptyList())
                        .stream()
                        .map(fieldsMapperDefinition -> fieldsMapperGenerationInputFactory.apply(typesMapperDefinition, fieldsMapperDefinition))
                        .collect(Collectors.toList());

        Collection<FieldsValueProducerGenerationInput> fieldsValueProducerGenerationInputCollection =
                Optional.ofNullable(typesMapperDefinition.getFieldsValueProducerDefinitions()).orElse(Collections.emptyList())
                        .stream()
                        .map(fieldsValueProducerDefinition -> fieldsValueProducerGenerationInputFactory.apply(typesMapperDefinition, fieldsValueProducerDefinition))
                        .collect(Collectors.toList());

        Collection<ConstantMappingGenerationInput> constantMappingGenerationInputCollection =
                Optional.ofNullable(typesMapperDefinition.getConstantMappingMetadata()).orElse(Collections.emptyList())
                        .stream()
                        .map(constantMappingDefinition -> constantMappingGenerationInputFactory.apply(typesMapperDefinition, constantMappingDefinition))
                        .collect(Collectors.toList());

        return TypesMapperGenerationInput.builder()
                .sourceClass(sourceJavaType)
                .targetClass(targetJavaType)
                .fieldsMappersGenerationInputs(fieldsMapperGenerationInputCollection)
                .fieldsValueProducersGenerationInputs(fieldsValueProducerGenerationInputCollection)
                .constantMappingsGenerationInputs(constantMappingGenerationInputCollection)
                .build();
    }

}
