package app.transcriber.generation.creation.input;

import app.transcriber.api.model.FieldsMapperDefinition;
import app.transcriber.api.model.MappingOperatorDefinition;
import app.transcriber.api.model.TypesMapperDefinition;
import app.transcriber.generation.creation.exception.NoClassSourceFoundException;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MappingOperatorsGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import app.transcriber.generation.model.adapters.FieldAdapter;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

@AllArgsConstructor
public class FieldsMapperGenerationInputFactory implements BiFunction<TypesMapperDefinition, FieldsMapperDefinition, FieldsMapperGenerationInput> {

    @NonNull
    private Function<String, ClassAdapter> sourceTypeNameToTypeMapper;

    @NonNull
    private Function<String, ClassAdapter> targetTypeNameToTypeMapper;

    @NonNull
    private Function<MappingOperatorDefinition, MappingOperatorsGenerationInput> mappingOperatorsGenerationInputFactory;

    @Override
    public FieldsMapperGenerationInput apply(@NonNull TypesMapperDefinition typesMapperDefinition, @NonNull FieldsMapperDefinition fieldsMapperDefinition) {

        ClassAdapter sourceType = getClassSource(sourceTypeNameToTypeMapper, typesMapperDefinition.getSourceClassName());
        ClassAdapter targetType = getClassSource(targetTypeNameToTypeMapper, typesMapperDefinition.getTargetClassName());

        List<MappingOperatorsGenerationInput> mappingOperatorsGenerationInputList =
                Optional.ofNullable(fieldsMapperDefinition.getMappingOperators()).orElse(Collections.emptyList())
                        .stream()
                        .map(mappingOperatorsGenerationInputFactory::apply)
                        .collect(Collectors.toList());

        return FieldsMapperGenerationInput.builder()
                .sourceFields(getFieldSources(fieldsMapperDefinition.getSourceFieldNames(), sourceType))
                .targetField(targetType.getDeclaredField(fieldsMapperDefinition.getTargetFieldName()))
                .mappingOperatorsGenerationInputs(mappingOperatorsGenerationInputList)
                .build();
    }

    private ClassAdapter getClassSource(Function<String, ClassAdapter> nameToSourceMapper, String className) {
        return nameToSourceMapper.andThen(Optional::ofNullable)
                .apply(className)
                .orElseThrow(() -> new NoClassSourceFoundException(className));
    }

    private List<FieldAdapter> getFieldSources(List<String> fieldNames, ClassAdapter initialClass) {
        List<FieldAdapter> result = new LinkedList<>();

        ClassAdapter currentType = initialClass;

        for (int i = 0; i < fieldNames.size() - 1; i++) {
            FieldAdapter field = currentType.getDeclaredField(fieldNames.get(i));
            result.add(field);
            currentType = sourceTypeNameToTypeMapper.apply(field.getType().getSimpleName());
        }

        if (!fieldNames.isEmpty()) {
            result.add(currentType.getDeclaredField(fieldNames.get(fieldNames.size() - 1)));
        }

        return result;
    }
}
