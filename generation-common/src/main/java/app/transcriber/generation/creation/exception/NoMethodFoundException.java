package app.transcriber.generation.creation.exception;

public class NoMethodFoundException extends RuntimeException {

    public NoMethodFoundException(Exception cause) {
        super(cause);
    }
}
