package app.transcriber.generation.creation.input;

import app.transcriber.api.model.ConstantMappingDefinition;
import app.transcriber.api.model.TypesMapperDefinition;
import app.transcriber.generation.creation.exception.NoClassSourceFoundException;
import app.transcriber.generation.model.ConstantMappingGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

@AllArgsConstructor
public class ConstantMappingGenerationInputFactory implements BiFunction<TypesMapperDefinition, ConstantMappingDefinition, ConstantMappingGenerationInput> {

    @NonNull
    private final Function<String, ClassAdapter> targetTypeNameToTypeMapper;

    @Override
    public ConstantMappingGenerationInput apply(@NonNull TypesMapperDefinition typesMapperDefinition, @NonNull ConstantMappingDefinition constantMappingDefinition) {
        String targetSourceClassName = typesMapperDefinition.getTargetClassName();

        ClassAdapter targetClassType = Optional.ofNullable(targetTypeNameToTypeMapper.apply(targetSourceClassName))
                .orElseThrow(() -> new NoClassSourceFoundException(targetSourceClassName));

        return ConstantMappingGenerationInput.builder()
                .targetField(targetClassType.getDeclaredField(constantMappingDefinition.getTargetFieldName()))
                .value(constantMappingDefinition.getValue())
                .build();
    }
}