package app.transcriber.generation.creation.input;

import app.transcriber.api.model.MappingOperatorDefinition;
import app.transcriber.api.model.TypedParameterDefinition;
import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.creation.exception.NoMethodFoundException;
import app.transcriber.generation.model.MappingOperatorsGenerationInput;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import lombok.AllArgsConstructor;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@AllArgsConstructor
public class MappingOperatorsGenerationInputFactory implements Function<MappingOperatorDefinition, MappingOperatorsGenerationInput> {

    private final Function<String, ClassAdapter> operatorNameToTypeMapper;

    private final Function<TypedParameterDefinition, TypedParameterGenerationInput> typedParameterGenerationInputFactory;

    @Override
    public MappingOperatorsGenerationInput apply(MappingOperatorDefinition mappingOperatorDefinition) {
        return MappingOperatorsGenerationInput.builder()
                .creationParametersGenerationInputs(Optional.ofNullable(mappingOperatorDefinition.getCreationParameters())
                        .orElse(Collections.emptyList())
                        .stream()
                        .map(typedParameterGenerationInputFactory)
                        .collect(Collectors.toList()))
                .mapperSourceType(getOperatorParameterType(operatorNameToTypeMapper.apply(mappingOperatorDefinition.getOperatorName())))
                .mapperTargetType(getOperatorReturnType(operatorNameToTypeMapper.apply(mappingOperatorDefinition.getOperatorName())))
                .operatorName(mappingOperatorDefinition.getOperatorName())
                .build();
    }


    private ClassAdapter getOperatorReturnType(ClassAdapter clz) {
        return ClassAdapter.of(getOperatorMethod(clz).getReturnType());
    }

    private ClassAdapter getOperatorParameterType(ClassAdapter clz) {
        return ClassAdapter.of(getOperatorMethod(clz).getParameters()[0].getType());
    }

    private Method getOperatorMethod(ClassAdapter clz) {
        return getMethodByNameOnly(clz, NamingConvention.OPERATOR_APPLY_METHOD.toString());
    }

    private Method getMethodByNameOnly(ClassAdapter clz, String methodName) {
        for (Method m : clz.getMethods()) {
            if (Objects.equals(methodName, m.getName())) {
                return m;
            }
        }

        throw new NoMethodFoundException(new NoSuchMethodException("Method " + methodName + " was not found in " + clz.getName()));
    }
}
