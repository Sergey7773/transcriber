package app.transcriber.generation.creation.input;

import app.transcriber.api.model.TypedParameterDefinition;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import lombok.AllArgsConstructor;

import java.util.Map;
import java.util.function.Function;

@AllArgsConstructor
public class TypedParameterGenerationInputFactory implements Function<TypedParameterDefinition, TypedParameterGenerationInput> {

    private Map<String, Class<?>> typesMap;

    private Map<String, Function<String, Object>> parsingFunctionsMap;

    @Override
    public TypedParameterGenerationInput apply(TypedParameterDefinition typedParameterDefinition) {

        return TypedParameterGenerationInput.builder()
                .parameterType(ClassAdapter.of(typesMap.getOrDefault(typedParameterDefinition.getTypeName(), Object.class)))
                .parameterValue(parsingFunctionsMap.getOrDefault(typedParameterDefinition.getTypeName(), Object::toString)
                        .apply(typedParameterDefinition.getParameterValue()))
                .build();
    }
}
