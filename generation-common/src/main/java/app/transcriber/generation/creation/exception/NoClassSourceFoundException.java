package app.transcriber.generation.creation.exception;

public class NoClassSourceFoundException extends IllegalArgumentException {

    public NoClassSourceFoundException(String className) {
        super("No matching source class was found for [" + className + "]");
    }

}
