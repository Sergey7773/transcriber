package app.transcriber.generation.creation;

import app.transcriber.api.exceptions.GenerationInputVerificationException;
import app.transcriber.api.model.GenerationProcessPackages;
import app.transcriber.api.model.TypesMapperDefinition;
import app.transcriber.api.model.verification.TypesMapperGenerationInputVerificationError;
import app.transcriber.generation.creation.input.TypesMapperGenerationInputFactory;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.NamingConventionsContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import app.transcriber.generation.verification.verifiers.TypesMapperGenerationInputVerifier;
import app.transcriber.tools.OptionalMapperCollector;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

@AllArgsConstructor
public class MapperGenerationContextFactory {

    @NonNull
    private Map<String, ClassAdapter> sourceClassesMap;

    @NonNull
    private Map<String, ClassAdapter> targetClassesMap;

    @NonNull
    private TypesMapperGenerationInputFactory typesMapperGenerationInputFactory;

    @NonNull
    private TypesMapperGenerationInputVerifier typesMapperGenerationInputVerifier;

    @NonNull
    private NamingConventionsContext namingConventionsContext;

    public MapperGenerationContext create(Collection<TypesMapperDefinition> mapperDefinitions,
                                          GenerationProcessPackages generationProcessPackages) throws GenerationInputVerificationException {

        MapperGenerationContext.MapperGenerationContextBuilder builder = MapperGenerationContext.builder();

        Collection<TypesMapperGenerationInput> typeMappersMetadata = mapperDefinitions
                .stream()
                .map(typesMapperGenerationInputFactory)
                .collect(Collectors.toList());

        Collection<TypesMapperGenerationInputVerificationError> verificationErrors =
                OptionalMapperCollector.mapCollection(typeMappersMetadata, typesMapperGenerationInputVerifier);

        if (!verificationErrors.isEmpty()) {
            throw new GenerationInputVerificationException(verificationErrors);
        }

        return builder.typesMapperGenerationInputs(typeMappersMetadata)
                .sourceClasses(sourceClassesMap)
                .targetClasses(targetClassesMap)
                .namingConventionsContext(namingConventionsContext)
                .outputPackage(generationProcessPackages.getOutputPackage())
                .build();
    }

}
