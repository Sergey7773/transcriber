package app.transcriber.generation.conventions;

public enum NamingConvention {

    SOURCE_OBJECT("source"),
    TARGET_OBJECT("target"),
    SOURCE_SUB_ENTITY_OBJECT("SourceSubEntity"),
    TARGET_SUB_ENTITY_OBJECT("TargetSubEntity"),
    SUPPLIER("supplier"),
    MAPPED_VALUE("mappedValue"),
    OPERATOR_APPLY_METHOD("apply"),
    MAPPING_OPERATOR_FACTORY_MEMBER("mappingOperatorFactory"),
    MAPPING_OPERATOR_FACTORY_CREATE_FUNCTION("createOperator"),
    NULL("null"),
    SETUP("setup"),
    MAPPER_FACTORY_MEMBER("mapperFactory"),
    MAPPER_FACTORY_CREATE_FUNCTION("createMapper"),
    VALUE_PRODUCER_FACTORY_MEMBER("valueProducerFactory"),
    SOURCE_SUB_ENTITY_TO_TARGET_SUB_ENTITY_MAPPER("sourceSubEntityToTargetSubEntityMapper"),
    VALUE_PRODUCER_FACTORY_CREATE_FUNCTION("createValueProducer"),
    VALUE_PRODUCER_GET_FUNCTION("get"),
    TARGET_MODEL_OBJECT_FACTORY_MEMBER("targetModelObjectFactory"),
    TARGET_MODEL_OBJECT_FACTORY_CREATE_FUNCTION("create"),
    OBJECT("Object");

    private String convention;

    NamingConvention(String convention) {
        this.convention = convention;
    }

    @Override
    public String toString() {
        return this.convention;
    }
}
