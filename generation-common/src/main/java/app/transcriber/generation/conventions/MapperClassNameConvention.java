package app.transcriber.generation.conventions;

import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import lombok.NonNull;

import java.util.Optional;
import java.util.function.Function;

/**
 * Used during the generation process in order to compute the names of classes which perform mapping between different types
 *
 * @See {@link app.transcriber.api.interfaces.Mapper}
 */
public class MapperClassNameConvention implements Function<TypesMapperGenerationInput, String> {

    public String apply(@NonNull TypesMapperGenerationInput typesMapperGenerationInput) {
        ClassAdapter sourceClassAdapter = Optional.ofNullable(typesMapperGenerationInput.getSourceClass())
                .orElseThrow(() -> new IllegalArgumentException("Source class must be non null"));

        ClassAdapter targetClassAdapter = Optional.ofNullable(typesMapperGenerationInput.getTargetClass())
                .orElseThrow(() -> new IllegalArgumentException("Target class must be non null"));

        return sourceClassAdapter.getSimpleName() +
                "To" +
                targetClassAdapter.getSimpleName() +
                "Mapper";
    }

    public String getMapperTestClassName(@NonNull TypesMapperGenerationInput typesMapperGenerationInput){
        return apply(typesMapperGenerationInput) + "Test";
    }
}
