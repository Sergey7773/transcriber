package app.transcriber.generation.conventions;

import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.adapters.FieldAdapter;
import app.transcriber.tools.ListAccessUtils;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;
import java.util.function.Function;

/**
 * Used during the generation process in order to compute the names of methods,
 * which perform a mapping from one field to another.
 */
public class MapMethodNameConvention implements Function<FieldsMapperGenerationInput, String> {

    @Override
    public String apply(@NonNull FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        FieldAdapter lastFieldInList = ListAccessUtils.getLastElementOrThrow(fieldsMapperGenerationInput.getSourceFields());

        FieldAdapter targetField = Optional.ofNullable(fieldsMapperGenerationInput.getTargetField())
                .orElseThrow(() -> new IllegalArgumentException("Target field must be non null"));

        return "map" + StringUtils.capitalize(lastFieldInList.getName()) + "To" + StringUtils.capitalize(targetField.getName());
    }
}
