package app.transcriber.generation.conventions;

import app.transcriber.generation.model.adapters.FieldAdapter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.util.function.Function;

/**
 * Used to compute the getter names for various fields which will be encountered during the generation process.
 */
public class GetterMethodNameConvention implements Function<FieldAdapter, String> {

    @Override
    public String apply(@NonNull FieldAdapter fieldSource) {
        return "get" + StringUtils.capitalize(fieldSource.getName());
    }

}