package app.transcriber.generation.conventions;

import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.adapters.FieldAdapter;
import app.transcriber.tools.ListAccessUtils;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;
import java.util.function.Function;

/**
 * Used during the generation process to compute the variable names of mappers which are created inside a map method,
 * when there is a need to perform a mapping between complex types
 */
public class SubEntityMapperVariableNameConvention implements Function<FieldsMapperGenerationInput, String> {

    @Override
    public String apply(@NonNull FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        FieldAdapter lastFieldSourceInList = ListAccessUtils.getLastElementOrThrow(fieldsMapperGenerationInput.getSourceFields());

        FieldAdapter targetField = Optional.ofNullable(fieldsMapperGenerationInput.getTargetField())
                .orElseThrow(() -> new IllegalArgumentException("Target field must be non null"));

        return new StringBuilder()
                .append(lastFieldSourceInList.getName())
                .append("To")
                .append(StringUtils.capitalize(targetField.getName()))
                .append("Mapper")
                .toString();
    }
}
