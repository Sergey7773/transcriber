package app.transcriber.generation.conventions;

import app.transcriber.generation.model.FieldsValueProducerGenerationInput;
import app.transcriber.generation.model.adapters.FieldAdapter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;
import java.util.function.Function;

/**
 * Used during the generation process in order to compute the names of methods which assign a value
 * to a given field via a specified producer
 *
 * @See {@link app.transcriber.api.annotations.ValuesProducer}
 */
public class ProduceMethodNameConvention implements Function<FieldsValueProducerGenerationInput, String> {

    @Override
    public String apply(@NonNull FieldsValueProducerGenerationInput fieldsValueProducerGenerationInput) {
        FieldAdapter targetField = Optional.ofNullable(fieldsValueProducerGenerationInput.getTargetField())
                .orElseThrow(() -> new IllegalArgumentException("Target field must be non null"));

        return "produce" + StringUtils.capitalize(targetField.getName()) + "Value";
    }
}
