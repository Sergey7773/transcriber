package app.transcriber.generation.conventions;

import app.transcriber.generation.model.ConstantMappingGenerationInput;
import app.transcriber.generation.model.adapters.FieldAdapter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;
import java.util.function.Function;

/**
 * Used during the generation process in order to compute the names of methods which assign a specified constant value to a field
 */
public class SetConstantValueMethodNameConvention implements Function<ConstantMappingGenerationInput, String> {

    @Override
    public String apply(@NonNull ConstantMappingGenerationInput constantMappingGenerationInput) {
        FieldAdapter targetField = Optional.ofNullable(constantMappingGenerationInput.getTargetField())
                .orElseThrow(() -> new IllegalArgumentException("Target field must be non null"));
        return "set" + StringUtils.capitalize(targetField.getName()) + "ConstantValue";
    }
}
