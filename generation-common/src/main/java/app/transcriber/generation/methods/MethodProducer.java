package app.transcriber.generation.methods;

import com.squareup.javapoet.MethodSpec;

import java.util.function.Supplier;

public interface MethodProducer extends Supplier<MethodSpec> {

}
