package app.transcriber.generation.methods;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.TypeName;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractMethodsProducer implements MethodProducer {

    @Override
    public MethodSpec get() {
        MethodSpec.Builder resultBuilder = createMethodDefinition()
                .addParameters(createMethodParameters())
                .addCode(createMethodBody())
                .addExceptions(thrownExceptions().stream().map(TypeName::get).collect(Collectors.toList()));

        createJavadoc().ifPresent(resultBuilder::addJavadoc);

        return resultBuilder.build();
    }

    protected abstract MethodSpec.Builder createMethodDefinition();

    protected abstract CodeBlock createMethodBody();

    protected List<ParameterSpec> createMethodParameters() {
        return Collections.emptyList();
    }

    protected List<Type> thrownExceptions() {
        return Collections.emptyList();
    }

    protected Optional<CodeBlock> createJavadoc() { return Optional.empty(); }

}