package app.transcriber.generation.methods.supplier.map.predicates;

import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.adapters.ClassAdapter;
import app.transcriber.generation.model.adapters.FieldAdapter;
import app.transcriber.tools.ListAccessUtils;
import lombok.NonNull;

import java.util.Collection;
import java.util.function.BiPredicate;

public class IsCollectionTypePredicate implements BiPredicate<MapperGenerationContext, FieldsMapperGenerationInput> {

    @Override
    public boolean test(@NonNull MapperGenerationContext mapperGenerationContext, @NonNull FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        FieldAdapter lastFieldSourceInList = ListAccessUtils.getOptionalLastElementOf(fieldsMapperGenerationInput.getSourceFields())
                .orElseThrow(() -> new IllegalArgumentException("Source fields list must not be null"));

        return ClassAdapter.of(Collection.class).isAssignableFrom(lastFieldSourceInList.getType());
    }
}
