package app.transcriber.generation.methods;

import app.transcriber.generation.conventions.GetterMethodNameConvention;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.adapters.FieldAdapter;
import com.squareup.javapoet.CodeBlock;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GettersChainGenerator {

    public CodeBlock createGettersChain(String variableName, List<FieldAdapter> fieldSources, MapperGenerationContext mapperGenerationContext) {
        return createGettersChain(variableName, fieldSources, mapperGenerationContext, CodeBlock.builder().add("$L", Objects.toString(null)).build());
    }

    public CodeBlock createGettersChain(String variableName, List<FieldAdapter> fieldSources, MapperGenerationContext mapperGenerationContext, CodeBlock defaultValue) {
        Function<FieldAdapter, String> getterMethodNameConvention = mapperGenerationContext.getNamingConventionsContext().getConvention(GetterMethodNameConvention.class);

        List<CodeBlock> getterBlocks = new LinkedList<>();
        FieldAdapter firstField = fieldSources.get(0);

        getterBlocks.add(CodeBlock.of("$T.ofNullable($L.$L())", Optional.class, variableName, getterMethodNameConvention.apply(firstField)));

        getterBlocks.addAll(fieldSources.stream().skip(1)
                .map(field -> CodeBlock.builder().add("map($T::$L)", field.getDeclaringClass().getWrappedType(),
                        getterMethodNameConvention.apply(field)).build())
                .collect(Collectors.toList()));

        getterBlocks.add(CodeBlock.of("orElse($L)", defaultValue));

        return CodeBlock.join(getterBlocks, ".");
    }

    public List<String> getListOfGetters(List<FieldAdapter> fieldSource, MapperGenerationContext mapperGenerationContext) {
        return fieldSource.stream().map(mapperGenerationContext.getNamingConventionsContext().getConvention(GetterMethodNameConvention.class)).collect(Collectors.toList());
    }

}