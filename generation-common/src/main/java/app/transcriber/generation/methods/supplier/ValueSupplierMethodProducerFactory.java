package app.transcriber.generation.methods.supplier;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;

import java.util.Optional;

public interface ValueSupplierMethodProducerFactory<T> {

    Optional<MethodProducer> create(MapperGenerationContext mapperGenerationContext,
                                    TypesMapperGenerationInput typesMapperGenerationInput, T t);

}
