package app.transcriber.generation.methods.supplier.map.predicates;

import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.adapters.FieldAdapter;
import app.transcriber.tools.ListAccessUtils;

import java.util.function.BiPredicate;

public class IsMappedArgPredicate implements BiPredicate<MapperGenerationContext, FieldsMapperGenerationInput> {

    @Override
    public boolean test(MapperGenerationContext mapperGenerationContext, FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        FieldAdapter lastFieldSourceInList = ListAccessUtils.getOptionalLastElementOf(fieldsMapperGenerationInput.getSourceFields())
                .orElseThrow(() -> new IllegalArgumentException("Source fields list must not be null"));
        return mapperGenerationContext.getSourceClasses().containsKey(lastFieldSourceInList.getType().getSimpleName());
    }
}
