package app.transcriber.generation.methods.supplier;

import java.util.Objects;

public class ConstantSetMethodArgumentFormatter {

    public String format(Object value) {
        if (value instanceof String) {
            return "\"" + value + "\"";
        } else if (value instanceof Character) {
            return "'" + value + "'";
        }

        return Objects.toString(value);
    }
}
