package app.transcriber.tools;

import org.reflections.ReflectionUtils;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ClassesMapLoader {

    public <T> Map<T, Class<?>> buildMap(Collection<String> scannedPackage, Function<Class<?>, T> keyCreator) {
        return buildMap(scannedPackage, keyCreator, clz -> true);
    }

    public <T> Map<T, Class<?>> buildMap(Collection<String> scannedPackage, Function<Class<?>, T> keyCreator, Predicate<Class<?>> filter) {
        ConfigurationBuilder reflectionsConfigurationBuilder = new ConfigurationBuilder();

        scannedPackage.stream()
                .map(ClasspathHelper::forPackage)
                .forEach(reflectionsConfigurationBuilder::addUrls);

        reflectionsConfigurationBuilder.setScanners(new SubTypesScanner(false));

        Reflections reflections = new Reflections(reflectionsConfigurationBuilder);

        return reflections.getAllTypes()
                .stream()
                .map(ReflectionUtils::forName)
                .filter(filter)
                .collect(Collectors.toMap(keyCreator, clz -> clz));
    }

}
