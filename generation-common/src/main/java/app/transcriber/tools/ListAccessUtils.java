package app.transcriber.tools;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Optional;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ListAccessUtils {

    public static <T> Optional<T> getOptionalLastElementOf(List<T> list) {
        if (list == null || list.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(list.get(list.size() - 1));
    }

    public static <T> T getLastElementOrThrow(List<T> list) {
        return getOptionalLastElementOf(list)
                .orElseThrow(() -> {
                    String listDescription = list == null ? "a null" : "an empty";
                    return new IllegalArgumentException("Attempted to fetch the last element from " + listDescription + " list");
                });
    }

    public static <T> T getLastElementOf(List<T> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }

        return list.get(list.size() - 1);
    }

}
