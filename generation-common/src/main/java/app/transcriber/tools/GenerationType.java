package app.transcriber.tools;

public enum GenerationType {

    FIELDS_MAPPER_GENERATION,
    FIELDS_VALUE_PRODUCER_GENERATION,
    CONSTANT_MAPPING_GENERATION
}