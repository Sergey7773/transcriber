package app.transcriber.tools;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OptionalMapperCollector {

    public static <T, S> Collection<T> mapCollection(Collection<S> source, Function<S, Optional<T>> mapper) {
        return Optional.ofNullable(source)
                .orElse(Collections.emptyList())
                .stream()
                .map(mapper)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }
}
