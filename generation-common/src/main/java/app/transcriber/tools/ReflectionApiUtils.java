package app.transcriber.tools;

import app.transcriber.generation.model.adapters.FieldAdapter;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ReflectionApiUtils {

    public static Type getGenericTypeArgument(FieldAdapter field) {
        return ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
    }

    public static String getGenericTypeArgumentSimpleName(FieldAdapter field) {
        String genericTypeParameterName = getGenericTypeArgument(field).getTypeName();
        return genericTypeParameterName.substring(genericTypeParameterName.lastIndexOf('.') + 1);
    }
}
