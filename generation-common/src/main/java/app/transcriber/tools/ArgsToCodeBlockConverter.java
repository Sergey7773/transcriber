package app.transcriber.tools;

import app.transcriber.generation.model.TypedParameterGenerationInput;
import com.squareup.javapoet.CodeBlock;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ArgsToCodeBlockConverter implements Function<List<TypedParameterGenerationInput>, CodeBlock> {

    @Override
    public CodeBlock apply(List<TypedParameterGenerationInput> args) {
        if (args == null || args.isEmpty()) {
            return CodeBlock.builder().add("$T.emptyList()", Collections.class).build();
        }

        List<String> argsAsString = args.stream()
                .map(TypedParameterGenerationInput::getParameterValue)
                .map(this::toStringWithQuotationMarks)
                .collect(Collectors.toList());

        return CodeBlock.builder()
                .add("$T.asList($L)", Arrays.class, String.join(", ", argsAsString))
                .build();
    }

    private String toStringWithQuotationMarks(Object obj) {
        if (obj instanceof String) {
            return "\"" + obj + "\"";
        }

        if (obj instanceof Character) {
            return "'" + obj + "'";
        }

        return Objects.toString(obj);
    }

}
