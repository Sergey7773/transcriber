package app.transcriber;

public class TranscriberConfigurationProperties {

    public static final String PROPERTIES_PREFIX = "transcriber.configuration";

    public static final String SOURCE_CLASSES_PACKAGES = "source-classes-packages";

    public static final String TARGET_CLASSES_PACKAGES = "target-classes-packages";

    public static final String MAPPING_OPERATOR_PACKAGES = "mapping-operator-packages";

    public static final String VALUE_PRODUCER_PACKAGES = "value-producer-packages";

    public static final String MAPPERS_PACKAGE = "mappers-package";

    public static final String MAPPERS_OUTPUT_DIRECTORY = "mappers-output-directory";

    public static final String MAPPER_JAVA_FILES_DIRECTORY = "mapper-java-files-directory";

    public static final String MAPPER_CLASSES_DIRECTORY = "mapper-classes-directory";

    public static final String CONCURRENT_CACHES_PROPERTY_NAME = "concurrent-caches";

    public static final String ENABLE_MAPPERS_RELOADING = "enable-mappers-reloading";

    public static final String MAPPERS_CONFIG_FILE = "mappers-config-file";
}