package app.transcriber.generation.conventions;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NamingConventionsConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public MapperClassNameConvention mapperClassNameConvention() {
        return new MapperClassNameConvention();
    }

    @Bean
    @ConditionalOnMissingBean
    public SubEntityMapperVariableNameConvention subEntityMapperVariableNameConvention() {
        return new SubEntityMapperVariableNameConvention();
    }

    @Bean
    @ConditionalOnMissingBean
    public GetterMethodNameConvention getterMethodNameConvention() {
        return new GetterMethodNameConvention();
    }

    @Bean
    @ConditionalOnMissingBean
    public SetterMethodNameConvention setterMethodNameConvention() {
        return new SetterMethodNameConvention();
    }

    @Bean
    @ConditionalOnMissingBean
    public MapMethodNameConvention mapMethodNameConvention() {
        return new MapMethodNameConvention();
    }

    @Bean
    @ConditionalOnMissingBean
    public ProduceMethodNameConvention produceMethodNameConvention() {
        return new ProduceMethodNameConvention();
    }

    @Bean
    @ConditionalOnMissingBean
    public SetConstantValueMethodNameConvention setConstantValueMethodNameConvention() {
        return new SetConstantValueMethodNameConvention();
    }
}
