package app.transcriber.generation.creation;

import app.transcriber.generation.creation.input.TypesMapperGenerationInputFactory;
import app.transcriber.generation.model.NamingConventionsContext;
import app.transcriber.generation.model.adapters.ClassAdapter;
import app.transcriber.generation.verification.verifiers.TypesMapperGenerationInputVerifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
public class MapperGenerationContextFactoryConfiguration {
    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public MapperGenerationContextFactory mapperGenerationContextFactory(@Qualifier(GenerationClassesConfiguration.SOURCE_TYPE_CLASSES_MAP) Map<String, ClassAdapter> sourceClassesMap,
                                                                         @Qualifier(GenerationClassesConfiguration.TARGET_TYPE_CLASSES_MAP) Map<String, ClassAdapter> targetClassesMap,
                                                                         TypesMapperGenerationInputFactory typesMapperGenerationInputFactory,
                                                                         TypesMapperGenerationInputVerifier typesMapperGenerationInputVerifier,
                                                                         NamingConventionsContext namingConventionsContext) {
        return new MapperGenerationContextFactory(sourceClassesMap, targetClassesMap, typesMapperGenerationInputFactory, typesMapperGenerationInputVerifier, namingConventionsContext);
    }
}
