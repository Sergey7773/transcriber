package app.transcriber.generation.creation;

import app.transcriber.generation.creation.input.*;
import app.transcriber.generation.model.adapters.ClassAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

@Configuration
public class GenerationInputFactoriesConfiguration {
    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public TypesMapperGenerationInputFactory typesMapperGenerationInputFactory(
            @Qualifier(GenerationClassesConfiguration.SOURCE_TYPE_CLASSES_MAP) Map<String, ClassAdapter> sourceTypeClasses,
            @Qualifier(GenerationClassesConfiguration.TARGET_TYPE_CLASSES_MAP) Map<String, ClassAdapter> targetTypeClasses,
            FieldsMapperGenerationInputFactory fieldsMapperGenerationInputFactory,
            FieldsValueProducerGenerationInputFactory fieldsValueProducerGenerationInputFactory,
            ConstantMappingGenerationInputFactory constantMappingGenerationInputFactory) {
        return new TypesMapperGenerationInputFactory(sourceTypeClasses::get,
                targetTypeClasses::get, fieldsMapperGenerationInputFactory,
                fieldsValueProducerGenerationInputFactory, constantMappingGenerationInputFactory);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public FieldsMapperGenerationInputFactory fieldsMapperGenerationInputFactory(
            @Qualifier(GenerationClassesConfiguration.SOURCE_TYPE_CLASSES_MAP) Map<String, ClassAdapter> sourceTypeClasses,
            @Qualifier(GenerationClassesConfiguration.TARGET_TYPE_CLASSES_MAP) Map<String, ClassAdapter> targetTypeClasses,
            MappingOperatorsGenerationInputFactory mappingOperatorsGenerationInputFactory) {
        return new FieldsMapperGenerationInputFactory(sourceTypeClasses::get, targetTypeClasses::get, mappingOperatorsGenerationInputFactory);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public ConstantMappingGenerationInputFactory constantMappingGenerationInputFactory(
            @Qualifier(GenerationClassesConfiguration.TARGET_TYPE_CLASSES_MAP) Map<String, ClassAdapter> targetTypeClasses) {
        return new ConstantMappingGenerationInputFactory(targetTypeClasses::get);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public MappingOperatorsGenerationInputFactory mappingOperatorsGenerationInputFactory(
            @Qualifier(GenerationClassesConfiguration.MAPPING_OPERATOR_TYPE_CLASSES_MAP) Map<String, ClassAdapter> mappingOperatorClasses,
            TypedParameterGenerationInputFactory typedParameterGenerationInputFactory) {
        return new MappingOperatorsGenerationInputFactory(mappingOperatorClasses::get, typedParameterGenerationInputFactory);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public FieldsValueProducerGenerationInputFactory fieldsValueProducerGenerationInputFactory(
            @Qualifier(GenerationClassesConfiguration.TARGET_TYPE_CLASSES_MAP) Map<String, ClassAdapter> targetJavaSources,
            TypedParameterGenerationInputFactory typedParameterGenerationInputFactory) {
        return new FieldsValueProducerGenerationInputFactory(targetJavaSources::get, typedParameterGenerationInputFactory);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public TypedParameterGenerationInputFactory typedParameterGenerationInputFactory() {
        Map<String, Class<?>> classNamesMap = new HashMap<>();
        classNamesMap.put("int", int.class);
        classNamesMap.put("Integer", Integer.class);
        classNamesMap.put("double", double.class);
        classNamesMap.put("Double", Double.class);
        classNamesMap.put("String", String.class);
        classNamesMap.put("boolean", boolean.class);
        classNamesMap.put("Boolean", Boolean.class);
        classNamesMap.put("char", char.class);
        classNamesMap.put("Character", Character.class);
        classNamesMap.put("float", float.class);
        classNamesMap.put("Float", Float.class);

        Map<String, Function<String, Object>> parsingFunctionsMap = new HashMap<>();
        parsingFunctionsMap.put("int", Integer::valueOf);
        parsingFunctionsMap.put("Integer", Integer::valueOf);
        parsingFunctionsMap.put("double", Double::valueOf);
        parsingFunctionsMap.put("Double", Double::valueOf);
        parsingFunctionsMap.put("String", Objects::toString);
        parsingFunctionsMap.put("boolean", Boolean::valueOf);
        parsingFunctionsMap.put("Boolean", Boolean::valueOf);
        parsingFunctionsMap.put("char", s -> s.charAt(0));
        parsingFunctionsMap.put("Character", s -> s.charAt(0));
        parsingFunctionsMap.put("float", Float::valueOf);
        parsingFunctionsMap.put("Float", Float::valueOf);

        return new TypedParameterGenerationInputFactory(classNamesMap, parsingFunctionsMap);
    }
}
