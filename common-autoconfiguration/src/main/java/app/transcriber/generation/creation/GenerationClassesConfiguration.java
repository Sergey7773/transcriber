package app.transcriber.generation.creation;

import app.transcriber.TranscriberConfigurationPropertiesHolder;
import app.transcriber.api.annotations.MappingOperator;
import app.transcriber.generation.model.adapters.ClassAdapter;
import app.transcriber.tools.ClassesMapLoader;
import app.transcriber.tools.MappingComponentNameFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Configuration
public class GenerationClassesConfiguration {
    public static final String SOURCE_TYPE_CLASSES_MAP = "sourceTypeClassesMap";

    public static final String TARGET_TYPE_CLASSES_MAP = "targetTypeClassesMap";

    public static final String MAPPING_OPERATOR_TYPE_CLASSES_MAP = "mappingOperatorTypeClassesMap";

    @Autowired
    private ClassesMapLoader classesMapLoader;

    @Bean(SOURCE_TYPE_CLASSES_MAP)
    @Autowired
    public Map<String, ClassAdapter> sourceTypeClasses(TranscriberConfigurationPropertiesHolder transcriberConfigurationPropertiesHolder) {
        return toClassAdapterMap(classesMapLoader.buildMap(transcriberConfigurationPropertiesHolder.getSourceClassesPackages(), Class::getSimpleName));
    }

    @Bean(TARGET_TYPE_CLASSES_MAP)
    @Autowired
    public Map<String, ClassAdapter> targetTypeClasses(TranscriberConfigurationPropertiesHolder transcriberConfigurationPropertiesHolder) {
        return toClassAdapterMap(classesMapLoader.buildMap(transcriberConfigurationPropertiesHolder.getTargetClassesPackages(), Class::getSimpleName));
    }

    @Bean(MAPPING_OPERATOR_TYPE_CLASSES_MAP)
    @Autowired
    public Map<String, ClassAdapter> mappingOperatorTypeClasses(TranscriberConfigurationPropertiesHolder transcriberConfigurationPropertiesHolder) {
        return toClassAdapterMap(classesMapLoader.buildMap(transcriberConfigurationPropertiesHolder.getMappingOperatorPackages(),
                new MappingComponentNameFetcher(), clz -> Objects.nonNull(clz.getAnnotation(MappingOperator.class))));
    }

    private Map<String, ClassAdapter> toClassAdapterMap(Map<String, Class<?>> classesMap) {
        return classesMap.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> ClassAdapter.of(entry.getValue())));
    }

}
