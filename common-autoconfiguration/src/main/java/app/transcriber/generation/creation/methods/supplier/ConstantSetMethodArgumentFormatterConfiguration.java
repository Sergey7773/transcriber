package app.transcriber.generation.creation.methods.supplier;

import app.transcriber.generation.methods.supplier.ConstantSetMethodArgumentFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConstantSetMethodArgumentFormatterConfiguration {

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public ConstantSetMethodArgumentFormatter constantSetMethodArgumentFormatter() {
        return new ConstantSetMethodArgumentFormatter();
    }
}
