package app.transcriber.generation.creation.verification.verifiers;

import app.transcriber.generation.verification.verifiers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GenerationInputVerifiersConfiguration {

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public TypesMapperGenerationInputVerifier typesMapperGenerationInputVerifier(FieldsMapperGenerationInputVerifier fieldsMapperGenerationInputVerifier,
                                                                                 FieldsValueProducerGenerationInputVerifier fieldsValueProducerGenerationInputVerifier,
                                                                                 ConstantMappingGenerationInputVerifier constantMappingGenerationInputVerifier) {
        return new TypesMapperGenerationInputVerifier(fieldsMapperGenerationInputVerifier,
                fieldsValueProducerGenerationInputVerifier, constantMappingGenerationInputVerifier);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public FieldsMapperGenerationInputVerifier fieldsMapperGenerationInputVerifier(MappingOperatorsGenerationInputVerifier mappingOperatorsGenerationInputVerifier) {
        return new FieldsMapperGenerationInputVerifier(mappingOperatorsGenerationInputVerifier);
    }

    @Bean
    @ConditionalOnMissingBean
    public ConstantMappingGenerationInputVerifier constantMappingGenerationInputVerifier() {
        return new ConstantMappingGenerationInputVerifier();
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public FieldsValueProducerGenerationInputVerifier fieldsValueProducerGenerationInputVerifier(TypedParametersGenerationInputVerifier typedParametersGenerationInputVerifier) {
        return new FieldsValueProducerGenerationInputVerifier(typedParametersGenerationInputVerifier);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public MappingOperatorsGenerationInputVerifier mappingOperatorsGenerationInputVerifier(TypedParametersGenerationInputVerifier typedParametersGenerationInputVerifier) {
        return new MappingOperatorsGenerationInputVerifier(typedParametersGenerationInputVerifier);
    }

    @Bean
    @ConditionalOnMissingBean
    public TypedParametersGenerationInputVerifier typedParametersGenerationInputVerifier() {
        return new TypedParametersGenerationInputVerifier();
    }

}
