package app.transcriber;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Set;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = TranscriberConfigurationProperties.PROPERTIES_PREFIX)
public class TranscriberConfigurationPropertiesHolder {

    private Set<String> sourceClassesPackages;

    private Set<String> targetClassesPackages;

    private Set<String> mappingOperatorPackages;

    private Set<String> valueProducerPackages;

    private String mappersPackage;

    private String mapperJavaFilesDirectory;

    private String mapperClassFilesDirectory;

    private boolean concurrentCaches;

    private boolean enableMappersReloading;

}
