package app.transcriber.tools;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ToolsConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public ArgsToCodeBlockConverter argsToCodeBlockConverter() {
        return new ArgsToCodeBlockConverter();
    }

    @Bean
    @ConditionalOnMissingBean
    public ClassesMapLoader classesMapLoader() {
        return new ClassesMapLoader();
    }
}
