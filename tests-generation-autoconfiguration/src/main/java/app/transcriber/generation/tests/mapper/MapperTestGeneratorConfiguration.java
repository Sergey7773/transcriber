package app.transcriber.generation.tests.mapper;

import app.transcriber.generation.fields.MapperFieldsGenerator;
import app.transcriber.generation.mappers.MapperTestGenerator;
import app.transcriber.generation.mappers.util.*;
import app.transcriber.generation.methods.apply.ApplyTestListMethodGenerator;
import app.transcriber.generation.methods.apply.ApplyTestMethodGeneratorFactory;
import app.transcriber.generation.mappers.constructor.AllArgsConstructorTestGeneratorFactory;
import app.transcriber.generation.mappers.constructor.ConstructorTestsMethodGenerator;
import app.transcriber.generation.methods.supplier.ConstantSetMethodArgumentFormatter;
import app.transcriber.generation.methods.supplier.SupplierTestMethodGenerator;
import app.transcriber.generation.methods.supplier.map.factories.ListFieldsMapMethodProducerFactory;
import app.transcriber.generation.methods.supplier.produce.ValueProducerMethodProducerTestGeneratorFactory;
import app.transcriber.generation.methods.supplier.set.ConstantSetMethodProducerTestGeneratorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperTestGeneratorConfiguration {

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public MapperTestGenerator mapperTestGenerator(MapperFieldsGenerator mapperFieldsGenerator,
                                                   ConstructorTestsMethodGenerator constructorTestsMethodGenerator,
                                                   SupplierTestMethodGenerator supplierTestMethodGenerator,
                                                   ApplyTestListMethodGenerator applyTestListMethodGenerator,
                                                   FieldsNameGenerator fieldsNameGenerator,
                                                   ParameterizedTypeFieldGenerator parameterizedTypeFieldGenerator,
                                                   TestSetupMethodGenerator testSetupMethodGenerator,
                                                   MapperClassFieldGenerator mapperClassFieldGenerator,
                                                   ClassNameGenerator classNameGenerator) {
        return MapperTestGenerator.builder()
                .mapperFieldsGenerator(mapperFieldsGenerator)
                .constructorTestsMethodGenerator(constructorTestsMethodGenerator)
                .supplierTestMethodGenerator(supplierTestMethodGenerator)
                .applyTestListMethodGenerator(applyTestListMethodGenerator)
                .fieldsNameGenerator(fieldsNameGenerator)
                .parameterizedTypeFieldGenerator(parameterizedTypeFieldGenerator)
                .testSetupMethodGenerator(testSetupMethodGenerator)
                .mapperClassFieldGenerator(mapperClassFieldGenerator)
                .classNameGenerator(classNameGenerator)
                .build();
    }

    @Bean
    @ConditionalOnMissingBean
    public MapperFieldsGenerator mapperFieldsGenerator() {
        return new MapperFieldsGenerator();
    }

    @Bean
    @ConditionalOnMissingBean
    public MapperClassFieldGenerator mapperClassFieldGenerator() {
        return new MapperClassFieldGenerator();
    }

    @Bean
    @ConditionalOnMissingBean
    public ClassNameGenerator classNameGenerator() { return new ClassNameGenerator();
    }

    @Bean
    @ConditionalOnMissingBean
    public AllArgsConstructorTestGeneratorFactory allArgsConstructorTestGeneratorFactory() {
        return new AllArgsConstructorTestGeneratorFactory();
    }

    @Bean
    @ConditionalOnMissingBean
    public ApplyTestMethodGeneratorFactory applyTestMethodGeneratorFactory() {
        return new ApplyTestMethodGeneratorFactory();
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public ConstructorTestsMethodGenerator constructorTestsMethodGenerator(AllArgsConstructorTestGeneratorFactory allArgsConstructorTestGeneratorFactory) {
        return new ConstructorTestsMethodGenerator(allArgsConstructorTestGeneratorFactory);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public SupplierTestMethodGenerator supplierTestMethodGenerator(ValueProducerMethodProducerTestGeneratorFactory valueProducerMethodProducerTestGeneratorFactory,
                                                                   ConstantSetMethodProducerTestGeneratorFactory constantSetMethodProducerTestGeneratorFactory, ListFieldsMapMethodProducerFactory listFieldsMapMethodProducerFactory) {
        return new SupplierTestMethodGenerator(valueProducerMethodProducerTestGeneratorFactory,constantSetMethodProducerTestGeneratorFactory, listFieldsMapMethodProducerFactory);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public ApplyTestListMethodGenerator applyTestListMethodGenerator(ApplyTestMethodGeneratorFactory applyTestMethodGeneratorFactory) {
        return new ApplyTestListMethodGenerator(applyTestMethodGeneratorFactory);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public FieldsNameGenerator generationUtils() {
        return new FieldsNameGenerator();
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public ParameterizedTypeFieldGenerator generateParameterizedTypeField() {
        return new ParameterizedTypeFieldGenerator();
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public TestSetupMethodGenerator generateTestSetupMethod() {
        return new TestSetupMethodGenerator();
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public ConstantSetMethodArgumentFormatter printConstantValueForConstantSetMethodProducer(){
        return new ConstantSetMethodArgumentFormatter();
    }
}