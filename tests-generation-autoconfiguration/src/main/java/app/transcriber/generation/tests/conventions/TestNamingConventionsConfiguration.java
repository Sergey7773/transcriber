package app.transcriber.generation.tests.conventions;

import app.transcriber.generation.conventions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestNamingConventionsConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public ApplyTestMethodNameConvention applyTestMethodNameConvention() {
        return new ApplyTestMethodNameConvention();
    }

    @Bean
    @ConditionalOnMissingBean
    public AllArgsConstructorTestNameConvention allArgsConstructorNameConvention() {
        return new AllArgsConstructorTestNameConvention();
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public MapTestMethodNameConvention mapTestMethodNameConvention(MapMethodNameConvention mapMethodNameConvention) {
        return new MapTestMethodNameConvention(mapMethodNameConvention);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public ProduceTestMethodNameConvention produceTestMethodNameConvention(ProduceMethodNameConvention produceMethodNameConvention) {
        return new ProduceTestMethodNameConvention(produceMethodNameConvention);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public SetConstantValueTestMethodNameConvention setConstantValueTestMethodNameConvention(SetConstantValueMethodNameConvention setConstantValueMethodNameConvention) {
        return new SetConstantValueTestMethodNameConvention(setConstantValueMethodNameConvention);
    }

}