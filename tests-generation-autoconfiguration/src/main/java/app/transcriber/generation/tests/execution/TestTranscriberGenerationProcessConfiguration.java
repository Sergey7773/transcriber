package app.transcriber.generation.tests.execution;

import app.transcriber.api.model.GenerationProcessPackages;
import app.transcriber.generation.creation.MapperGenerationContextFactory;
import app.transcriber.generation.execution.TranscriberGenerationProcess;
import app.transcriber.generation.mappers.MapperTestGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestTranscriberGenerationProcessConfiguration {

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public TranscriberGenerationProcess transcriberGenerationProcess(
            MapperTestGenerator mapperGenerator,
            MapperGenerationContextFactory mapperGenerationContextFactory,
            GenerationProcessPackages generationProcessPackages) {
        return TranscriberGenerationProcess.builder()
                .mapperGenerator(mapperGenerator)
                .generationProcessPackages(generationProcessPackages)
                .mapperGenerationContextFactory(mapperGenerationContextFactory)
                .build();
    }
}
