package app.transcriber.generation.tests.conventions;

import app.transcriber.generation.conventions.*;
import app.transcriber.generation.model.NamingConventionsContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestNamingConventionsContextConfiguration {
    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public NamingConventionsContext namingConventionsContext(MapperClassNameConvention mapperClassNameConvention,
                                                             SubEntityMapperVariableNameConvention subEntityMapperVariableNameConvention,
                                                             GetterMethodNameConvention getterMethodNameConvention,
                                                             SetterMethodNameConvention setterMethodNameConvention,
                                                             MapMethodNameConvention mapMethodNameConvention,
                                                             ProduceMethodNameConvention produceMethodNameConvention,
                                                             SetConstantValueMethodNameConvention setConstantValueMethodNameConvention,
                                                             ApplyTestMethodNameConvention applyTestMethodNameConventionl,
                                                             AllArgsConstructorTestNameConvention allArgsConstructorTestNameConvention,
                                                             MapTestMethodNameConvention mapTestMethodNameConvention,
                                                             ProduceTestMethodNameConvention produceTestMethodNameConvention,
                                                             SetConstantValueTestMethodNameConvention setConstantValueTestMethodNameConvention) {
        return new NamingConventionsContext()
                .addConvention(mapperClassNameConvention)
                .addConvention(subEntityMapperVariableNameConvention)
                .addConvention(getterMethodNameConvention)
                .addConvention(setterMethodNameConvention)
                .addConvention(mapMethodNameConvention)
                .addConvention(produceMethodNameConvention)
                .addConvention(setConstantValueMethodNameConvention)
                .addConvention(applyTestMethodNameConventionl)
                .addConvention(allArgsConstructorTestNameConvention)
                .addConvention(mapTestMethodNameConvention)
                .addConvention(produceTestMethodNameConvention)
                .addConvention(setConstantValueTestMethodNameConvention);
    }
}
