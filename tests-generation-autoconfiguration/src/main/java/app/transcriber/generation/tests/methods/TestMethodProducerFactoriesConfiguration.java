package app.transcriber.generation.tests.methods;

import app.transcriber.generation.methods.TestTypeGenerator;
import app.transcriber.generation.methods.supplier.ConstantSetMethodArgumentFormatter;
import app.transcriber.generation.methods.supplier.ValueSupplierMethodListProducerFactory;
import app.transcriber.generation.methods.supplier.map.factories.*;
import app.transcriber.generation.methods.supplier.map.predicates.IsCollectionTypePredicate;
import app.transcriber.generation.methods.supplier.map.predicates.IsGenericTypeWithMappedArgPredicate;
import app.transcriber.generation.methods.supplier.map.predicates.IsGenericTypeWithUnmappedPredicate;
import app.transcriber.generation.methods.supplier.map.predicates.IsMappedArgPredicate;
import app.transcriber.generation.methods.supplier.produce.ValueProducerMethodProducerTestGeneratorFactory;
import app.transcriber.generation.methods.supplier.set.ConstantSetMethodProducerTestGeneratorFactory;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.tools.ArgsToCodeBlockConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiPredicate;

@Configuration
public class TestMethodProducerFactoriesConfiguration {

    @Bean
    public ValueProducerMethodProducerTestGeneratorFactory valueProducerMethodProducerTestGeneratorFactory(@Autowired ArgsToCodeBlockConverter argsToCodeBlockConverter, TestTypeGenerator testTypeGenerator) {
        return new ValueProducerMethodProducerTestGeneratorFactory(argsToCodeBlockConverter, testTypeGenerator);
    }

    @Bean
    @ConditionalOnMissingBean
    public TestTypeGenerator testTypeGenerator() {
        return new TestTypeGenerator();
    }

    @Bean
    public ConstantSetMethodProducerTestGeneratorFactory constantSetMethodProducerTestGeneratorFactory(@Autowired ConstantSetMethodArgumentFormatter constantSetMethodArgumentFormatter) {
        return new ConstantSetMethodProducerTestGeneratorFactory(constantSetMethodArgumentFormatter);
    }

    @Bean
    @Autowired
    public ListFieldsMapMethodProducerFactory listFieldsMapMethodProducerFactory(ArgsToCodeBlockConverter argsToCodeBlockConverter, TestTypeGenerator testTypeGenerator) {
        Map<BiPredicate<MapperGenerationContext, FieldsMapperGenerationInput>, ValueSupplierMethodListProducerFactory<FieldsMapperGenerationInput>> factoriesMapOfList = new HashMap<>();

        factoriesMapOfList.put(new IsMappedArgPredicate().and((x, y) -> (new IsCollectionTypePredicate().negate().test(x, y))),
                new SubEntityMapTestMethodGeneratorFactory(argsToCodeBlockConverter, testTypeGenerator));

        factoriesMapOfList.put(new IsCollectionTypePredicate().and(new IsGenericTypeWithMappedArgPredicate()),
                new SubEntityCollectionMapTestMethodGeneratorFactory(argsToCodeBlockConverter, testTypeGenerator));

        factoriesMapOfList.put(new IsMappedArgPredicate().negate().and(new IsCollectionTypePredicate().negate()),
                new PrimitiveTypesMapTestMethodGeneratorFactory(argsToCodeBlockConverter, testTypeGenerator));

        factoriesMapOfList.put(new IsCollectionTypePredicate().and(new IsGenericTypeWithUnmappedPredicate()),
                new PrimitiveCollectionTypesMapTestMethodGeneratorFactory(argsToCodeBlockConverter, testTypeGenerator));

        return new ListFieldsMapMethodProducerFactory(factoriesMapOfList);
    }
}