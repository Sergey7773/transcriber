package app.transcriber.generation.tests.execution;

import app.transcriber.TranscriberConfigurationPropertiesHolder;
import app.transcriber.api.model.GenerationProcessPackages;
import app.transcriber.generation.execution.OutputFilePathCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestJavaFileConsumersConfiguration {

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public OutputFilePathCalculator outputFilePathCalculator(GenerationProcessPackages generationProcessPackages,
                                                             TranscriberConfigurationPropertiesHolder transcriberConfigurationPropertiesHolder) {
        return new OutputFilePathCalculator(transcriberConfigurationPropertiesHolder.getMapperJavaFilesDirectory());
    }
}