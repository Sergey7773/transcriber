package app.transcriber.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Optional;

@RestController
public class GraphQLController {

    @Autowired
    private GraphQL graphQL;

    @PostMapping(value = "/graphql")
    @ResponseBody
    public Map<String, Object> indexFromAnnotated(@RequestBody String request, HttpServletRequest raw) {

        Type stringToObjectMapType = new TypeToken<Map<String, Object>>() {
        }.getType();

        Map<String, Object> requestMap = new Gson().fromJson(request, stringToObjectMapType);

        String query = Optional.of(requestMap.get("query")).map(String::valueOf).orElseThrow(IllegalAccessError::new);
        String operationName = Optional.ofNullable(requestMap.get("operationName")).map(String::valueOf).orElse(null);

        ExecutionResult executionResult = graphQL.execute(ExecutionInput.newExecutionInput()
                .query(query)
                .operationName(operationName)
                .context(raw)
                .build());
        return executionResult.toSpecification();
    }

}
