package app.transcriber.entities;

import app.transcriber.api.model.TypesMapperDefinition;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TypesMapperDefinitionEntity extends RepositoryEntity<TypesMapperDefinition> {

    public TypesMapperDefinitionEntity(String id, TypesMapperDefinition data) {
        super(id, data);
    }
}
