package app.transcriber.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RepositoryEntity<T> {

    @Id
    protected String id;

    protected T data;
}
