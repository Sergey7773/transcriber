package app.transcriber.runners;

import app.transcriber.services.EntityPersistenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

@Component
public class CollectionCreatorRunner implements ApplicationRunner {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private Collection<EntityPersistenceService> entityPersistenceServices;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Set<String> existingCollections = mongoTemplate.getCollectionNames();

        Optional.ofNullable(entityPersistenceServices).orElse(Collections.emptyList())
                .stream()
                .map(EntityPersistenceService::getCollectionName)
                .filter(collectionName -> !existingCollections.contains(collectionName))
                .forEach(mongoTemplate::createCollection);
    }
}
