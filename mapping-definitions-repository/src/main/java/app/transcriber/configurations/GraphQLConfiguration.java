package app.transcriber.configurations;

import app.transcriber.services.TypesMapperDefinitionsServiceEntity;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import io.leangen.graphql.GraphQLSchemaGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collection;

@Configuration
public class GraphQLConfiguration {

    @Value("${transcriber.repository.base-packages}")
    private Collection<String> basePackages;

    @Bean
    @Autowired
    public GraphQL graphQL(TypesMapperDefinitionsServiceEntity typesMapperDefinitionsService) {
        GraphQLSchemaGenerator graphQLSchemaGenerator = new GraphQLSchemaGenerator();
        basePackages.forEach(graphQLSchemaGenerator::withBasePackages);

        GraphQLSchema schema = graphQLSchemaGenerator
                .withOperationsFromSingletons(typesMapperDefinitionsService)
                .generate();
        return GraphQL.newGraphQL(schema).build();
    }

}
