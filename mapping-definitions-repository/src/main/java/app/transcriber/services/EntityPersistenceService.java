package app.transcriber.services;

public interface EntityPersistenceService {

    String getCollectionName();
}
