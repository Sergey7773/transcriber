package app.transcriber.services;

import app.transcriber.entities.TypesMapperDefinitionEntity;
import com.mongodb.client.result.DeleteResult;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class TypesMapperDefinitionsServiceEntity implements EntityPersistenceService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @GraphQLQuery(name = "typesMapperDefinitionEntities")
    public Collection<TypesMapperDefinitionEntity> typesMapperDefinitions() {
        return mongoTemplate.findAll(TypesMapperDefinitionEntity.class, getCollectionName());
    }

    @GraphQLMutation(name = "saveTypesMapperDefinition")
    public TypesMapperDefinitionEntity saveTypesMapperDefinition(@GraphQLArgument(name = "input") TypesMapperDefinitionEntity typesMapperDefinitionEntity) {
        return mongoTemplate.save(typesMapperDefinitionEntity, getCollectionName());
    }

    @GraphQLMutation(name = "deleteTypesMapperDefinition")
    public TypesMapperDefinitionEntity deleteTypesMapperDefinition(@GraphQLArgument(name = "input") String id) {
        Query query = Query.query(Criteria.where("_id").is(id));
        TypesMapperDefinitionEntity currentEntity = mongoTemplate.findOne(query, TypesMapperDefinitionEntity.class, getCollectionName());
        if(currentEntity == null) {
            return null;
        }

        DeleteResult deleteResult = mongoTemplate.remove(currentEntity, getCollectionName());
        if(!deleteResult.wasAcknowledged() || deleteResult.getDeletedCount() < 1) {
            return null;
        }

        return currentEntity;
    }

    @Override
    public String getCollectionName() {
        return "TypesMapperDefinitionEntity";
    }
}
