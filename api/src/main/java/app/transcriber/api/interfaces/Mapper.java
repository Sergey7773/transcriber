package app.transcriber.api.interfaces;

import app.transcriber.api.exceptions.MappingException;

public interface Mapper<S, T> {

    T apply(S source) throws MappingException;
}
