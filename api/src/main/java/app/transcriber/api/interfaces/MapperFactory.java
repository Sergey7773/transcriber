package app.transcriber.api.interfaces;

import app.transcriber.api.exceptions.MapperCreationException;

public interface MapperFactory {

    <S, T> Mapper<S, T> createMapper(Class<S> sourceClass, Class<T> targetClass) throws MapperCreationException;

}
