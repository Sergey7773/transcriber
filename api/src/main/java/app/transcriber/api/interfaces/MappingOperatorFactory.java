package app.transcriber.api.interfaces;

import app.transcriber.api.exceptions.MappingOperatorCreationException;

import java.util.List;
import java.util.function.Function;

public interface MappingOperatorFactory {

    <T, R> Function<T, R> createOperator(String operatorName, List<Object> creationParameters) throws MappingOperatorCreationException;

}
