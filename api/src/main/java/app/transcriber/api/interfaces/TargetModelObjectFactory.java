package app.transcriber.api.interfaces;

import app.transcriber.api.exceptions.TargetModelObjectCreationException;

public interface TargetModelObjectFactory {

    <T> T create(Class<T> type) throws TargetModelObjectCreationException;
}
