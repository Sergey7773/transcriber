package app.transcriber.api.interfaces;

import app.transcriber.api.exceptions.ValueProducerCreationException;

import java.util.List;
import java.util.function.Supplier;

public interface ValueProducerFactory {

    <T> Supplier<T> createValueProducer(String producerName, List<Object> creationParameters) throws ValueProducerCreationException;

}
