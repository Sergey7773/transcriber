package app.transcriber.api.exceptions;

import app.transcriber.api.model.verification.TypesMapperGenerationInputVerificationError;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import java.util.Collection;

@AllArgsConstructor
@Getter
public class GenerationInputVerificationException extends Exception {

    @NonNull
    private final Collection<TypesMapperGenerationInputVerificationError> typedParameterGenerationInputVerificationErrors;
}
