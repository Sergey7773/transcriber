package app.transcriber.api.exceptions;

import lombok.Getter;

public class MappingException extends Exception {

    @Getter
    private final Object sourceEntity;

    public MappingException(Object sourceEntity, Exception e) {
        super(e);
        this.sourceEntity = sourceEntity;
    }
}
