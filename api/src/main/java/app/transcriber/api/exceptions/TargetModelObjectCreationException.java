package app.transcriber.api.exceptions;

import lombok.Getter;

@Getter
public class TargetModelObjectCreationException extends Exception {

    private final Class<?> targetClass;

    public TargetModelObjectCreationException(Class<?> targetClass, Exception e) {
        super(e);
        this.targetClass = targetClass;
    }
}
