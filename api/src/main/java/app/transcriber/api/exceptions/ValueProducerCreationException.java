package app.transcriber.api.exceptions;

import lombok.Getter;

import java.util.List;

@Getter
public class ValueProducerCreationException extends Exception {

    private final String valueProducerName;

    private final List<Object> parameters;

    public ValueProducerCreationException(String valueProducerName, List<Object> parameters, Exception e) {
        super(e);
        this.valueProducerName = valueProducerName;
        this.parameters = parameters;
    }

}
