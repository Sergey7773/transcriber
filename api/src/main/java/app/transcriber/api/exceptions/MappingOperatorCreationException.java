package app.transcriber.api.exceptions;

import lombok.Getter;

import java.util.List;

@Getter
public class MappingOperatorCreationException extends Exception {

    private final String operatorName;

    private final List<Object> parameters;

    public MappingOperatorCreationException(String operatorName, List<Object> parameters, Exception e) {
        super(e);
        this.operatorName = operatorName;
        this.parameters = parameters;
    }
}
