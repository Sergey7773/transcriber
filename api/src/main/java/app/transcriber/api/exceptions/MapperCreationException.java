package app.transcriber.api.exceptions;

import lombok.Getter;

@Getter
public class MapperCreationException extends Exception {

    private final Class<?> sourceClass;

    private final Class<?> targetClass;

    public MapperCreationException(Class<?> sourceClass, Class<?> targetClass, Exception e) {
        super(e);
        this.sourceClass = sourceClass;
        this.targetClass = targetClass;
    }
}
