package app.transcriber.api.model.verification;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.Collection;

@Builder
@Getter
public class FieldsValueProducerGenerationInputVerificationError implements Serializable {

    private Collection<String> message;

    private Collection<TypedParameterGenerationInputVerificationError> typedParameterGenerationInputVerificationErrors;
}
