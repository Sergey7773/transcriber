package app.transcriber.api.model.verification;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.Collection;

@Builder
@Getter
public class FieldsMapperGenerationInputVerificationError implements Serializable {

    private Collection<String> messages;

    private Collection<MappingOperatorsGenerationInputVerificationError> mappingOperatorsVerificationErrors;


}
