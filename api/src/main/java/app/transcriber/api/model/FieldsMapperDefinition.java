package app.transcriber.api.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * Allows to create a definition of mapping between two fields.
 *
 * @see TypesMapperDefinition
 */
@Data
@Accessors(chain = true)
public class FieldsMapperDefinition {

    /**
     * <p>
     *      The names of the fields in some <i>source</i> entity.
     *      We use a collection of source fields in order to allow chaining multiple getters one after the other.
     * </p>
     * <p>
     *     For example, given an entity of type <i>source</i> with a field <i>f1</i> of type <i>subEntity</i> which has a field of <i>f2</i>.
     *     We can define a mapping whose source field is <i>source.f1.f2</i>.
     * </p>
     */
    private List<String> sourceFieldNames;

    /**
     * The name of the field, whose value should be set, in some <i>target</i> entity.
     */
    private String targetFieldName;

    /**
     * Allows us to define multiple transformations which will be applied on the source field,
     * before setting its value in the target field.
     *
     */
    private List<MappingOperatorDefinition> mappingOperators;
}
