package app.transcriber.api.model.verification;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.Collection;

@Builder
@Getter
public class TypesMapperGenerationInputVerificationError implements Serializable {

    private Collection<String> messages;

    private Collection<FieldsMapperGenerationInputVerificationError> fieldMappersVerificationErrors;

    private Collection<FieldsValueProducerGenerationInputVerificationError> fieldValueProducersVerificationErrors;

    private Collection<ConstantMappingGenerationInputVerificationError> constantMappingVerificationErrors;


}
