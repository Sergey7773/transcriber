package app.transcriber.api.model;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Defines a mapping of a constanct value to some field in a <i>target</i> object.
 *
 * @see TypesMapperDefinition
 */
@Data
@Accessors(chain = true)
public class ConstantMappingDefinition {

    /**
     * The name of the field whose value we wish to set in the defined mapping
     */
    private String targetFieldName;

    /**
     * The value which we will to set in the mapping
     */
    private Object value;
}
