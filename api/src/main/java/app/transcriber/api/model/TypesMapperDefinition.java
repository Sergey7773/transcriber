package app.transcriber.api.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Collection;

/**
 * <p>
 *     Represents a definition of a mapping logic between two types. Each mapping logic may consist
 *     of the following components
 *     <ol>
 *         <ul>Zero or more fields in a source entity which are mapped to fields in the target entity</ul>
 *         <ul>Zero or more fields in the target entity which receive their value from a producer of values</ul>
 *         <ul>Zero or more fields in the target entity which receive a constant value</ul>
 *     </ol>
 * </p>
 *
 * <p>
 * Like all the <i>definition</i> classes, this class is not the actual input for the generation process,
 * but an abstraction of the data structure which is used by it.
 * This allows us to separate the following concerns
 * <ol>
 *     <li>Defining the mapping logic and required behavior</li>
 *     <li>Creating the data structures required for the actual code generation</li>
 * </ol>
 * </p>
 */
@Data
@Accessors(chain = true)
public class TypesMapperDefinition {

    /**
     * The name of the source type (fields of instances of this type will be fetched and their values mapped to fields in a target type)
     */
    private String sourceClassName;

    /**
     * The name of the target type
     */
    private String targetClassName;

    /**
     * Contains the defintions of mappings between fields in the <i>source</i> type and felds in the <i>target</i> type
     */
    private Collection<FieldsMapperDefinition> fieldMappersDefinitions;

    /**
     * Contains the definitions of mappings for fields in the <i>target</i> type, where the value is supplied by a producer logic
     */
    private Collection<FieldsValueProducerDefinition> fieldsValueProducerDefinitions;

    /**
     * Contains the definitions of mappings for fields in the <i>target</i> type, where the value is constant
     */
    private Collection<ConstantMappingDefinition> constantMappingMetadata;

}
