package app.transcriber.api.model.verification;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.Collection;

@Builder
@Getter
public class ConstantMappingGenerationInputVerificationError implements Serializable {

    private Collection<String> messages;
}
