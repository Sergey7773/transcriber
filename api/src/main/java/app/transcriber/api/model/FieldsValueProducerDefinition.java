package app.transcriber.api.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * Allows us to create a mapping definition for some field in a <i>target</i> entity,
 * where the value of the field is supplied by a supplier which the user (of this class) defines.
 *
 * @see TypesMapperDefinition
 */
@Data
@Accessors(chain = true)
public class FieldsValueProducerDefinition {

    /**
     * The name of the field whose value should be set at the end of the defined mapping logic
     */
    private String targetFieldName;

    /**
     * The name of the producer/supplier which will be used to create the value which will be set in the specified field.
     */
    private String valueProducerName;

    /**
     * The parameters required for the creation of the producer.
     */
    private List<TypedParameterDefinition> creationParameters;

}
