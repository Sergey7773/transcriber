package app.transcriber.api.model;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Responsible for creating a definition which contains a parameter's value and its type
 *
 * @see TypesMapperDefinition
 */
@Data
@Accessors(chain = true)
public class TypedParameterDefinition {

    /**
     * The name of the parameter's type
     */
    private String typeName;

    /**
     * The parameter's value
     */
    private String parameterValue;

}
