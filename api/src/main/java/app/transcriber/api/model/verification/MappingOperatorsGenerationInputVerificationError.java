package app.transcriber.api.model.verification;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.Collection;

@Builder
@Getter
public class MappingOperatorsGenerationInputVerificationError implements Serializable {

    private Collection<String> messages;

    private Collection<TypedParameterGenerationInputVerificationError> typedParameterGenerationInputVerificationErrors;
}
