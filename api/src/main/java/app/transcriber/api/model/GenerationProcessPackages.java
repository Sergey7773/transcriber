package app.transcriber.api.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.util.Collection;

@Builder
@Getter
public class GenerationProcessPackages {

    @NonNull
    private Collection<String> sourceClassesPackages;

    @NonNull
    private Collection<String> targetClassesPackages;

    @NonNull
    private Collection<String> mappingOperatorsClassesPackages;

    @NonNull
    private String outputPackage;

}
