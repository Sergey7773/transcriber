package app.transcriber.api.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * Responsible for defining a function which will be used to transform values during the mapping process.
 *
 * @see TypesMapperDefinition
 */
@Getter
@Setter
@Accessors(chain = true)
public class MappingOperatorDefinition {

    /**
     * The name of the function which will be used.
     */
    private String operatorName;

    /**
     * The parameters which we wish to use for the creation of the function.
     */
    private List<TypedParameterDefinition> creationParameters;

}
