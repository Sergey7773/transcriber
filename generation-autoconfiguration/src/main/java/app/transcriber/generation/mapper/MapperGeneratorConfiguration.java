package app.transcriber.generation.mapper;

import app.transcriber.generation.fields.MapperFieldsGenerator;
import app.transcriber.generation.mappers.MapperGenerator;
import app.transcriber.generation.methods.apply.ApplyMethodGeneratorFactory;
import app.transcriber.generation.methods.supplier.SupplierMethodGenerator;
import app.transcriber.generation.methods.supplier.map.factories.FieldsMapMethodProducerFactory;
import app.transcriber.generation.methods.supplier.produce.ValueProducerMethodProducerFactory;
import app.transcriber.generation.methods.supplier.set.ConstantSetMethodProducerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperGeneratorConfiguration {

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public MapperGenerator mapperGenerator(ApplyMethodGeneratorFactory applyMethodGeneratorFactory,
                                           SupplierMethodGenerator supplierMethodGenerator, MapperFieldsGenerator mapperFieldsGenerator) {
        return new MapperGenerator(applyMethodGeneratorFactory, supplierMethodGenerator, mapperFieldsGenerator);
    }

    @Bean
    @ConditionalOnMissingBean
    public ApplyMethodGeneratorFactory applyMethodGeneratorFactory() {
        return new ApplyMethodGeneratorFactory();
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public SupplierMethodGenerator supplierMethodGenerator(ValueProducerMethodProducerFactory valueProducerMethodProducerFactory,
                                                           FieldsMapMethodProducerFactory fieldsMapMethodProducerFactory,
                                                           ConstantSetMethodProducerFactory constantSetMethodProducerFactory) {
        return new SupplierMethodGenerator(valueProducerMethodProducerFactory,
                constantSetMethodProducerFactory, fieldsMapMethodProducerFactory);
    }

    @Bean
    @ConditionalOnMissingBean
    public MapperFieldsGenerator mapperFieldsGenerator() {
        return new MapperFieldsGenerator();
    }

}
