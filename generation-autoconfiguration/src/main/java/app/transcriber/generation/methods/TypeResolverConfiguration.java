package app.transcriber.generation.methods;

import app.transcriber.generation.model.adapters.ClassAdapter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.*;
import java.util.function.UnaryOperator;

@Configuration
public class TypeResolverConfiguration {

    public static final String COLLECTION_TYPE_RESOLVER_BEAN = "collectionTargetTypeResolver";

    @Bean(name = COLLECTION_TYPE_RESOLVER_BEAN)
    @ConditionalOnMissingBean(name = {COLLECTION_TYPE_RESOLVER_BEAN})
    public UnaryOperator<ClassAdapter> collectionTypeResolver() {
        Map<ClassAdapter, ClassAdapter> collectionInterfaceToConcreteTypeMap = new HashMap<>();
        collectionInterfaceToConcreteTypeMap.put(ClassAdapter.of(Set.class), ClassAdapter.of(HashSet.class));

        return new CollectionTypeToImplementationTypeResolver(collectionInterfaceToConcreteTypeMap, ClassAdapter.of(LinkedList.class));
    }
}
