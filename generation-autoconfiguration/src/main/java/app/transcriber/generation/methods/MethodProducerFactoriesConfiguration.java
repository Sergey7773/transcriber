package app.transcriber.generation.methods;

import app.transcriber.generation.methods.supplier.ConstantSetMethodArgumentFormatter;
import app.transcriber.generation.methods.supplier.ValueSupplierMethodProducerFactory;
import app.transcriber.generation.methods.supplier.map.factories.*;
import app.transcriber.generation.methods.supplier.map.predicates.IsCollectionTypePredicate;
import app.transcriber.generation.methods.supplier.map.predicates.IsGenericTypeWithMappedArgPredicate;
import app.transcriber.generation.methods.supplier.map.predicates.IsGenericTypeWithUnmappedPredicate;
import app.transcriber.generation.methods.supplier.map.predicates.IsMappedArgPredicate;
import app.transcriber.generation.methods.supplier.produce.ValueProducerMethodProducerFactory;
import app.transcriber.generation.methods.supplier.set.ConstantSetMethodProducerFactory;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.adapters.ClassAdapter;
import app.transcriber.tools.ArgsToCodeBlockConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.function.UnaryOperator;

@Configuration
public class MethodProducerFactoriesConfiguration {

    @Bean
    public ValueProducerMethodProducerFactory valueProducerMethodProducerFactory(@Autowired ArgsToCodeBlockConverter argsToCodeBlockConverter) {
        return new ValueProducerMethodProducerFactory(argsToCodeBlockConverter);
    }

    @Bean
    public ConstantSetMethodProducerFactory constantSetMethodProducerFactory(@Autowired ConstantSetMethodArgumentFormatter constantSetMethodArgumentFormatter) {
        return new ConstantSetMethodProducerFactory(constantSetMethodArgumentFormatter);
    }

    @Bean
    @Autowired
    public FieldsMapMethodProducerFactory fieldsMapMethodProducerFactory(ArgsToCodeBlockConverter argsToCodeBlockConverter,
                                                                         @Qualifier(TypeResolverConfiguration.COLLECTION_TYPE_RESOLVER_BEAN) UnaryOperator<ClassAdapter> collectionTargetTypeResolver) {
        Map<BiPredicate<MapperGenerationContext, FieldsMapperGenerationInput>, ValueSupplierMethodProducerFactory<FieldsMapperGenerationInput>> factoriesMap = new HashMap<>();

        factoriesMap.put(new IsCollectionTypePredicate().and(new IsGenericTypeWithMappedArgPredicate()),
                new SubEntityCollectionMapMethodGeneratorFactory(argsToCodeBlockConverter, collectionTargetTypeResolver));

        factoriesMap.put(new IsCollectionTypePredicate().and(new IsGenericTypeWithUnmappedPredicate()),
                new PrimitiveCollectionTypesMapMethodGeneratorFactory(argsToCodeBlockConverter, collectionTargetTypeResolver));

        factoriesMap.put(new IsMappedArgPredicate().and(new IsCollectionTypePredicate().negate()),
                new SubEntityMapMethodGeneratorFactory(argsToCodeBlockConverter));

        factoriesMap.put(new IsMappedArgPredicate().negate().and(new IsCollectionTypePredicate().negate()),
                new PrimitiveTypesMapMethodGeneratorFactory(argsToCodeBlockConverter));

        return new FieldsMapMethodProducerFactory(factoriesMap);
    }
}
