package app.transcriber.generation.execution;

import app.transcriber.TranscriberConfigurationPropertiesHolder;
import app.transcriber.api.model.GenerationProcessPackages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GenerationProcessPackagesConfiguration {

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public GenerationProcessPackages generationProcessPackages(TranscriberConfigurationPropertiesHolder propertiesHolder) {
        return GenerationProcessPackages.builder()
                .sourceClassesPackages(propertiesHolder.getSourceClassesPackages())
                .targetClassesPackages(propertiesHolder.getTargetClassesPackages())
                .mappingOperatorsClassesPackages(propertiesHolder.getMappingOperatorPackages())
                .outputPackage(propertiesHolder.getMappersPackage())
                .build();
    }

}
