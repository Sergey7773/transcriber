package app.transcriber.generation.execution;

import app.transcriber.api.model.GenerationProcessPackages;
import app.transcriber.generation.creation.MapperGenerationContextFactory;
import app.transcriber.generation.mappers.MapperGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TranscriberGenerationProcessConfiguration {

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public TranscriberGenerationProcess transcriberGenerationProcess(
            MapperGenerator mapperGenerator,
            MapperGenerationContextFactory mapperGenerationContextFactory,
            GenerationProcessPackages generationProcessPackages) {
        return TranscriberGenerationProcess.builder()
                .mapperGenerator(mapperGenerator)
                .generationProcessPackages(generationProcessPackages)
                .mapperGenerationContextFactory(mapperGenerationContextFactory)
                .build();
    }

}
