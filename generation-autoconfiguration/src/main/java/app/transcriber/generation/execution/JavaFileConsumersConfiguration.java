package app.transcriber.generation.execution;

import app.transcriber.TranscriberConfigurationPropertiesHolder;
import app.transcriber.api.model.GenerationProcessPackages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JavaFileConsumersConfiguration {

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public OutputFilePathCalculator outputFilePathCalculator(GenerationProcessPackages generationProcessPackages,
                                                             TranscriberConfigurationPropertiesHolder transcriberConfigurationPropertiesHolder) {
        return new OutputFilePathCalculator(transcriberConfigurationPropertiesHolder.getMapperJavaFilesDirectory());
    }
}
