package app.transcriber.tools;

import app.transcriber.api.annotations.MappingComponentName;

import java.util.Optional;
import java.util.function.Function;

public class MappingComponentNameFetcher implements Function<Class<?>, String> {

    @Override
    public String apply(Class<?> clz) {
        return Optional.of(clz)
                .map(c -> c.getAnnotation(MappingComponentName.class))
                .map(MappingComponentName::value)
                .orElse(clz.getSimpleName());
    }
}
