package app.transcriber.generation.execution;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PluginJavaFileConsumersConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public JavaFileToFileWriter javaFileToFileWriter(@Autowired OutputFilePathCalculator outputFilePathCalculator) {
        return new JavaFileToFileWriter(outputFilePathCalculator);
    }
}
