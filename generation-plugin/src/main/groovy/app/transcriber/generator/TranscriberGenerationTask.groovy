package app.transcriber.generator

import org.gradle.api.DefaultTask
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

abstract class TranscriberGenerationTask extends DefaultTask {

    @Input
    abstract ListProperty<String> getSourceClassesPackages()
    
    @Input
    abstract ListProperty<String> getTargetClassesPackages()

    @Input
    abstract Property<String> getMappingConfigFile()

    @Input
    abstract ListProperty<String> getMappingOperatorsClassesPackages()

    @Input
    abstract Property<String> getOutputPackage()

    @OutputDirectory
    abstract Property<File> getRootOutputDirectory()

    @Input
    abstract Property<String> getGenerationConfiguration()

    @Input
    abstract MapProperty<String, Object> getDebugArgs()

    TranscriberGenerationTask() {
        TranscriberGenerationPluginExtension extension = project.getExtensions().getByType(TranscriberGenerationPluginExtension)
        sourceClassesPackages.convention(extension.sourceClassesPackages)
        targetClassesPackages.convention(extension.targetClassesPackages)
        mappingConfigFile.convention(extension.mappingConfigFile)
        mappingOperatorsClassesPackages.convention(extension.mappingOperatorsClassesPackages)
        outputPackage.convention(extension.outputPackage)
        rootOutputDirectory.convention(extension.mappersOutputDirectory)
        generationConfiguration.convention(extension.generationConfiguration)
    }

    @TaskAction
    void generate() {
        Map<String, Object> debugArgsMap = Optional.ofNullable(debugArgs)
                .map({ debugArgs.getOrElse(Collections.emptyMap()) })
                .orElse(Collections.emptyMap())

        project.javaexec {
            main TranscriberGenerationSpringApplication.class.getName()
            classpath project.getConfigurations().getByName(generationConfiguration.get()).asPath

            debugOptions {
                enabled = debugArgsMap.getOrDefault('enabled', false)
                port = debugArgsMap.getOrDefault('port', 5566)
                suspend = debugArgsMap.getOrDefault('suspend', true)
                server = debugArgsMap.getOrDefault('server', true)
            }

            environment TranscriberPluginConstants.SOURCE_CLASSES_PACKAGES,
                    String.join(TranscriberPluginConstants.VALUES_DELIMITER, sourceClassesPackages.get())
            environment TranscriberPluginConstants.TARGET_CLASSES_PACKAGES,
                    String.join(TranscriberPluginConstants.VALUES_DELIMITER, targetClassesPackages.get())
            environment TranscriberPluginConstants.MAPPERS_CONFIG_FILE, mappingConfigFile.get()
            environment TranscriberPluginConstants.MAPPING_OPERATORS_PACKAGES,
                    String.join(TranscriberPluginConstants.VALUES_DELIMITER, mappingOperatorsClassesPackages.get())
            environment TranscriberPluginConstants.OUTPUT_PACKAGE, outputPackage.get()
            environment TranscriberPluginConstants.MAPPERS_OUTPUT_DIRECTORY, rootOutputDirectory.get().getPath()
        }
    }

}
