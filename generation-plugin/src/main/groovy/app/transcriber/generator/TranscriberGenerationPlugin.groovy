package app.transcriber.generator

import org.gradle.api.Plugin
import org.gradle.api.Project

class TranscriberGenerationPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.getExtensions().create('transcriberGenerationExtension', TranscriberGenerationPluginExtension)
    }
}
