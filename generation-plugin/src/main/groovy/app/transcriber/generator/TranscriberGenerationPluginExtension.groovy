package app.transcriber.generator

import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property

interface TranscriberGenerationPluginExtension {

    ListProperty<String> getSourceClassesPackages()

    ListProperty<String> getTargetClassesPackages()

    Property<String> getMappingConfigFile()

    ListProperty<String> getMappingOperatorsClassesPackages()

    Property<String> getOutputPackage()

    Property<File> getMappersOutputDirectory()

    Property<String> getGenerationConfiguration()

//    TranscriberGenerationPluginExtension(ObjectFactory objectFactory) {
//        sourceClassesPackages.convention(objectFactory.listProperty(String))
//        targetClassesPackages = objectFactory.listProperty(String)
//        mappingConfigFile = objectFactory.property(String)
//        mappingOperatorsClassesPackages = objectFactory.listProperty(String)
//        outputPackage = objectFactory.property(String)
//        mappersOutputDirectory = objectFactory.property(File)
//        generationConfiguration = objectFactory.property(String)
//    }

}
