package app.transcriber.generator;

import app.transcriber.TranscriberConfigurationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

@SpringBootApplication
@ComponentScan({"app.transcriber", "app.transcriber.*"})
public class TranscriberGenerationSpringApplication {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(TranscriberGenerationSpringApplication.class);
        Properties properties = new Properties();
        setGenerationProperty(properties, TranscriberConfigurationProperties.MAPPERS_CONFIG_FILE, System.getenv(TranscriberPluginConstants.MAPPERS_CONFIG_FILE));
        setGenerationProperties(properties, TranscriberConfigurationProperties.SOURCE_CLASSES_PACKAGES, TranscriberPluginConstants.SOURCE_CLASSES_PACKAGES);
        setGenerationProperties(properties, TranscriberConfigurationProperties.TARGET_CLASSES_PACKAGES, TranscriberPluginConstants.TARGET_CLASSES_PACKAGES);
        setGenerationProperties(properties, TranscriberConfigurationProperties.MAPPING_OPERATOR_PACKAGES, TranscriberPluginConstants.MAPPING_OPERATORS_PACKAGES);
        setGenerationProperty(properties, TranscriberConfigurationProperties.MAPPERS_PACKAGE, System.getenv(TranscriberPluginConstants.OUTPUT_PACKAGE));
        setGenerationProperty(properties, TranscriberConfigurationProperties.MAPPER_JAVA_FILES_DIRECTORY, System.getenv(TranscriberPluginConstants.MAPPERS_OUTPUT_DIRECTORY));

        springApplication.setDefaultProperties(properties);
        springApplication.run();
    }

    private static void setGenerationProperty(Properties properties, String propertyName, String value) {
        properties.setProperty(TranscriberConfigurationProperties.PROPERTIES_PREFIX + "." + propertyName, value);
    }

    private static void setGenerationProperties(Properties properties, String propertyName, String environmentVariableName) {
        setGenerationProperties(properties, propertyName, Arrays.asList(System.getenv(environmentVariableName).split(TranscriberPluginConstants.VALUES_DELIMITER)));
    }

    private static void setGenerationProperties(Properties properties, String propertyName, List<String> values) {
        setProperties(properties, TranscriberConfigurationProperties.PROPERTIES_PREFIX, propertyName, values);
    }

    private static void setProperties(Properties properties, String prefix, String propertyName, List<String> values) {
        for (int i = 0; i < values.size(); i++) {
            properties.setProperty(prefix + "." + propertyName + "[" + i + "]", values.get(i));
        }
    }
}
