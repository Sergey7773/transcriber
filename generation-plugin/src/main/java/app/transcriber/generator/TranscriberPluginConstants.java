package app.transcriber.generator;

public class TranscriberPluginConstants {

    public static final String SOURCE_CLASSES_PACKAGES = "sourceClassesPackages";

    public static final String TARGET_CLASSES_PACKAGES = "targetClassesPackages";

    public static final String MAPPING_OPERATORS_PACKAGES = "mappingOperatorsClassesPackages";

    public static final String OUTPUT_PACKAGE = "outputPackage";

    public static final String MAPPERS_OUTPUT_DIRECTORY = "mappersOutputDirectory";

    public static final String MAPPERS_CONFIG_FILE = "mappersConfigFile";

    public static final String VALUES_DELIMITER = ",";

}
