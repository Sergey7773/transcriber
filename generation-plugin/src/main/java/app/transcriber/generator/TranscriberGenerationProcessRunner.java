package app.transcriber.generator;

import app.transcriber.TranscriberConfigurationProperties;
import app.transcriber.api.model.TypesMapperDefinition;
import app.transcriber.generation.execution.JavaFileConsumer;
import app.transcriber.generation.execution.TranscriberGenerationProcess;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.javapoet.JavaFile;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.List;

@Component
@ConfigurationProperties(prefix = TranscriberConfigurationProperties.PROPERTIES_PREFIX)
public class TranscriberGenerationProcessRunner implements ApplicationRunner {

    @Autowired
    private TranscriberGenerationProcess transcriberGenerationProcess;

    @Autowired
    private Collection<JavaFileConsumer> javaFileConsumers;

    @Setter
    private String mappersConfigFile;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        File sampleConfig = new File(mappersConfigFile);
        Collection<TypesMapperDefinition> typesMapperDefinitions =
                new GsonBuilder()
                        .create().fromJson(new FileReader(sampleConfig),
                        new TypeToken<List<TypesMapperDefinition>>() {
                        }.getType());

        Collection<JavaFile> javaFiles = transcriberGenerationProcess.executeGeneration(typesMapperDefinitions);

        javaFileConsumers.forEach(javaFiles::forEach);
    }
}
