package app.transcriber.tools.autoconfiguration;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

public interface ValueProducerCacheSupplier extends Supplier<Map<String, Map<List<Object>, Supplier<?>>>> {
}
