package app.transcriber.tools.autoconfiguration;

import app.transcriber.TranscriberConfigurationPropertiesHolder;
import app.transcriber.api.interfaces.MappingOperatorFactory;
import app.transcriber.api.interfaces.TargetModelObjectFactory;
import app.transcriber.api.interfaces.ValueProducerFactory;
import app.transcriber.operator.factories.CachingMappingOperatorFactory;
import app.transcriber.operator.factories.ReflectiveMappingOperatorFactoryFactory;
import app.transcriber.producer.factories.CachingValueProducerFactory;
import app.transcriber.producer.factories.ReflectiveValueProducerFactoryFactory;
import app.transcriber.target.factories.DefaultConstructorTargetModelObjectFactory;
import app.transcriber.tools.MappingComponentNameFetcher;
import app.transcriber.tools.autoconfiguration.cache.CacheMapsConfiguration;
import app.transcriber.tools.autoconfiguration.cache.ConcurrentCacheMapsConfiguration;
import app.transcriber.tools.autoconfiguration.reloading.MapperClassReloadingServiceConfiguration;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Setter
@Configuration
@Import({CacheMapsConfiguration.class, ConcurrentCacheMapsConfiguration.class, MapperClassReloadingServiceConfiguration.class, MappersFactoryConfiguration.class})
public class ClientToolsAutoConfiguration {

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public MappingOperatorFactory mappingOperatorFactory(
            MappingOperatorCacheSupplier cacheMapFactory,
            MappingComponentNameFetcher mappingComponentNameFetcher,
            TranscriberConfigurationPropertiesHolder transcriberConfigurationPropertiesHolder) {
        return new CachingMappingOperatorFactory(
                new ReflectiveMappingOperatorFactoryFactory(mappingComponentNameFetcher).createMappingOperatorFactory(transcriberConfigurationPropertiesHolder.getMappingOperatorPackages()),
                cacheMapFactory.get());
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public ValueProducerFactory valueProducerFactory(
            ValueProducerCacheSupplier cacheMapFactory,
            MappingComponentNameFetcher mappingComponentNameFetcher,
            TranscriberConfigurationPropertiesHolder transcriberConfigurationPropertiesHolder) {
        return new CachingValueProducerFactory(
                new ReflectiveValueProducerFactoryFactory(mappingComponentNameFetcher).createValueProducersFactory(transcriberConfigurationPropertiesHolder.getValueProducerPackages()),
                cacheMapFactory.get());
    }

    @Bean
    @ConditionalOnMissingBean
    public TargetModelObjectFactory targetModelObjectFactory() {
        return new DefaultConstructorTargetModelObjectFactory();
    }

    @Bean
    @ConditionalOnMissingBean
    public MappingComponentNameFetcher mappingComponentNameFetcher() {
        return new MappingComponentNameFetcher();
    }

}