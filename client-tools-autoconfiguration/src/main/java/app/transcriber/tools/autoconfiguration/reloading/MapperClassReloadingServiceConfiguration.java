package app.transcriber.tools.autoconfiguration.reloading;

import app.transcriber.TranscriberConfigurationProperties;
import app.transcriber.TranscriberConfigurationPropertiesHolder;
import app.transcriber.generation.execution.JavaFileConsumer;
import app.transcriber.generation.execution.TranscriberGenerationProcess;
import app.transcriber.mapper.reloading.MapperClassReloadingService;
import app.transcriber.mapper.reloading.ReloadableMapperFactory;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.Collection;

@Configuration
@Setter
@Import(ReloadableMappersConfiguration.class)
@ConditionalOnProperty(prefix = TranscriberConfigurationProperties.PROPERTIES_PREFIX,
        name = TranscriberConfigurationProperties.ENABLE_MAPPERS_RELOADING, havingValue = "true")
public class MapperClassReloadingServiceConfiguration {

    @Bean
    @Autowired
    public MapperClassReloadingService mapperClassReloadingService(ReloadableMapperFactory reloadableMapperFactory,
                                                                   Collection<JavaFileConsumer> javaFileConsumers,
                                                                   TranscriberGenerationProcess transcriberGenerationProcess,
                                                                   TranscriberConfigurationPropertiesHolder transcriberConfigurationPropertiesHolder) {
        return MapperClassReloadingService.builder()
                .transcriberGenerationProcess(transcriberGenerationProcess)
                .mapperJavaFilesDirectory(transcriberConfigurationPropertiesHolder.getMapperJavaFilesDirectory())
                .mapperClassesDirectory(transcriberConfigurationPropertiesHolder.getMapperClassFilesDirectory())
                .javaFileConsumers(javaFileConsumers)
                .reloadableMapperFactory(reloadableMapperFactory)
                .build();
    }
}
