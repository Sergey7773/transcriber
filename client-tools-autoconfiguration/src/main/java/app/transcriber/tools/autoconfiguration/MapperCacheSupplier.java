package app.transcriber.tools.autoconfiguration;

import app.transcriber.api.interfaces.Mapper;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

public interface MapperCacheSupplier extends Supplier<Map<List<Class<?>>, Mapper<?, ?>>> {
}
