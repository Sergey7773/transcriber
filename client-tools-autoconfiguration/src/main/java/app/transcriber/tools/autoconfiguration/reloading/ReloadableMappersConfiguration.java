package app.transcriber.tools.autoconfiguration.reloading;

import app.transcriber.TranscriberConfigurationProperties;
import app.transcriber.TranscriberConfigurationPropertiesHolder;
import app.transcriber.api.interfaces.MapperFactory;
import app.transcriber.api.interfaces.MappingOperatorFactory;
import app.transcriber.api.interfaces.TargetModelObjectFactory;
import app.transcriber.api.interfaces.ValueProducerFactory;
import app.transcriber.mapper.factories.CachingMapperFactory;
import app.transcriber.mapper.factories.ReflectiveMapperFactoryFactory;
import app.transcriber.mapper.reloading.ReloadableMapperFactory;
import app.transcriber.tools.autoconfiguration.MapperCacheSupplier;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.UnaryOperator;

@Configuration
@Setter
@ConditionalOnProperty(prefix = TranscriberConfigurationProperties.PROPERTIES_PREFIX,
        name = TranscriberConfigurationProperties.ENABLE_MAPPERS_RELOADING, havingValue = "true")
public class ReloadableMappersConfiguration {

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public ReloadableMapperFactory mapperFactory(MappingOperatorFactory mappingOperatorFactory,
                                                 ValueProducerFactory valueProducerFactory,
                                                 MapperCacheSupplier cacheMapFactory,
                                                 TargetModelObjectFactory targetModelObjectFactory,
                                                 TranscriberConfigurationPropertiesHolder transcriberConfigurationPropertiesHolder) {
        UnaryOperator<MapperFactory> factoryDecorator = factory -> new CachingMapperFactory(factory, cacheMapFactory.get());

        return new ReloadableMapperFactory(new ReflectiveMapperFactoryFactory(mappingOperatorFactory, valueProducerFactory, targetModelObjectFactory),
                factoryDecorator, transcriberConfigurationPropertiesHolder.getMappersPackage());
    }
}
