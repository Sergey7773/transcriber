package app.transcriber.tools.autoconfiguration;

import app.transcriber.TranscriberConfigurationProperties;
import app.transcriber.TranscriberConfigurationPropertiesHolder;
import app.transcriber.api.interfaces.MapperFactory;
import app.transcriber.api.interfaces.MappingOperatorFactory;
import app.transcriber.api.interfaces.TargetModelObjectFactory;
import app.transcriber.api.interfaces.ValueProducerFactory;
import app.transcriber.mapper.factories.CachingMapperFactory;
import app.transcriber.mapper.factories.ReflectiveMapperFactoryFactory;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Setter
@ConditionalOnProperty(prefix = TranscriberConfigurationProperties.PROPERTIES_PREFIX,
        name = TranscriberConfigurationProperties.ENABLE_MAPPERS_RELOADING, havingValue = "false", matchIfMissing = true)
public class MappersFactoryConfiguration {

    @Bean
    @Autowired
    @ConditionalOnMissingBean
    public MapperFactory mapperFactory(MappingOperatorFactory mappingOperatorFactory,
                                       ValueProducerFactory valueProducerFactory,
                                       MapperCacheSupplier cacheMapFactory,
                                       TargetModelObjectFactory targetModelObjectFactory,
                                       TranscriberConfigurationPropertiesHolder transcriberConfigurationPropertiesHolder) {
        return new CachingMapperFactory(
                new ReflectiveMapperFactoryFactory(mappingOperatorFactory, valueProducerFactory, targetModelObjectFactory).createReflectiveMapperFactory(transcriberConfigurationPropertiesHolder.getMappersPackage()),
                cacheMapFactory.get());
    }

}
