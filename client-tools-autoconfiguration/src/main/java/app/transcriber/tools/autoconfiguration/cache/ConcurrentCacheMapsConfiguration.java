package app.transcriber.tools.autoconfiguration.cache;

import app.transcriber.TranscriberConfigurationProperties;
import app.transcriber.tools.autoconfiguration.MapperCacheSupplier;
import app.transcriber.tools.autoconfiguration.MappingOperatorCacheSupplier;
import app.transcriber.tools.autoconfiguration.ValueProducerCacheSupplier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.concurrent.ConcurrentHashMap;

@Configuration
@ConditionalOnProperty(prefix = TranscriberConfigurationProperties.PROPERTIES_PREFIX,
        name = TranscriberConfigurationProperties.CONCURRENT_CACHES_PROPERTY_NAME, havingValue = "true", matchIfMissing = true)
public class ConcurrentCacheMapsConfiguration {

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public MapperCacheSupplier mapperCacheSupplier() {
        return ConcurrentHashMap::new;
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public MappingOperatorCacheSupplier mappingOperatorCacheSupplier() {
        return ConcurrentHashMap::new;
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public ValueProducerCacheSupplier valueProducerCacheSupplier() {
        return ConcurrentHashMap::new;
    }

}
