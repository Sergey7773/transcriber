package app.transcriber.operator.factories;

import app.transcriber.ReflectiveFactory;
import app.transcriber.api.annotations.MappingOperator;
import lombok.AllArgsConstructor;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@AllArgsConstructor
public class ReflectiveMappingOperatorFactoryFactory {

    private Function<Class<?>, String> operatorNameFetcher;

    public ReflectiveMappingOperatorFactory createMappingOperatorFactory(Collection<String> mappingOperatorsPackage) {

        Map<String, Class<? extends Function<?, ?>>> operatorClassesMap = new HashMap<>();

        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();

        mappingOperatorsPackage.stream()
                .map(ClasspathHelper::forPackage)
                .forEach(configurationBuilder::addUrls);

        Reflections reflections = new Reflections(configurationBuilder);

        reflections.getTypesAnnotatedWith(MappingOperator.class).stream()
                .filter(Function.class::isAssignableFrom)
                .forEach(clz -> operatorClassesMap.put(operatorNameFetcher.apply(clz),
                        (Class<? extends Function<?, ?>>) clz));

        return new ReflectiveMappingOperatorFactory(new ReflectiveFactory<>(operatorClassesMap));
    }

}
