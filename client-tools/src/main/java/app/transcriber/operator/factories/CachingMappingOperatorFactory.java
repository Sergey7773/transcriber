package app.transcriber.operator.factories;

import app.transcriber.api.exceptions.MappingOperatorCreationException;
import app.transcriber.api.interfaces.MappingOperatorFactory;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.*;
import java.util.function.Function;

@AllArgsConstructor
public class CachingMappingOperatorFactory implements MappingOperatorFactory {

    @NonNull
    private MappingOperatorFactory nestedOperatorFactory;

    @NonNull
    private Map<String, Map<List<Object>, Function<?, ?>>> operatorsCache;

    @Override
    public <T, R> Function<T, R> createOperator(String operatorName, List<Object> creationParameters) throws MappingOperatorCreationException {
        Map<List<Object>, Function<?, ?>> parametersToOperatorsMap = operatorsCache.computeIfAbsent(operatorName, s-> new HashMap<>());

        List<Object> key = Optional.ofNullable(creationParameters).orElse(Collections.emptyList());

        if (!parametersToOperatorsMap.containsKey(key)) {
            parametersToOperatorsMap.put(key, nestedOperatorFactory.createOperator(operatorName, creationParameters));
        }

        return (Function<T, R>) parametersToOperatorsMap.get(key);
    }
}
