package app.transcriber.operator.factories;

import app.transcriber.ReflectiveFactory;
import app.transcriber.api.exceptions.MappingOperatorCreationException;
import app.transcriber.api.interfaces.MappingOperatorFactory;
import lombok.AllArgsConstructor;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.function.Function;

@AllArgsConstructor
public class ReflectiveMappingOperatorFactory implements MappingOperatorFactory {

    private ReflectiveFactory<Function<?, ?>> reflectiveFactory;

    @Override
    public <T, R> Function<T, R> createOperator(String operatorName, List<Object> creationParameters)
            throws MappingOperatorCreationException {
        try {
            return (Function<T, R>) reflectiveFactory.create(operatorName, creationParameters);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            throw new MappingOperatorCreationException(operatorName, creationParameters, e);
        }

    }
}
