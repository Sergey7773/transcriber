package app.transcriber.operator.converters;

import app.transcriber.api.annotations.MappingOperator;

import java.util.Objects;
import java.util.function.Function;

@MappingOperator
public class ToDoubleConverter implements Function<Object, Double> {

    @Override
    public Double apply(Object o) {
        return Double.valueOf(Objects.toString(o));
    }
}
