package app.transcriber.operator.converters;

import app.transcriber.api.annotations.MappingOperator;

import java.util.Objects;
import java.util.function.Function;

@MappingOperator
public class ToIntegerConverter implements Function<Object, Integer> {

    @Override
    public Integer apply(Object o) {
        return Integer.valueOf(Objects.toString(o));
    }
}
