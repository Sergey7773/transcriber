package app.transcriber.operator.converters;

import app.transcriber.api.annotations.MappingOperator;

import java.util.function.Function;

@MappingOperator
public class ToBooleanConverter implements Function<Object, Boolean> {

    @Override
    public Boolean apply(Object o) {
        return Boolean.valueOf(String.valueOf(o));
    }
}
