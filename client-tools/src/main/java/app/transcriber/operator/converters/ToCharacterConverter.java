package app.transcriber.operator.converters;

import app.transcriber.api.annotations.MappingOperator;

import java.util.Objects;
import java.util.function.Function;

@MappingOperator
public class ToCharacterConverter implements Function<Object, Character> {

    @Override
    public Character apply(Object o) {
        return Character.valueOf(Objects.toString(o).charAt(0));
    }
}
