package app.transcriber.operator.converters;

import app.transcriber.api.annotations.MappingOperator;

import java.util.Objects;
import java.util.function.Function;

@MappingOperator
public class ToFloatConverter implements Function<Object, Float> {

    @Override
    public Float apply(Object o) {
        return Float.valueOf(Objects.toString(o));
    }
}
