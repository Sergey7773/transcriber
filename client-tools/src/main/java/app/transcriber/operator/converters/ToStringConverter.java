package app.transcriber.operator.converters;

import app.transcriber.api.annotations.MappingOperator;

import java.util.Objects;
import java.util.function.Function;

@MappingOperator
public class ToStringConverter implements Function<Object, String> {

    @Override
    public String apply(Object o) {
        return Objects.toString(o);
    }
}
