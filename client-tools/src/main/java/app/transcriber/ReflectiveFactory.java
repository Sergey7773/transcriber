package app.transcriber;

import lombok.AllArgsConstructor;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
public class ReflectiveFactory<T> {

    private Map<String, Class<? extends T>> namesToClassesMap;

    public T create(String name, List<Object> creationParameters) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        List<Object> parameters = Optional.ofNullable(creationParameters).orElse(Collections.emptyList());

        Class<?>[] parameterTypes = new Class<?>[parameters.size()];
        parameters.stream().map(Object::getClass).collect(Collectors.toList()).toArray(parameterTypes);

        Object[] parametersArray = parameters.toArray();

        return namesToClassesMap.get(name).getConstructor(parameterTypes).newInstance(parametersArray);
    }
}
