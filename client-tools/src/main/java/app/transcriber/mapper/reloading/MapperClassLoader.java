package app.transcriber.mapper.reloading;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Objects;

public class MapperClassLoader extends ClassLoader {

    private String mappersPackage;

    private String mapperClassDirectory;

    public MapperClassLoader(String mappersPackage, String mapperClassDirectory, ClassLoader parent) {
        super(parent);
        this.mappersPackage = mappersPackage;
        this.mapperClassDirectory = mapperClassDirectory;
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        if (Objects.isNull(name) || Objects.isNull(mapperClassDirectory) || !name.startsWith(mappersPackage)) {
            return super.loadClass(name);
        }

        Class<?> loadedClass = findLoadedClass(name);

        if(Objects.nonNull(loadedClass)) {
            return loadedClass;
        }

        URLConnection connection = openUrlConnection(name);

        try (InputStream input = connection.getInputStream()) {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int data = input.read();

            while (data != -1) {
                buffer.write(data);
                data = input.read();
            }

            byte[] classData = buffer.toByteArray();

            return defineClass(name,
                    classData, 0, classData.length);

        } catch (Exception e) {
            throw new ClassNotFoundException(e.getMessage(), e);
        }
    }

    private URLConnection openUrlConnection(String className) throws ClassNotFoundException {
        try {
            File classFile = new File(mapperClassDirectory + File.separator + className.replace(".", File.separator) + ".class");
            URL url = classFile.toURI().toURL();
            return url.openConnection();
        } catch (Exception e) {
            throw new ClassNotFoundException(e.getMessage(), e);
        }
    }
}
