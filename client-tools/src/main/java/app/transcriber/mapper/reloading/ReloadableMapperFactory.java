package app.transcriber.mapper.reloading;

import app.transcriber.api.annotations.MapperMetadata;
import app.transcriber.api.exceptions.MapperCreationException;
import app.transcriber.api.interfaces.Mapper;
import app.transcriber.api.interfaces.MapperFactory;
import app.transcriber.mapper.factories.ReflectiveMapperFactoryFactory;
import org.reflections.Reflections;

import java.util.Collection;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.UnaryOperator;

public class ReloadableMapperFactory implements MapperFactory {

    private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    private ReflectiveMapperFactoryFactory reflectiveMapperFactoryFactory;

    private UnaryOperator<MapperFactory> mapperFactoryDecorator;

    private String mappersPackage;

    private MapperFactory nestedFactory;

    public ReloadableMapperFactory(ReflectiveMapperFactoryFactory reflectiveMapperFactoryFactory,
                                   UnaryOperator<MapperFactory> mapperFactoryDecorator,
                                   String mappersPackage) {
        this.mappersPackage = mappersPackage;
        this.reflectiveMapperFactoryFactory = reflectiveMapperFactoryFactory;
        this.mapperFactoryDecorator = mapperFactoryDecorator;
        this.nestedFactory = mapperFactoryDecorator.apply(reflectiveMapperFactoryFactory.createReflectiveMapperFactory(loadMapperClasses()));
    }

    public ReloadableMapperFactory(ReflectiveMapperFactoryFactory reflectiveMapperFactoryFactory,
                                   String mappersPackage) {
        this(reflectiveMapperFactoryFactory, factory -> factory, mappersPackage);
    }

    @Override
    public <S, T> Mapper<S, T> createMapper(Class<S> sourceClass, Class<T> targetClass) throws MapperCreationException {
        try {
            readWriteLock.readLock().lock();
            return nestedFactory.createMapper(sourceClass, targetClass);
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    public void reloadMapperClasses(String mappersSourceDir) {
        try {
            readWriteLock.writeLock().lock();
            nestedFactory = mapperFactoryDecorator.apply(reflectiveMapperFactoryFactory.createReflectiveMapperFactory(loadMapperClasses(mappersSourceDir)));
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    private final Collection<Class<?>> loadMapperClasses(String mappersSourceDir) {
        ClassLoader classLoader = new MapperClassLoader(mappersPackage, mappersSourceDir, MapperClassLoader.class.getClassLoader());
        Reflections reflections = new Reflections(mappersPackage, classLoader);
        return reflections.getTypesAnnotatedWith(MapperMetadata.class);
    }

    private final Collection<Class<?>> loadMapperClasses() {
        return loadMapperClasses(null);
    }
}
