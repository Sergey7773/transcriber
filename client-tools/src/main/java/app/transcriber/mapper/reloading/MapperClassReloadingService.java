package app.transcriber.mapper.reloading;

import app.transcriber.api.exceptions.GenerationInputVerificationException;
import app.transcriber.api.model.TypesMapperDefinition;
import app.transcriber.generation.execution.JavaFileConsumer;
import app.transcriber.generation.execution.TranscriberGenerationProcess;
import com.squareup.javapoet.JavaFile;
import lombok.Builder;
import lombok.NonNull;

import java.util.Collection;

@Builder
public class MapperClassReloadingService {

    @NonNull
    private String mapperClassesDirectory;

    @NonNull
    private String mapperJavaFilesDirectory;

    @NonNull
    private ReloadableMapperFactory reloadableMapperFactory;

    @NonNull
    private TranscriberGenerationProcess transcriberGenerationProcess;

    @NonNull
    private Collection<JavaFileConsumer> javaFileConsumers;

    public void reloadMappers(@NonNull Collection<TypesMapperDefinition> mapperDefinitions) throws GenerationInputVerificationException {
        Collection<JavaFile> generatedMappers = transcriberGenerationProcess.executeGeneration(mapperDefinitions);
        javaFileConsumers.forEach(generatedMappers::forEach);
        reloadableMapperFactory.reloadMapperClasses(mapperClassesDirectory);
    }

}