package app.transcriber.mapper.factories;

import app.transcriber.api.exceptions.MapperCreationException;
import app.transcriber.api.interfaces.Mapper;
import app.transcriber.api.interfaces.MapperFactory;
import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class CachingMapperFactory implements MapperFactory {

    private MapperFactory nestedFactory;

    private Map<List<Class<?>>, Mapper<?, ?>> resultsMap;

    @Override
    public <S, T> Mapper<S, T> createMapper(Class<S> sourceClass, Class<T> targetClass) throws MapperCreationException {
        List<Class<?>> resultsMapKey = Arrays.asList(sourceClass, targetClass);

        if (!resultsMap.containsKey(resultsMapKey)) {
            resultsMap.put(resultsMapKey, nestedFactory.createMapper(sourceClass, targetClass));
        }

        return (Mapper<S, T>) resultsMap.get(resultsMapKey);
    }

}
