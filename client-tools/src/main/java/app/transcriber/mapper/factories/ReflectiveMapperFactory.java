package app.transcriber.mapper.factories;

import app.transcriber.api.exceptions.MapperCreationException;
import app.transcriber.api.interfaces.*;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ReflectiveMapperFactory implements MapperFactory {

    private Map<String, Map<String, Class<? extends Mapper<?, ?>>>> mapperClassesMap;

    private Map<Class<?>, Object> constructorArgTypeToValueMap;

    public ReflectiveMapperFactory(MappingOperatorFactory mappingOperatorFactory, ValueProducerFactory valueProducerFactory,
                                   TargetModelObjectFactory targetModelObjectFactory,
                                   Map<String, Map<String, Class<? extends Mapper<?, ?>>>> mapperClassesMap) {
        this.mapperClassesMap = mapperClassesMap;

        constructorArgTypeToValueMap = new HashMap<>();
        constructorArgTypeToValueMap.put(MappingOperatorFactory.class, mappingOperatorFactory);
        constructorArgTypeToValueMap.put(ValueProducerFactory.class, valueProducerFactory);
        constructorArgTypeToValueMap.put(MapperFactory.class, this);
        constructorArgTypeToValueMap.put(TargetModelObjectFactory.class, targetModelObjectFactory);
    }

    @Override
    public <S, T> Mapper<S, T> createMapper(Class<S> sourceClass, Class<T> targetClass) throws MapperCreationException {
        try {
            Class<?> mapperClass = mapperClassesMap.get(sourceClass.getSimpleName())
                    .get(targetClass.getSimpleName());

            if (mapperClass.getConstructors().length == 0) {
                return (Mapper<S, T>) mapperClass.newInstance();
            }

            Constructor<?> constructor = mapperClass.getConstructors()[0];
            Class<?>[] constructorParameterTypes = constructor.getParameterTypes();

            Object[] constructorParameters = Arrays.asList(constructorParameterTypes).stream()
                    .map(constructorArgTypeToValueMap::get)
                    .collect(Collectors.toList())
                    .toArray();

            return (Mapper<S, T>)
                    constructor.newInstance(constructorParameters);
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            throw new MapperCreationException(sourceClass, targetClass, e);
        }
    }
}
