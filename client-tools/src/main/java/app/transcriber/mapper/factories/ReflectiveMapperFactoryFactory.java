package app.transcriber.mapper.factories;

import app.transcriber.api.annotations.MapperMetadata;
import app.transcriber.api.interfaces.Mapper;
import app.transcriber.api.interfaces.MappingOperatorFactory;
import app.transcriber.api.interfaces.TargetModelObjectFactory;
import app.transcriber.api.interfaces.ValueProducerFactory;
import lombok.AllArgsConstructor;
import org.reflections.Reflections;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@AllArgsConstructor
public class ReflectiveMapperFactoryFactory {

    private MappingOperatorFactory mappingOperatorFactory;

    private ValueProducerFactory valueProducerFactory;

    private TargetModelObjectFactory targetModelObjectFactory;

    public ReflectiveMapperFactory createReflectiveMapperFactory(String mapperPackage) {

        Reflections reflections = new Reflections(mapperPackage);

        return createReflectiveMapperFactory(reflections.getTypesAnnotatedWith(MapperMetadata.class));
    }

    public ReflectiveMapperFactory createReflectiveMapperFactory(Collection<Class<?>> mapperClasses) {
        Map<String, Map<String, Class<? extends Mapper<?, ?>>>> mapperClassesMap = new HashMap<>();

        mapperClasses.stream()
                .filter(mapperClass -> Objects.nonNull(mapperClass.getAnnotation(MapperMetadata.class)))
                .forEach(mapperClass -> {
                    Class<?> sourceTypeName = mapperClass.getAnnotation(MapperMetadata.class).sourceClass();
                    Class<?> targetTypeName = mapperClass.getAnnotation(MapperMetadata.class).targetClass();
                    mapperClassesMap.putIfAbsent(sourceTypeName.getSimpleName(), new HashMap<>());
                    mapperClassesMap.get(sourceTypeName.getSimpleName()).putIfAbsent(targetTypeName.getSimpleName(), (Class<? extends Mapper<?, ?>>) mapperClass);
                });

        return new ReflectiveMapperFactory(mappingOperatorFactory, valueProducerFactory, targetModelObjectFactory, mapperClassesMap);
    }
}
