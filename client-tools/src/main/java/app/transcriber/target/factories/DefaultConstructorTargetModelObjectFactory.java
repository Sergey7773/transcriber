package app.transcriber.target.factories;

import app.transcriber.api.exceptions.TargetModelObjectCreationException;
import app.transcriber.api.interfaces.TargetModelObjectFactory;

public class DefaultConstructorTargetModelObjectFactory implements TargetModelObjectFactory {

    @Override
    public <T> T create(Class<T> type) throws TargetModelObjectCreationException {
        try {
            return type.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new TargetModelObjectCreationException(type, e);
        }
    }
}
