package app.transcriber.producer.factories;

import app.transcriber.ReflectiveFactory;
import app.transcriber.api.exceptions.ValueProducerCreationException;
import app.transcriber.api.interfaces.ValueProducerFactory;
import lombok.AllArgsConstructor;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.function.Supplier;

@AllArgsConstructor
public class ReflectiveValueProducerFactory implements ValueProducerFactory {

    private ReflectiveFactory<Supplier<?>> reflectiveFactory;

    @Override
    public <T> Supplier<T> createValueProducer(String producerName, List<Object> creationParameters)
            throws ValueProducerCreationException {
        try {
            return (Supplier<T>) reflectiveFactory.create(producerName, creationParameters);
        } catch (InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {
            throw new ValueProducerCreationException(producerName, creationParameters, e);
        }
    }
}
