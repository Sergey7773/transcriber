package app.transcriber.producer.factories;

import app.transcriber.ReflectiveFactory;
import app.transcriber.api.annotations.ValuesProducer;
import lombok.AllArgsConstructor;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

@AllArgsConstructor
public class ReflectiveValueProducerFactoryFactory {

    private Function<Class<?>, String> producerNameFetcher;

    public ReflectiveValueProducerFactory createValueProducersFactory(Collection<String> valueProducersPackageName) {

        Map<String, Class<? extends Supplier<?>>> producerClassesMap = new HashMap<>();

        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();

        valueProducersPackageName.stream()
                .map(ClasspathHelper::forPackage)
                .forEach(configurationBuilder::addUrls);

        Reflections reflections = new Reflections(configurationBuilder);

        reflections.getTypesAnnotatedWith(ValuesProducer.class).stream()
                .filter(Supplier.class::isAssignableFrom)
                .forEach(clz -> producerClassesMap.put(producerNameFetcher.apply(clz),
                        (Class<? extends Supplier<?>>) clz));

        return new ReflectiveValueProducerFactory(new ReflectiveFactory<>(producerClassesMap));

    }

}
