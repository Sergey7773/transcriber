package app.transcriber.producer.factories;

import app.transcriber.api.exceptions.ValueProducerCreationException;
import app.transcriber.api.interfaces.ValueProducerFactory;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.*;
import java.util.function.Supplier;

@AllArgsConstructor
public class CachingValueProducerFactory implements ValueProducerFactory {

    @NonNull
    private ValueProducerFactory nestedProducersFactory;

    @NonNull
    private Map<String, Map<List<Object>, Supplier<?>>> producersCache;

    @Override
    public <T> Supplier<T> createValueProducer(String producerName, List<Object> creationParameters) throws ValueProducerCreationException {
        Map<List<Object>, Supplier<?>> parametersToOperatorsMap = producersCache.computeIfAbsent(producerName, s -> new HashMap<>());

        List<Object> key = Optional.ofNullable(creationParameters).orElse(Collections.emptyList());

        if (!parametersToOperatorsMap.containsKey(key)) {
            parametersToOperatorsMap.put(key, nestedProducersFactory.createValueProducer(producerName, creationParameters));
        }

        return (Supplier<T>) parametersToOperatorsMap.get(key);
    }
}
