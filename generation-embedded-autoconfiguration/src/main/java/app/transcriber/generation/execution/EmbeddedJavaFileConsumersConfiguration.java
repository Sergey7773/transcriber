package app.transcriber.generation.execution;

import app.transcriber.TranscriberConfigurationPropertiesHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

@Configuration
public class EmbeddedJavaFileConsumersConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public CompilingJavaFileConsumer compilingJavaFileConsumer(@Autowired OutputFilePathCalculator outputFilePathCalculator,
                                                               @Autowired JavaCompiler javaCompiler,
                                                               @Autowired TranscriberConfigurationPropertiesHolder transcriberConfigurationPropertiesHolder) {
        return new CompilingJavaFileConsumer(outputFilePathCalculator,
                transcriberConfigurationPropertiesHolder.getMapperClassFilesDirectory(), javaCompiler);
    }

    @Bean
    @ConditionalOnMissingBean
    public JavaCompiler javaCompiler() {
        return ToolProvider.getSystemJavaCompiler();
    }
}
