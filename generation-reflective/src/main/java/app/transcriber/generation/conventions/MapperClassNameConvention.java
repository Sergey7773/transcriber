package app.transcriber.generation.conventions;

import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.tools.ListAccessUtils;

import java.lang.reflect.Field;
import java.util.Collection;

public class MapperClassNameConvention {

    public String getByTypesMapperMetadata(TypesMapperGenerationInput typesMapperGenerationInput) {
        return getMapperClassName(typesMapperGenerationInput.getSourceClass().getSimpleName(),
                typesMapperGenerationInput.getTargetClass().getSimpleName());
    }

    public String getByFieldsMapperMetadata(FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        Field lastFieldSourceInList = ListAccessUtils.getLastElementOf(fieldsMapperGenerationInput.getSourceFieldsSources())
                .orElseThrow(() -> new IllegalArgumentException("Source fields list must not be empty"));

        if (Collection.class.isAssignableFrom(lastFieldSourceInList.getType())) {
            return getMapperClassName(getFirstTypeArgumentName(lastFieldSourceInList.getType()),
                    getFirstTypeArgumentName(fieldsMapperGenerationInput.getTargetFieldSource().getType()));
        }

        return getMapperClassName(lastFieldSourceInList.getType().getName(),
                fieldsMapperGenerationInput.getTargetFieldSource().getType().getName());
    }

    private String getFirstTypeArgumentName(Class<?> clz) {
        return (clz.getTypeParameters()[0]).getName();
    }

    private String getMapperClassName(String sourceTypeName, String targetTypeName) {
        return new StringBuilder()
                .append(sourceTypeName)
                .append("To")
                .append(targetTypeName)
                .append("Mapper")
                .toString();
    }

}
