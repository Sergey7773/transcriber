package app.transcriber.generation.model;

import app.transcriber.generation.conventions.*;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class TestsNamingConventionContext {

    private MapperClassNameConvention mapperClassNameConvention;
    private ApplyTestMethodNameConvention applyTestMethodNameConventionl;
    private AllArgsConstructorTestNameConvention allArgsConstructorTestNameConvention;
    private MapTestMethodNameConvention mapTestMethodNameConvention;
    private ProduceTestMethodNameConvention produceTestMethodNameConvention;
    private SetConstantValueTestMethodNameConvention setConstantValueTestMethodNameConvention;

}
