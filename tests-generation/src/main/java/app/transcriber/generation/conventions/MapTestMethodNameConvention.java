package app.transcriber.generation.conventions;

import app.transcriber.generation.model.FieldsMapperGenerationInput;
import lombok.AllArgsConstructor;

import java.util.Objects;
import java.util.function.Function;

@AllArgsConstructor
public class MapTestMethodNameConvention {

    private final Function<FieldsMapperGenerationInput, String> mapMethodNameConvention;

    public String apply(FieldsMapperGenerationInput fieldsMapperGenerationInput, Class<?> exceptionToBeThrown, boolean verifySetterMethodTestNeeded, boolean verifyTwiceSetterMethodTestNeeded) {

        if (exceptionToBeThrown == null) {
            if (verifySetterMethodTestNeeded) {
                return getMapMethodName(fieldsMapperGenerationInput);
            }

            if (verifyTwiceSetterMethodTestNeeded) {
                return getMapMethodNameForDoubleVerification(fieldsMapperGenerationInput);
            }
        }
        return getMapMethodNameForThrowingException(fieldsMapperGenerationInput, Objects.requireNonNull(exceptionToBeThrown).getSimpleName());
    }

    private String getMapMethodName(FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        return mapMethodNameConvention.apply(fieldsMapperGenerationInput) + "_onMockedCorrectInput_verifySetterMethod";
    }

    private String getMapMethodNameForThrowingException(FieldsMapperGenerationInput fieldsMapperGenerationInput, String exceptionToBeThrown) {
        return mapMethodNameConvention.apply(fieldsMapperGenerationInput) + "_throws" + exceptionToBeThrown;
    }

    private String getMapMethodNameForDoubleVerification(FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        return mapMethodNameConvention.apply(fieldsMapperGenerationInput) + "_onMockedCorrectInput_verifyMappingForMultipleEntities";
    }
}
