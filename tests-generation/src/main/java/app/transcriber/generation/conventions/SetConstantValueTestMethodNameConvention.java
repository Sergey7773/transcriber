package app.transcriber.generation.conventions;

import app.transcriber.generation.model.ConstantMappingGenerationInput;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SetConstantValueTestMethodNameConvention {

    private final SetConstantValueMethodNameConvention setConstantValueMethodNameConvention;

    public String apply(ConstantMappingGenerationInput constantMappingGenerationInput) {
        return setConstantValueMethodNameConvention.apply(constantMappingGenerationInput)
                + "_onMockedCorrectInput_verifySetterMethod";
    }
}
