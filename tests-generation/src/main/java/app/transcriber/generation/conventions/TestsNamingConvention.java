package app.transcriber.generation.conventions;

public enum TestsNamingConvention {

    SOURCE_MOCKED_OBJECT("mockedSource"),
    TARGET_MOCKED_OBJECT("mockedTarget"),
    TARGET_SUB_ENTITY_MOCKED("mockedTargetSubEntity"),
    SOURCE_SUB_ENTITY_MOCKED("mockedSourceSubEntity"),
    SOURCE_SUB_ENTITY_COLLECTION("subEntitiesSourceCol"),
    SOURCE_FIELD_VALUE("sourceFieldValue"),
    MAPPING_OPERATOR_STUB("mappingOperatorStub"),
    EXPECTED_RESULT_OF_OPERATORS("expectedResultOfOperators");

    private String convention;

    TestsNamingConvention(String convention) {
        this.convention = convention;
    }

    @Override
    public String toString() {
        return this.convention;
    }
}
