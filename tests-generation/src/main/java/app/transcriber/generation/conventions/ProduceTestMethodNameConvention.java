package app.transcriber.generation.conventions;

import app.transcriber.generation.model.FieldsValueProducerGenerationInput;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ProduceTestMethodNameConvention {

    private final ProduceMethodNameConvention produceMethodNameConvention;

    public String apply(FieldsValueProducerGenerationInput fieldsValueProducerGenerationInput, Class<?> exceptionToBeThrown) {
        String produceMethodName = produceMethodNameConvention.apply(fieldsValueProducerGenerationInput);

        if (exceptionToBeThrown == null) {
            return produceMethodName + "_onMockedCorrectInput_verifySetterMethod";
        }

        return produceMethodName + "_throws" + exceptionToBeThrown.getSimpleName();
    }
}
