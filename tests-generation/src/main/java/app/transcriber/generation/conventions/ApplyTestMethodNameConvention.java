package app.transcriber.generation.conventions;

import java.util.function.UnaryOperator;

public class ApplyTestMethodNameConvention implements UnaryOperator<String> {

    @Override
    public String apply(String methodName) {
        return NamingConvention.OPERATOR_APPLY_METHOD.toString()
                + "_doNothingWhenCallingMapMethods_" + methodName + "WasInvoked";
    }
}