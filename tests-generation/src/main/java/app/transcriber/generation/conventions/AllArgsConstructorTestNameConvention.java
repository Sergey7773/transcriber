package app.transcriber.generation.conventions;

import org.apache.commons.lang3.StringUtils;

import java.util.function.BiFunction;

public class AllArgsConstructorTestNameConvention implements BiFunction<String, Class<? extends Exception>, String> {

    @Override
    public String apply(String nullFieldName, Class<? extends Exception> expectedExceptionType) {
        return "allArgsConstructor_" + "onNull" + StringUtils.capitalize(nullFieldName) + "_throws" + expectedExceptionType.getSimpleName();
    }
}