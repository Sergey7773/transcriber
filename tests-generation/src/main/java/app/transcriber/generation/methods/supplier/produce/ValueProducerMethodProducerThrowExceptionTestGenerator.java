package app.transcriber.generation.methods.supplier.produce;

import app.transcriber.api.exceptions.ValueProducerCreationException;
import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.model.FieldsValueProducerGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.CodeBlock;
import org.mockito.Mockito;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;


public class ValueProducerMethodProducerThrowExceptionTestGenerator extends TemplateValueProducerTestMethodsGenerator {

    public ValueProducerMethodProducerThrowExceptionTestGenerator(Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter, MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, FieldsValueProducerGenerationInput fieldsValueProducerGenerationInput, Class<?> currentTest, boolean verifySetterMethodTestNeeded,
                                                                  boolean verifyTwiceSetterMethodTestNeeded) {
        super(argsToCodeBlockConverter, mapperGenerationContext, typesMapperGenerationInput, fieldsValueProducerGenerationInput, currentTest, verifySetterMethodTestNeeded, verifyTwiceSetterMethodTestNeeded);
    }

    @Override
    public CodeBlock getMapStatement(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, FieldsValueProducerGenerationInput fieldsValueProducerGenerationInput, Type currentTest, boolean verifySetterMethodTestNeeded,
                                     boolean verifyTwiceSetterMethodTestNeeded) {

        CodeBlock producerCreationParameters = argsToCodeBlockConverter.apply(fieldsValueProducerGenerationInput.getCreationParametersGenerationInputs());
        Class<?> mockitoClassObject = Mockito.class;
        CodeBlock.Builder resultBuilder = CodeBlock.builder();

        if (currentTest.equals(NullPointerException.class)) {
            resultBuilder.addStatement("$T.when($L.$L($S, $L)).thenReturn($L)", mockitoClassObject,
                    NamingConvention.VALUE_PRODUCER_FACTORY_MEMBER, NamingConvention.VALUE_PRODUCER_FACTORY_CREATE_FUNCTION,
                    fieldsValueProducerGenerationInput.getValueProducerName(), producerCreationParameters,
                    NamingConvention.NULL)
                    .build();
        }

        if (currentTest.equals(ValueProducerCreationException.class)) {
            resultBuilder.addStatement("$T.when($L.$L($S, $L)).thenThrow($T.class)", mockitoClassObject,
                    NamingConvention.VALUE_PRODUCER_FACTORY_MEMBER, NamingConvention.VALUE_PRODUCER_FACTORY_CREATE_FUNCTION,
                    fieldsValueProducerGenerationInput.getValueProducerName(), producerCreationParameters,
                    currentTest)
                    .build();
        }

        return resultBuilder.build();
    }


    @Override
    protected Optional<CodeBlock> createJavadoc() {
        CodeBlock.Builder javadocBuilder = CodeBlock.builder();
        super.createJavadoc().ifPresent(codeBlock -> javadocBuilder.add(codeBlock).add(CodeBlock.of("\n")));
        List<CodeBlock> javadocBlocks = new LinkedList<>();
        if (!verifySetterMethodTestNeeded) {
            javadocBlocks.add(CodeBlock.of("@throws $T is expected to be thrown", currentTest));
        }

        CodeBlock throwsJavadocBlock = CodeBlock.join(javadocBlocks, "\n");
        javadocBuilder.add(throwsJavadocBlock);
        return Optional.of(javadocBuilder.build());
    }

}