package app.transcriber.generation.methods.supplier.map.factories;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.TestTypeGenerator;
import app.transcriber.generation.methods.supplier.ValueSupplierMethodListProducerFactory;
import app.transcriber.generation.methods.supplier.map.PrimitiveCollectionTypesMapTestMethodGenerator;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.CodeBlock;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

@AllArgsConstructor
public class PrimitiveCollectionTypesMapTestMethodGeneratorFactory implements ValueSupplierMethodListProducerFactory<FieldsMapperGenerationInput> {

    @NonNull
    private Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter;

    protected TestTypeGenerator testTypeGenerator;

    @Override
    public List<MethodProducer> create(@NonNull MapperGenerationContext mapperGenerationContext,
                                       @NonNull TypesMapperGenerationInput typesMapperGenerationInput, @NonNull FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        setTypesOfTestsToBeGenerated();
        List<MethodProducer> listOfGeneratedTest = new LinkedList<>();
        if (testTypeGenerator.isVerifySetterMethodTestNeeded())
            listOfGeneratedTest.add(new PrimitiveCollectionTypesMapTestMethodGenerator(argsToCodeBlockConverter,
                    mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, null, true, false));

        return listOfGeneratedTest;
    }

    private void setTypesOfTestsToBeGenerated() {
        testTypeGenerator.setVerifySetterMethodTestNeeded(true);
        testTypeGenerator.setVerifyTwiceSetterMethodTestNeeded(false);
    }
}