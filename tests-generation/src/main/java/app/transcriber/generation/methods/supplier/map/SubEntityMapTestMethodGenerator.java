package app.transcriber.generation.methods.supplier.map;

import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.conventions.TestsNamingConvention;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.generation.model.adapters.FieldAdapter;
import com.squareup.javapoet.CodeBlock;

import java.lang.reflect.Type;
import java.util.List;
import java.util.function.Function;

public class SubEntityMapTestMethodGenerator extends SubEntityMapTestMethodsGeneratorCommon {

    public SubEntityMapTestMethodGenerator(Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter,
                                           MapperGenerationContext mapperGenerationContext,
                                           TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput, Class<?> currentTest, boolean verifySetterMethodTestNeeded,
                                           boolean verifyTwiceSetterMethodTestNeeded) {
        super(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, argsToCodeBlockConverter, currentTest, verifySetterMethodTestNeeded, verifyTwiceSetterMethodTestNeeded);
    }

    @Override
    public CodeBlock subEntityMapTestMethodsGeneratorMapStatement( Class<?> mockitoClassObject,Type currentTest, String mapperVariableName, Type sourceFieldType, Type targetFieldType, Function<FieldAdapter, String> getterMethodNameConvention) {

        CodeBlock.Builder resultBuilder = CodeBlock.builder();
        if(currentTest == null) {
            resultBuilder
                    .addStatement("$T $L = $T.mock($T.class)", targetFieldType, TestsNamingConvention.TARGET_SUB_ENTITY_MOCKED, mockitoClassObject, targetFieldType); }
        resultBuilder
                .addStatement("$T.when($L.$L($T.class, $T.class)).thenReturn($L)", mockitoClassObject, NamingConvention.MAPPER_FACTORY_MEMBER, NamingConvention.MAPPER_FACTORY_CREATE_FUNCTION,
                        sourceFieldType, targetFieldType, mapperVariableName)
                .addStatement("$T.when($L.$L($L.$L())).thenReturn($L)", mockitoClassObject, mapperVariableName, NamingConvention.OPERATOR_APPLY_METHOD
                        , TestsNamingConvention.SOURCE_MOCKED_OBJECT, getterMethodNameConvention.apply(fieldsMapperGenerationInput.getSourceFields().get(0)),
                        TestsNamingConvention.TARGET_SUB_ENTITY_MOCKED)
                .build();
        return resultBuilder.build();
    }
}