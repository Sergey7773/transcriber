package app.transcriber.generation.methods.supplier.map;

import app.transcriber.api.exceptions.MapperCreationException;
import app.transcriber.api.exceptions.MappingException;
import app.transcriber.generation.conventions.GetterMethodNameConvention;
import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.conventions.SetterMethodNameConvention;
import app.transcriber.generation.conventions.TestsNamingConvention;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import app.transcriber.generation.model.adapters.FieldAdapter;
import app.transcriber.tools.ListAccessUtils;
import app.transcriber.tools.ReflectionApiUtils;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.ParameterizedTypeName;
import org.mockito.Mockito;

import java.lang.reflect.Type;
import java.util.*;
import java.util.function.Function;

public class SubEntityCollectionMapThrowExceptionTestMethodGenerator extends TemplateFieldsMapTestMethodsGenerator {

    public SubEntityCollectionMapThrowExceptionTestMethodGenerator(Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter,
                                                                   MapperGenerationContext mapperGenerationContext,
                                                                   TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput, Class<?> currentTest, boolean verifySetterMethodTestNeeded,
                                                                   boolean verifyTwiceSetterMethodTestNeeded) {
        super(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, argsToCodeBlockConverter, currentTest, verifySetterMethodTestNeeded,
                verifyTwiceSetterMethodTestNeeded);

    }

    @Override
    public CodeBlock getMapStatement(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput, Type currentTest) {
        boolean atLeastOneExceptionIsTrue = false;
        boolean isCurrentTestNull = true;
        if (currentTest != null)
            isCurrentTestNull = false;
        FieldAdapter lastFieldSourceInList = ListAccessUtils.getLastElementOrThrow(fieldsMapperGenerationInput.getSourceFields());

        Class<?> mockitoClassObject = Mockito.class;

        Type targetSubEntityType = getSubEntityType(fieldsMapperGenerationInput.getTargetField());
        Type sourceSubEntityType = getSubEntityType(lastFieldSourceInList);

        ClassAdapter sourceFieldType = lastFieldSourceInList.getType();
        ParameterizedTypeName sourceDefinedCollectionType = ParameterizedTypeName.get((Class<?>) sourceFieldType.getWrappedType(), sourceSubEntityType);

        Function<FieldAdapter, String> getterMethodNameConvention = mapperGenerationContext.getNamingConventionsContext().getConvention(GetterMethodNameConvention.class);

        List<String> args = new ArrayList<>(2);
        args.add(TestsNamingConvention.SOURCE_SUB_ENTITY_MOCKED.toString());
        args.add(TestsNamingConvention.SOURCE_SUB_ENTITY_MOCKED.toString());

        CodeBlock.Builder resultBuilder = CodeBlock.builder();
        if (!isCurrentTestNull) {
            atLeastOneExceptionIsTrue = currentTest.equals(NullPointerException.class) || currentTest.equals(MappingException.class);
        }
        if (verifySetterMethodTestNeeded || verifyTwiceSetterMethodTestNeeded || atLeastOneExceptionIsTrue) {
            resultBuilder
                    .addStatement("$T $L = $T.mock($T.class)", sourceSubEntityType, TestsNamingConvention.SOURCE_SUB_ENTITY_MOCKED, mockitoClassObject, sourceSubEntityType)
                    .addStatement("$T $L = $L", sourceDefinedCollectionType, TestsNamingConvention.SOURCE_SUB_ENTITY_COLLECTION, arrayAsListCollectionWithParameters(args))
                    .addStatement("$T.when($L.$L()).thenReturn($L)", mockitoClassObject, TestsNamingConvention.SOURCE_MOCKED_OBJECT, getterMethodNameConvention.apply(fieldsMapperGenerationInput.getSourceFields().get(0)), TestsNamingConvention.SOURCE_SUB_ENTITY_COLLECTION)
                    .addStatement("$T.when($L.$L($T.class, $T.class)).thenReturn($L)", mockitoClassObject, NamingConvention.MAPPER_FACTORY_MEMBER,
                            NamingConvention.MAPPER_FACTORY_CREATE_FUNCTION, sourceSubEntityType, targetSubEntityType, NamingConvention.SOURCE_SUB_ENTITY_TO_TARGET_SUB_ENTITY_MAPPER);
            if (atLeastOneExceptionIsTrue) {
                resultBuilder.addStatement("$T.when($L.$L($L)).thenThrow($T.class)", mockitoClassObject, NamingConvention.SOURCE_SUB_ENTITY_TO_TARGET_SUB_ENTITY_MAPPER, NamingConvention.OPERATOR_APPLY_METHOD, TestsNamingConvention.SOURCE_SUB_ENTITY_MOCKED, currentTest);
            }
        }

        if (!isCurrentTestNull && currentTest.equals(MapperCreationException.class)) {
            resultBuilder.addStatement("$T.when($L.$L($T.class, $T.class)).thenThrow($T.class)", mockitoClassObject, NamingConvention.MAPPER_FACTORY_MEMBER,
                    NamingConvention.MAPPER_FACTORY_CREATE_FUNCTION, sourceSubEntityType, targetSubEntityType, currentTest);
        }

        return resultBuilder.build();
    }

    private Type getSubEntityType(FieldAdapter lastFieldSourceInList) {
        return ReflectionApiUtils.getGenericTypeArgument(lastFieldSourceInList);
    }


    private CodeBlock arrayAsListCollectionWithParameters(List<String> args) {
        return CodeBlock.builder()
                .add("$T.asList($L)", Arrays.class, String.join(", ", args))
                .build();
    }

    @Override
    public CodeBlock getMapVerifyStatement(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput, Class<?> mockitoClassObject, Type currentTest) {
        String setterMethodName = mapperGenerationContext.getNamingConventionsContext().getConvention(SetterMethodNameConvention.class).apply(fieldsMapperGenerationInput.getTargetField());

        CodeBlock.Builder resultBuilder = CodeBlock.builder();
        if (currentTest == null || verifySetterMethodTestNeeded ) {
            resultBuilder.addStatement("$T.verify($L,$T.times(1)).$L($T.any())", mockitoClassObject,
                    TestsNamingConvention.TARGET_MOCKED_OBJECT, mockitoClassObject, setterMethodName, mockitoClassObject)
                    .build();
        }
        if (verifyTwiceSetterMethodTestNeeded) {
            resultBuilder.addStatement("$T.verify($L,$T.times(2)).$L($T.eq($L))", mockitoClassObject,
                    NamingConvention.SOURCE_SUB_ENTITY_TO_TARGET_SUB_ENTITY_MAPPER, mockitoClassObject, NamingConvention.OPERATOR_APPLY_METHOD, mockitoClassObject, TestsNamingConvention.SOURCE_SUB_ENTITY_MOCKED)
                    .build();
        }

        return resultBuilder.build();
    }

    @Override
    protected List<Type> thrownExceptions() {
        List<Type> result = new LinkedList<>(super.thrownExceptions());

        result.add(MapperCreationException.class);
        result.add(MappingException.class);

        return result;
    }

}