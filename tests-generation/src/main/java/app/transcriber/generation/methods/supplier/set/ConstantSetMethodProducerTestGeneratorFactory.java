package app.transcriber.generation.methods.supplier.set;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.supplier.ConstantSetMethodArgumentFormatter;
import app.transcriber.generation.methods.supplier.ValueSupplierMethodProducerFactory;
import app.transcriber.generation.model.ConstantMappingGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.Optional;

@AllArgsConstructor
public class ConstantSetMethodProducerTestGeneratorFactory implements ValueSupplierMethodProducerFactory<ConstantMappingGenerationInput> {

    @NonNull
    private ConstantSetMethodArgumentFormatter constantSetMethodArgumentFormatter;

    @Override
    public Optional<MethodProducer> create(MapperGenerationContext mapperGenerationContext,
                                           TypesMapperGenerationInput typesMapperGenerationInput, ConstantMappingGenerationInput constantMappingGenerationInput) {
        return Optional.of(new ConstantSetMethodProducerTestGenerator(mapperGenerationContext, typesMapperGenerationInput, constantMappingGenerationInput, constantSetMethodArgumentFormatter));
    }

}
