package app.transcriber.generation.methods.supplier.map;

import app.transcriber.generation.conventions.GetterMethodNameConvention;
import app.transcriber.generation.conventions.SetterMethodNameConvention;
import app.transcriber.generation.conventions.TestsNamingConvention;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.CodeBlock;

import java.lang.reflect.Type;
import java.util.List;
import java.util.function.Function;

public class PrimitiveCollectionTypesMapTestMethodGenerator extends TemplateFieldsMapTestMethodsGenerator {

    public PrimitiveCollectionTypesMapTestMethodGenerator(Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter,
                                                          MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput,
                                                          FieldsMapperGenerationInput fieldsMapperGenerationInput, Class<?> currentTest, boolean verifySetterMethodTestNeeded,
                                                          boolean verifyTwiceSetterMethodTestNeeded) {
        super(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, argsToCodeBlockConverter, currentTest, verifySetterMethodTestNeeded, verifyTwiceSetterMethodTestNeeded);

    }

    @Override
    public CodeBlock getMapStatement(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput, Type currentTest) {
        return CodeBlock.builder().add("").build();
    }

    @Override
    public CodeBlock getMapVerifyStatement(MapperGenerationContext mapperGenerationContext,
                                           TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput, Class<?> mockitoClassObject, Type currentTest) {

        String setterMethodName = mapperGenerationContext.getNamingConventionsContext().getConvention(SetterMethodNameConvention.class).apply(fieldsMapperGenerationInput.getTargetField());
        String getterMethodName = (mapperGenerationContext.getNamingConventionsContext().getConvention(GetterMethodNameConvention.class).apply(fieldsMapperGenerationInput.getSourceFields().get(0))) + "()";

        return CodeBlock.builder()
                .addStatement("$T.verify($L,$T.times(1)).$L($L.$L)", mockitoClassObject,
                        TestsNamingConvention.TARGET_MOCKED_OBJECT, mockitoClassObject, setterMethodName, TestsNamingConvention.SOURCE_MOCKED_OBJECT, getterMethodName)
                .build();
    }

}