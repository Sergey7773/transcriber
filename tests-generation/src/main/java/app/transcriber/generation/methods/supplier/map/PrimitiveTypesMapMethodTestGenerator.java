package app.transcriber.generation.methods.supplier.map;

import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.conventions.SetterMethodNameConvention;
import app.transcriber.generation.conventions.TestsNamingConvention;
import app.transcriber.generation.methods.GettersChainGenerator;
import app.transcriber.generation.model.*;
import app.transcriber.generation.model.adapters.ClassAdapter;
import app.transcriber.generation.model.adapters.FieldAdapter;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.ParameterizedTypeName;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mockito;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;


public class PrimitiveTypesMapMethodTestGenerator extends TemplateFieldsMapTestMethodsGenerator {


    public PrimitiveTypesMapMethodTestGenerator(Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter,
                                                MapperGenerationContext mapperGenerationContext,
                                                TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput, Class<?> currentTest, boolean verifySetterMethodTestNeeded,
                                                boolean verifyTwiceSetterMethodTestNeeded) {
        super(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, argsToCodeBlockConverter, currentTest, verifySetterMethodTestNeeded,
                verifyTwiceSetterMethodTestNeeded);
    }

    @Override
    public CodeBlock getMapStatement(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput, Type currentTest) {
        List<String> gettersChain = getGettersChain(mapperGenerationContext, fieldsMapperGenerationInput);
        Class<?> mockitoClassObject = Mockito.class;

        CodeBlock.Builder resultBuilder = CodeBlock.builder();
        if (gettersChain.size() > 1) {
            ClassAdapter sourceFieldTypeAdapter = fieldsMapperGenerationInput.getSourceFields().get(0).getType();
            Type sourceFieldType = sourceFieldTypeAdapter.getWrappedType();
            String sourceFieldTypeName = sourceFieldTypeAdapter.getName();
            if (sourceFieldType != null) {
                sourceFieldTypeName = sourceFieldTypeName.substring(sourceFieldTypeName.lastIndexOf(".") + 1);
            }

            String getSourceFieldTypeName = NamingConvention.VALUE_PRODUCER_GET_FUNCTION + StringUtils.capitalize(sourceFieldTypeName);
            resultBuilder
                    .addStatement("$T $L = $T.mock($T.class)", sourceFieldType, TestsNamingConvention.SOURCE_SUB_ENTITY_MOCKED, mockitoClassObject, sourceFieldType)
                    .addStatement("$T.when($L.$L()).thenReturn($L)", mockitoClassObject, TestsNamingConvention.SOURCE_MOCKED_OBJECT, getSourceFieldTypeName, TestsNamingConvention.SOURCE_SUB_ENTITY_MOCKED);
        }

        if (!fieldsMapperGenerationInput.getMappingOperatorsGenerationInputs().isEmpty()) {

            ParameterizedTypeName functionType = ParameterizedTypeName.get(Function.class, Object.class, Object.class);
            List<MappingOperatorsGenerationInput> mappingOperatorMetadata = fieldsMapperGenerationInput.getMappingOperatorsGenerationInputs();
            String targetTypeSimpleName = mappingOperatorMetadata.get(0).getMapperTargetType().getSimpleName();
            String stringDefinitionStatement = "$L $L = $L.class.getDeclaredConstructor().newInstance()";
            String functionDefinitionStatement = "$T $L = ($L) -> $L";

            resultBuilder
                    .addStatement(stringDefinitionStatement, targetTypeSimpleName, TestsNamingConvention.SOURCE_FIELD_VALUE, targetTypeSimpleName)
                    .addStatement(stringDefinitionStatement, targetTypeSimpleName, TestsNamingConvention.EXPECTED_RESULT_OF_OPERATORS, targetTypeSimpleName)
                    .addStatement(functionDefinitionStatement, functionType, TestsNamingConvention.MAPPING_OPERATOR_STUB, NamingConvention.OBJECT, TestsNamingConvention.EXPECTED_RESULT_OF_OPERATORS)
                    .addStatement("$T.when($L.$L()).thenReturn($L)", mockitoClassObject, TestsNamingConvention.SOURCE_MOCKED_OBJECT, gettersChain.get(0), TestsNamingConvention.SOURCE_FIELD_VALUE);
        } else
            resultBuilder.add("");
        return resultBuilder.build();
    }


    private List<String> getSettersChain(MapperGenerationContext mapperGenerationContext, FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        return getListOfSetters(
                fieldsMapperGenerationInput.getTargetField(),
                mapperGenerationContext);
    }

    public List<String> getListOfSetters(FieldAdapter fieldSource, MapperGenerationContext mapperGenerationContext) {
        SetterMethodNameConvention setterMethodNameConvention = mapperGenerationContext.getNamingConventionsContext().getConvention(SetterMethodNameConvention.class);
        List<String> setterBlocks = new LinkedList<>();
        setterBlocks.add(setterMethodNameConvention.apply(fieldSource));

        return setterBlocks;
    }

    private List<String> getGettersChain(MapperGenerationContext mapperGenerationContext, FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        return new GettersChainGenerator().getListOfGetters(
                fieldsMapperGenerationInput.getSourceFields(),
                mapperGenerationContext);
    }

    @Override
    public CodeBlock getMapVerifyStatement(MapperGenerationContext mapperGenerationContext,
                                           TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput, Class<?> mockitoClassObject, Type currentTest) {

        List<String> settersChain = getSettersChain(mapperGenerationContext, fieldsMapperGenerationInput);
        List<String> gettersChain = getGettersChain(mapperGenerationContext, fieldsMapperGenerationInput);

        CodeBlock.Builder resultBuilder = CodeBlock.builder();

        if (gettersChain.size() > 1) {
            resultBuilder
                    .addStatement("$T.verify($L,$T.times(1)).$L($T.eq($L.$L()))", mockitoClassObject,
                            TestsNamingConvention.TARGET_MOCKED_OBJECT, mockitoClassObject, settersChain.get(0), mockitoClassObject, TestsNamingConvention.SOURCE_SUB_ENTITY_MOCKED, gettersChain.get(1))
                    .build();
        } else if (!gettersChain.isEmpty() && !fieldsMapperGenerationInput.getMappingOperatorsGenerationInputs().isEmpty()) {
            resultBuilder
                    .addStatement("$T.verify($L,$T.times(1)).$L($L)", mockitoClassObject,
                            TestsNamingConvention.TARGET_MOCKED_OBJECT, mockitoClassObject, settersChain.get(0), TestsNamingConvention.EXPECTED_RESULT_OF_OPERATORS)
                    .build();
        } else resultBuilder
                .addStatement("$T.verify($L,$T.times(1)).$L($L.$L())", mockitoClassObject,
                        TestsNamingConvention.TARGET_MOCKED_OBJECT, mockitoClassObject, settersChain.get(0), TestsNamingConvention.SOURCE_MOCKED_OBJECT, gettersChain.get(0))
                .build();
        return resultBuilder.build();
    }
}