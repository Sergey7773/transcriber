package app.transcriber.generation.methods.supplier.produce;

import app.transcriber.api.exceptions.ValueProducerCreationException;
import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.TestTypeGenerator;
import app.transcriber.generation.methods.supplier.ValueSupplierMethodListProducerFactory;
import app.transcriber.generation.model.FieldsValueProducerGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.CodeBlock;
import lombok.AllArgsConstructor;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

@AllArgsConstructor
public class ValueProducerMethodProducerTestGeneratorFactory implements ValueSupplierMethodListProducerFactory<FieldsValueProducerGenerationInput> {

    private Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter;

    protected TestTypeGenerator testTypeGenerator;

    @Override
    public List<MethodProducer> create(MapperGenerationContext mapperGenerationContext,
                                       TypesMapperGenerationInput typesMapperGenerationInput,
                                       FieldsValueProducerGenerationInput fieldsValueProducerGenerationInput) {
        setTypesOfTestsToBeGenerated();
        List<Class<?>> typesOfTestsToBeGenerated = testTypeGenerator.getThrownExceptions();
        List<MethodProducer> listOfGeneratedTest = new LinkedList<>();
        for (Class<?> type : typesOfTestsToBeGenerated) {
            listOfGeneratedTest.add(new ValueProducerMethodProducerThrowExceptionTestGenerator(argsToCodeBlockConverter,
                    mapperGenerationContext, typesMapperGenerationInput, fieldsValueProducerGenerationInput, type, false, false));
        }
        if (testTypeGenerator.isVerifySetterMethodTestNeeded()) {
            listOfGeneratedTest.add(new ValueProducerMethodProducerTestGenerator(argsToCodeBlockConverter,
                    mapperGenerationContext, typesMapperGenerationInput, fieldsValueProducerGenerationInput, null, testTypeGenerator.isVerifySetterMethodTestNeeded(), testTypeGenerator.isVerifyTwiceSetterMethodTestNeeded()));
        }
        return listOfGeneratedTest;
    }


    private void setTypesOfTestsToBeGenerated() {
        List<Class<?>> result = new LinkedList<>();
        result.add(ValueProducerCreationException.class);
        result.add(NullPointerException.class);

        testTypeGenerator.setThrownExceptions(result);
        testTypeGenerator.setVerifySetterMethodTestNeeded(true);
        testTypeGenerator.setVerifyTwiceSetterMethodTestNeeded(false);
    }

}