package app.transcriber.generation.methods.supplier;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.supplier.map.factories.ListFieldsMapMethodProducerFactory;
import app.transcriber.generation.methods.supplier.produce.ValueProducerMethodProducerTestGeneratorFactory;
import app.transcriber.generation.methods.supplier.set.ConstantSetMethodProducerTestGeneratorFactory;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.MethodSpec;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
public class SupplierTestMethodGenerator {

    @NonNull
    private final ValueProducerMethodProducerTestGeneratorFactory valueProducerMethodProducerTestGeneratorFactory;
    @NonNull
    private final ConstantSetMethodProducerTestGeneratorFactory constantSetMethodProducerTestGeneratorFactory;

    private final ListFieldsMapMethodProducerFactory listFieldsMapMethodProducerFactory;

    public List<MethodSpec> generate(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        List<MethodSpec> result = new LinkedList<>();
        result.addAll(generateListOfMapMethods(mapperGenerationContext, typesMapperGenerationInput));
        result.addAll(generateProduceMethods(mapperGenerationContext, typesMapperGenerationInput));
        result.addAll(generateSetMethods(mapperGenerationContext, typesMapperGenerationInput));
        return result;

    }

    private List<MethodSpec> generateListOfMapMethods(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        return typesMapperGenerationInput.getFieldsMappersGenerationInputs()
                .stream()
                .map(fieldsMapperMetadata ->
                        listFieldsMapMethodProducerFactory.create(mapperGenerationContext,
                                typesMapperGenerationInput, fieldsMapperMetadata)
                ).flatMap(x -> x.stream().map(MethodProducer::get))
                .collect(Collectors.toList());
    }

    private List<MethodSpec> generateProduceMethods(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        return typesMapperGenerationInput.getFieldsValueProducersGenerationInputs()
                .stream()
                .map(fieldsValueProducerMetadata ->
                        valueProducerMethodProducerTestGeneratorFactory.create(mapperGenerationContext,
                                typesMapperGenerationInput, fieldsValueProducerMetadata)
                )
                .flatMap(x -> x.stream().map(MethodProducer::get))
                .collect(Collectors.toList());
    }

    private List<MethodSpec> generateSetMethods(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        return typesMapperGenerationInput.getConstantMappingsGenerationInputs()
                .stream().
                        map(constantMappingMetadata ->
                                constantSetMethodProducerTestGeneratorFactory.create(mapperGenerationContext,
                                        typesMapperGenerationInput, constantMappingMetadata)
                        ).filter(Optional::isPresent)
                .map(Optional::get)
                .map(MethodProducer::get)
                .collect(Collectors.toList());
    }
}