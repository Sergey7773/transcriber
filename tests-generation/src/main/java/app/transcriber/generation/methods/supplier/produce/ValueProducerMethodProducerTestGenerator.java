package app.transcriber.generation.methods.supplier.produce;

import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.conventions.TestsNamingConvention;
import app.transcriber.generation.model.FieldsValueProducerGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.ParameterizedTypeName;
import org.mockito.Mockito;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

public class ValueProducerMethodProducerTestGenerator extends TemplateValueProducerTestMethodsGenerator {

    public ValueProducerMethodProducerTestGenerator(Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter, MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, FieldsValueProducerGenerationInput fieldsValueProducerGenerationInput, Class<?> currentTest, boolean verifySetterMethodTestNeeded,
                                                    boolean verifyTwiceSetterMethodTestNeeded) {
        super(argsToCodeBlockConverter, mapperGenerationContext, typesMapperGenerationInput, fieldsValueProducerGenerationInput, currentTest, verifySetterMethodTestNeeded, verifyTwiceSetterMethodTestNeeded);
    }

    @Override
    public CodeBlock getMapStatement(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, FieldsValueProducerGenerationInput fieldsValueProducerGenerationInput, Type currentTest, boolean verifySetterMethodTestNeeded,
                                     boolean verifyTwiceSetterMethodTestNeeded) {

        CodeBlock producerCreationParameters = argsToCodeBlockConverter.apply(fieldsValueProducerGenerationInput.getCreationParametersGenerationInputs());
        Class<?> mockitoClassObject = Mockito.class;
        ParameterizedTypeName supplierType = ParameterizedTypeName.get(Supplier.class, Object.class);
        String targetTypeSimpleName = fieldsValueProducerGenerationInput.getTargetField().getType().getSimpleName();
        String stringDefinitionStatement = "$L $L = $L.class.getDeclaredConstructor().newInstance()";
        CodeBlock.Builder resultBuilder = CodeBlock.builder();

        if (verifySetterMethodTestNeeded) {
            resultBuilder
                    .addStatement(stringDefinitionStatement, targetTypeSimpleName, TestsNamingConvention.SOURCE_FIELD_VALUE, targetTypeSimpleName)
                    .addStatement("$T $L= () -> $L", supplierType, NamingConvention.SUPPLIER, TestsNamingConvention.SOURCE_FIELD_VALUE)
                    .addStatement("$T.when($L.$L($S, $L)).thenReturn($L)", mockitoClassObject,
                            NamingConvention.VALUE_PRODUCER_FACTORY_MEMBER, NamingConvention.VALUE_PRODUCER_FACTORY_CREATE_FUNCTION,
                            fieldsValueProducerGenerationInput.getValueProducerName(), producerCreationParameters,
                            NamingConvention.SUPPLIER)
                    .build();
        }
        return resultBuilder.build();
    }

    @Override
    protected List<Type> thrownExceptions() {
        List<Type> result = new LinkedList<>(super.thrownExceptions());

        result.add(NoSuchMethodException.class);
        result.add(IllegalAccessException.class);
        result.add(InvocationTargetException.class);
        result.add(InstantiationException.class);

        return result;
    }
}