package app.transcriber.generation.methods.supplier.map;

import app.transcriber.api.exceptions.MapperCreationException;
import app.transcriber.api.exceptions.MappingException;
import app.transcriber.generation.conventions.GetterMethodNameConvention;
import app.transcriber.generation.conventions.SetterMethodNameConvention;
import app.transcriber.generation.conventions.SubEntityMapperVariableNameConvention;
import app.transcriber.generation.conventions.TestsNamingConvention;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.generation.model.adapters.FieldAdapter;
import app.transcriber.tools.ListAccessUtils;
import com.squareup.javapoet.CodeBlock;
import org.mockito.Mockito;

import java.lang.reflect.Type;
import java.util.*;
import java.util.function.Function;

import static com.squareup.javapoet.CodeBlock.builder;

public abstract class SubEntityMapTestMethodsGeneratorCommon extends TemplateFieldsMapTestMethodsGenerator {

    protected SubEntityMapTestMethodsGeneratorCommon(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput, Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter, Class<?> currentTest, boolean verifySetterMethodTestNeeded, boolean verifyTwiceSetterMethodTestNeeded) {
        super(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, argsToCodeBlockConverter, currentTest, verifySetterMethodTestNeeded, verifyTwiceSetterMethodTestNeeded);
    }

    @Override
    public CodeBlock getMapVerifyStatement(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput, Class<?> mockitoClassObject, Type currentTest) {
        String setterMethodName = mapperGenerationContext.getNamingConventionsContext().getConvention(SetterMethodNameConvention.class).apply(fieldsMapperGenerationInput.getTargetField());
        CodeBlock.Builder resultBuilder = CodeBlock.builder();

        if (currentTest == null || verifySetterMethodTestNeeded) {
            resultBuilder.addStatement("$T.verify($L,$T.times(1)).$L($T.eq($L))", mockitoClassObject,
                    TestsNamingConvention.TARGET_MOCKED_OBJECT, mockitoClassObject, setterMethodName, mockitoClassObject, TestsNamingConvention.TARGET_SUB_ENTITY_MOCKED)
                    .build();
        }

        return resultBuilder.build();
    }

    @Override
    protected List<Type> thrownExceptions() {
        List<Type> result = new LinkedList<>(super.thrownExceptions());

        result.add(MapperCreationException.class);
        result.add(MappingException.class);

        return result;
    }

    @Override
    protected Optional<CodeBlock> createJavadoc() {
        CodeBlock.Builder javadocBuilder = builder();
        super.createJavadoc().ifPresent(codeBlock -> javadocBuilder.add(codeBlock).add(CodeBlock.of("\n")));

        List<CodeBlock> javadocBlocks = new LinkedList<>();
        javadocBlocks.add(CodeBlock.of("@throws $T if an exception occurred during the creation of a required mapper", MapperCreationException.class));
        javadocBlocks.add(CodeBlock.of("@throws $T if an exception occurred during the mapping of the sub entity", MappingException.class));
        CodeBlock throwsJavadocBlock = CodeBlock.join(javadocBlocks, "\n");

        javadocBuilder.add(throwsJavadocBlock);

        return Optional.of(javadocBuilder.build());
    }

    @Override
    public CodeBlock getMapStatement(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput, Type currentTest) {
        String mapperVariableName = mapperGenerationContext.getNamingConventionsContext().getConvention(SubEntityMapperVariableNameConvention.class).apply(fieldsMapperGenerationInput);

        FieldAdapter lastFieldSourceInList = ListAccessUtils.getLastElementOrThrow(fieldsMapperGenerationInput.getSourceFields());
        Type sourceFieldType = lastFieldSourceInList.getType().getWrappedType();
        Type targetFieldType = fieldsMapperGenerationInput.getTargetField().getType().getWrappedType();
        Function<FieldAdapter, String> getterMethodNameConvention = mapperGenerationContext.getNamingConventionsContext().getConvention(GetterMethodNameConvention.class);
        Class<?> mockitoClassObject = Mockito.class;

        return subEntityMapTestMethodsGeneratorMapStatement(mockitoClassObject, currentTest, mapperVariableName, sourceFieldType, targetFieldType,getterMethodNameConvention);
    }

    public abstract CodeBlock subEntityMapTestMethodsGeneratorMapStatement(Class<?> mockitoClassObject, Type currentTest, String mapperVariableName, Type sourceFieldType, Type targetFieldType, Function<FieldAdapter, String> getterMethodNameConvention);
}
