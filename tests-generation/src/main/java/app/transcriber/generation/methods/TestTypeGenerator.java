package app.transcriber.generation.methods;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TestTypeGenerator {

    boolean verifySetterMethodTestNeeded;
    boolean verifyTwiceSetterMethodTestNeeded;
    List<Class<?>> thrownExceptions;
}