package app.transcriber.generation.methods.apply;

import app.transcriber.generation.conventions.MapMethodNameConvention;
import app.transcriber.generation.conventions.ProduceMethodNameConvention;
import app.transcriber.generation.conventions.SetConstantValueMethodNameConvention;
import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.tools.GenerationType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ApplyTestMethodGeneratorFactory {

    public List<MethodProducer> create(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        Map<String, GenerationType> methodNameToTestTypeMap = getMethodToTestTypeMap(mapperGenerationContext, typesMapperGenerationInput);

        return methodNameToTestTypeMap.entrySet()
                .stream()
                .map(entry -> new ApplyTestMethodGenerator(mapperGenerationContext, typesMapperGenerationInput, entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    private Map<String, GenerationType> getMethodToTestTypeMap(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        Map<String, GenerationType> methodsNameConvention = new HashMap<>();
        methodsNameConvention.putAll(getMapForFieldsMappers(mapperGenerationContext, typesMapperGenerationInput));
        methodsNameConvention.putAll(getMapForFieldsValueProducers(mapperGenerationContext, typesMapperGenerationInput));
        methodsNameConvention.putAll(getMapForConstantMappings(mapperGenerationContext, typesMapperGenerationInput));
        return methodsNameConvention;
    }

    private Map<String, GenerationType> getMapForFieldsMappers(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        MapMethodNameConvention mapMethodNameConvention = mapperGenerationContext.getNamingConventionsContext().getConvention(MapMethodNameConvention.class);

        return typesMapperGenerationInput.getFieldsMappersGenerationInputs()
                .stream().map(mapMethodNameConvention).collect(Collectors.toMap(Function.identity(), methodName -> GenerationType.FIELDS_MAPPER_GENERATION));
    }

    private Map<String, GenerationType> getMapForFieldsValueProducers(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        ProduceMethodNameConvention produceMethodNameConvention = mapperGenerationContext.getNamingConventionsContext().getConvention(ProduceMethodNameConvention.class);

        return typesMapperGenerationInput.getFieldsValueProducersGenerationInputs()
                .stream().map(produceMethodNameConvention).collect(Collectors.toMap(Function.identity(), methodName -> GenerationType.FIELDS_VALUE_PRODUCER_GENERATION));
    }

    private Map<String, GenerationType> getMapForConstantMappings(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        SetConstantValueMethodNameConvention setConstantValueMethodNameConvention = mapperGenerationContext.getNamingConventionsContext().getConvention(SetConstantValueMethodNameConvention.class);

        return typesMapperGenerationInput.getConstantMappingsGenerationInputs()
                .stream().map(setConstantValueMethodNameConvention).collect(Collectors.toMap(Function.identity(), methodName -> GenerationType.CONSTANT_MAPPING_GENERATION));
    }
}