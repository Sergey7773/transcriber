package app.transcriber.generation.methods.supplier.map;

import app.transcriber.api.exceptions.MapperCreationException;
import app.transcriber.api.exceptions.MappingException;
import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.conventions.TestsNamingConvention;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.generation.model.adapters.FieldAdapter;
import com.squareup.javapoet.CodeBlock;

import java.lang.reflect.Type;
import java.util.List;
import java.util.function.Function;

public class SubEntityMapThrowExceptionTestMethodGenerator extends SubEntityMapTestMethodsGeneratorCommon {

    public SubEntityMapThrowExceptionTestMethodGenerator(Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter,
                                                         MapperGenerationContext mapperGenerationContext,
                                                         TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput, Class<?> currentTest, boolean verifySetterMethodTestNeeded,
                                                         boolean verifyTwiceSetterMethodTestNeeded) {
        super(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, argsToCodeBlockConverter, currentTest, verifySetterMethodTestNeeded, verifyTwiceSetterMethodTestNeeded);
    }
    @Override
    public CodeBlock subEntityMapTestMethodsGeneratorMapStatement(Class<?> mockitoClassObject, Type currentTest, String mapperVariableName, Type sourceFieldType, Type targetFieldType, Function<FieldAdapter, String> getterMethodNameConvention) {

        CodeBlock.Builder resultBuilder = CodeBlock.builder();
        if (currentTest.equals(NullPointerException.class) || currentTest.equals(MappingException.class)) {
            resultBuilder
                    .addStatement("$T.when($L.$L($T.class, $T.class)).thenReturn($L)", mockitoClassObject, NamingConvention.MAPPER_FACTORY_MEMBER, NamingConvention.MAPPER_FACTORY_CREATE_FUNCTION,
                            sourceFieldType, targetFieldType, mapperVariableName);
            if (currentTest.equals(NullPointerException.class)) {
                resultBuilder.addStatement("$T.when($L.$L($T.any())).thenThrow($T.class)", mockitoClassObject, mapperVariableName, NamingConvention.OPERATOR_APPLY_METHOD
                        , mockitoClassObject, currentTest)
                        .build();
            }

            if (currentTest.equals(MappingException.class)) {
                resultBuilder.addStatement("$T.when($L.$L($L.$L())).thenThrow($T.class)", mockitoClassObject, mapperVariableName, NamingConvention.OPERATOR_APPLY_METHOD
                        , TestsNamingConvention.SOURCE_MOCKED_OBJECT, getterMethodNameConvention.apply(fieldsMapperGenerationInput.getSourceFields().get(0)),
                        currentTest)
                        .build();
            }
        } else if (currentTest.equals(MapperCreationException.class)) {
            resultBuilder.addStatement("$T.when($L.$L($T.class, $T.class)).thenThrow($T.class)", mockitoClassObject, NamingConvention.MAPPER_FACTORY_MEMBER, NamingConvention.MAPPER_FACTORY_CREATE_FUNCTION,
                    sourceFieldType, targetFieldType, currentTest).build();
        }
        return resultBuilder.build();
    }

}