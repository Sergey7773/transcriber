package app.transcriber.generation.methods.supplier.map;

import app.transcriber.api.exceptions.MappingOperatorCreationException;
import app.transcriber.generation.conventions.*;
import app.transcriber.generation.methods.AbstractMethodsProducer;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.tools.ListAccessUtils;
import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.mockito.Mockito;

import javax.lang.model.element.Modifier;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@AllArgsConstructor
public abstract class TemplateFieldsMapTestMethodsGenerator extends AbstractMethodsProducer {

    protected MapperGenerationContext mapperGenerationContext;
    protected TypesMapperGenerationInput typesMapperGenerationInput;
    protected FieldsMapperGenerationInput fieldsMapperGenerationInput;
    private final Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter;
    private final Class<?> currentTest;
    protected boolean verifySetterMethodTestNeeded;
    protected boolean verifyTwiceSetterMethodTestNeeded;

    @Override
    protected MethodSpec.Builder createMethodDefinition() {
        String methodName = mapperGenerationContext.getNamingConventionsContext().getConvention(MapTestMethodNameConvention.class).apply(fieldsMapperGenerationInput, currentTest, verifySetterMethodTestNeeded, verifyTwiceSetterMethodTestNeeded);
        AnnotationSpec templateFieldsMapTestMethodsAnnotation = getAnnotationSpec();
        return MethodSpec.methodBuilder(methodName)
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(templateFieldsMapTestMethodsAnnotation)
                .returns(void.class);
    }

    private AnnotationSpec getAnnotationSpec() {
        AnnotationSpec templateFieldsMapTestMethodsAnnotation;
        if (!verifyTwiceSetterMethodTestNeeded && !verifySetterMethodTestNeeded && currentTest != null) {
            templateFieldsMapTestMethodsAnnotation =
                    AnnotationSpec.builder(Test.class)
                            .addMember("expected", "$T.class", currentTest)
                            .build();
        } else {
            templateFieldsMapTestMethodsAnnotation =
                    AnnotationSpec.builder(Test.class).build();
        }
        return templateFieldsMapTestMethodsAnnotation;
    }

    @Override
    protected List<Type> thrownExceptions() {
        List<Type> result = new LinkedList<>(super.thrownExceptions());

        if (!fieldsMapperGenerationInput.getMappingOperatorsGenerationInputs().isEmpty()) {
            result.add(MappingOperatorCreationException.class);
            result.add(NoSuchMethodException.class);
            result.add(IllegalAccessException.class);
            result.add(InvocationTargetException.class);
            result.add(InstantiationException.class);
        }
        return result;
    }

    @Override
    protected CodeBlock createMethodBody() {
        Type targetClass = typesMapperGenerationInput.getTargetClass().getWrappedType();
        Type sourceClass = typesMapperGenerationInput.getSourceClass().getWrappedType();
        Class<?> mockitoClassObject = Mockito.class;
        String methodName = mapperGenerationContext.getNamingConventionsContext().getConvention(MapMethodNameConvention.class).apply(fieldsMapperGenerationInput);
        String mapperClassName = mapperGenerationContext.getNamingConventionsContext().getConvention(MapperClassNameConvention.class).apply(typesMapperGenerationInput);
        CodeBlock.Builder resultBuilder = CodeBlock.builder().addStatement("$T $L = $T.mock($T.class)", targetClass, TestsNamingConvention.TARGET_MOCKED_OBJECT, mockitoClassObject, targetClass);
        resultBuilder.addStatement("$T $L = $T.mock($T.class)", sourceClass, TestsNamingConvention.SOURCE_MOCKED_OBJECT, mockitoClassObject, sourceClass);
        resultBuilder.add(getMapStatement(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, currentTest));

        fieldsMapperGenerationInput.getMappingOperatorsGenerationInputs().stream()
                .map(mappingOperatorsMetadata ->
                        CodeBlock.builder()
                                .addStatement("$T.when($L.$L($T.eq($S), $T.eq($L))).thenReturn($L)", mockitoClassObject, NamingConvention.MAPPING_OPERATOR_FACTORY_MEMBER,
                                        NamingConvention.MAPPING_OPERATOR_FACTORY_CREATE_FUNCTION, mockitoClassObject, mappingOperatorsMetadata.getOperatorName(), mockitoClassObject,
                                        argsToCodeBlockConverter.apply(mappingOperatorsMetadata.getCreationParametersGenerationInputs()),
                                        TestsNamingConvention.MAPPING_OPERATOR_STUB).build())
                .forEach(resultBuilder::add);


        resultBuilder.addStatement("$L.$L($L,$L)", StringUtils.uncapitalize(mapperClassName), methodName, TestsNamingConvention.SOURCE_MOCKED_OBJECT, TestsNamingConvention.TARGET_MOCKED_OBJECT);

        resultBuilder.add(getMapVerifyStatement(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, mockitoClassObject, currentTest));
        return resultBuilder.build();
    }

    @Override
    protected Optional<CodeBlock> createJavadoc() {
        CodeBlock.Builder javadocBuilder = CodeBlock.builder();
        super.createJavadoc().ifPresent(codeBlock -> javadocBuilder.add(codeBlock).add(CodeBlock.of("\n")));

        List<CodeBlock> javadocBlocks = new LinkedList<>();
        javadocBlocks.add(CodeBlock.of("Verifies the mapping between the $L field of <i>source</i> and $L field of <i>target</i> has been applied",
                ListAccessUtils.getLastElementOrThrow(fieldsMapperGenerationInput.getSourceFields()).getName(),
                fieldsMapperGenerationInput.getTargetField().getName()));
        if (currentTest != null && !verifySetterMethodTestNeeded && !verifyTwiceSetterMethodTestNeeded) {
            javadocBlocks.add(CodeBlock.of("@throws $T is expected to be thrown", currentTest));
        }
        javadocBuilder.add(CodeBlock.join(javadocBlocks, "\n"));
        return Optional.of(javadocBuilder.build());
    }

    public abstract CodeBlock getMapStatement(MapperGenerationContext mapperGenerationContext,
                                              TypesMapperGenerationInput typesMapperGenerationInput,
                                              FieldsMapperGenerationInput fieldsMapperGenerationInput, Type currentTest);

    public abstract CodeBlock getMapVerifyStatement(MapperGenerationContext mapperGenerationContext,
                                                    TypesMapperGenerationInput typesMapperGenerationInput,
                                                    FieldsMapperGenerationInput fieldsMapperGenerationInput, Class<?> mockitoClassObject, Type currentTest);
}