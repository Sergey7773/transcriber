package app.transcriber.generation.methods.supplier;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;

import java.util.List;

public interface ValueSupplierMethodListProducerFactory<T> {

    List<MethodProducer> create(MapperGenerationContext mapperGenerationContext,
                                TypesMapperGenerationInput typesMapperGenerationInput, T t);

}
