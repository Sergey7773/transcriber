package app.transcriber.generation.methods.apply;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.MethodSpec;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@AllArgsConstructor
public class ApplyTestListMethodGenerator {

    private final ApplyTestMethodGeneratorFactory applyTestMethodGeneratorFactory;

    public List<MethodSpec> generateApplyMethods(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {

        List<MethodProducer> applyList = applyTestMethodGeneratorFactory.create(mapperGenerationContext, typesMapperGenerationInput);
        return applyList.stream().map(Supplier::get).collect(Collectors.toList());
    }
}