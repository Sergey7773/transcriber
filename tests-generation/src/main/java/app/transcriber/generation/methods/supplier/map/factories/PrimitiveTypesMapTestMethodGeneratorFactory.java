package app.transcriber.generation.methods.supplier.map.factories;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.TestTypeGenerator;
import app.transcriber.generation.methods.supplier.ValueSupplierMethodListProducerFactory;
import app.transcriber.generation.methods.supplier.map.PrimitiveTypesMapMethodTestGenerator;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.CodeBlock;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;

@AllArgsConstructor
public class PrimitiveTypesMapTestMethodGeneratorFactory implements ValueSupplierMethodListProducerFactory<FieldsMapperGenerationInput> {

    @NonNull
    private Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter;

    protected TestTypeGenerator testTypeGenerator;

    @Override
    public List<MethodProducer> create(@NonNull MapperGenerationContext mapperGenerationContext,
                                       @NonNull TypesMapperGenerationInput typesMapperGenerationInput, @NonNull FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        setTypesOfTestsToBeGenerated();
        if (testTypeGenerator.isVerifySetterMethodTestNeeded())
            return Collections.singletonList(new PrimitiveTypesMapMethodTestGenerator(argsToCodeBlockConverter, mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, null, true, false));
        return Collections.emptyList();
    }

    private void setTypesOfTestsToBeGenerated() {
        testTypeGenerator.setVerifySetterMethodTestNeeded(true);
        testTypeGenerator.setVerifyTwiceSetterMethodTestNeeded(false);
    }
}