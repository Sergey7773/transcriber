package app.transcriber.generation.methods.supplier.produce;

import app.transcriber.api.exceptions.ValueProducerCreationException;
import app.transcriber.generation.conventions.*;
import app.transcriber.generation.methods.AbstractMethodsProducer;
import app.transcriber.generation.model.FieldsValueProducerGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.mockito.Mockito;

import javax.lang.model.element.Modifier;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@AllArgsConstructor
public abstract class TemplateValueProducerTestMethodsGenerator extends AbstractMethodsProducer {

    protected Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter;
    protected MapperGenerationContext mapperGenerationContext;
    protected TypesMapperGenerationInput typesMapperGenerationInput;
    protected FieldsValueProducerGenerationInput fieldsValueProducerGenerationInput;
    protected Class<?> currentTest;
    protected boolean verifySetterMethodTestNeeded;
    protected boolean verifyTwiceSetterMethodTestNeeded;


    protected MethodSpec.Builder createMethodDefinition() {
        String methodName = mapperGenerationContext.getNamingConventionsContext().getConvention(ProduceTestMethodNameConvention.class).apply(fieldsValueProducerGenerationInput, currentTest);
        AnnotationSpec valueProducerMethodProducerTestAnnotation = getAnnotationSpec();

        return MethodSpec.methodBuilder(methodName)
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(valueProducerMethodProducerTestAnnotation)
                .returns(void.class);
    }


    private AnnotationSpec getAnnotationSpec() {
        AnnotationSpec valueProducerMethodProducerTestAnnotation;
        if (!verifySetterMethodTestNeeded) {
            valueProducerMethodProducerTestAnnotation =
                    AnnotationSpec.builder(Test.class)
                            .addMember("expected", "$T.class", currentTest)
                            .build();
        } else {
            valueProducerMethodProducerTestAnnotation =
                    AnnotationSpec.builder(Test.class).build();
        }
        return valueProducerMethodProducerTestAnnotation;
    }


    @Override
    protected List<Type> thrownExceptions() {
        List<Type> result = new LinkedList<>(super.thrownExceptions());

        result.add(ValueProducerCreationException.class);
        return result;
    }

    @Override
    protected CodeBlock createMethodBody() {
        String methodName = mapperGenerationContext.getNamingConventionsContext().getConvention(ProduceMethodNameConvention.class).apply(fieldsValueProducerGenerationInput);
        String mockedTarget = TestsNamingConvention.TARGET_MOCKED_OBJECT.toString();
        String mapperClassName = mapperGenerationContext.getNamingConventionsContext().getConvention(MapperClassNameConvention.class).apply(typesMapperGenerationInput);
        Class<?> mockitoClassObject = Mockito.class;
        CodeBlock.Builder resultBuilder = CodeBlock.builder().addStatement("$T $L = $T.mock($T.class)", typesMapperGenerationInput.getTargetClass().getWrappedType(), mockedTarget, mockitoClassObject, typesMapperGenerationInput.getTargetClass().getWrappedType());
        resultBuilder.add(getMapStatement(mapperGenerationContext, typesMapperGenerationInput, fieldsValueProducerGenerationInput, currentTest, verifySetterMethodTestNeeded, verifyTwiceSetterMethodTestNeeded));
        resultBuilder.addStatement("$L.$L($L)", StringUtils.uncapitalize(mapperClassName), methodName, mockedTarget);
        resultBuilder.add(getMapVerifyStatement(mockitoClassObject, currentTest));
        return resultBuilder.build();
    }


    @Override
    protected Optional<CodeBlock> createJavadoc() {
        CodeBlock.Builder javadocBuilder = CodeBlock.builder();
        super.createJavadoc().ifPresent(codeBlock -> javadocBuilder.add(codeBlock).add(CodeBlock.of("\n")));

        List<CodeBlock> javadocBlocks = new LinkedList<>();
        javadocBlocks.add(CodeBlock.of("Verifies the setting of the $L field in mocked <i>target</i>, according to the Mocked value produced by the $S value producer",
                fieldsValueProducerGenerationInput.getTargetField().getName(), fieldsValueProducerGenerationInput.getValueProducerName()));
        javadocBlocks.add(CodeBlock.of("@throws $T if an exception occurred during the creation of a required valueProducer", ValueProducerCreationException.class));
        CodeBlock throwsJavadocBlock = CodeBlock.join(javadocBlocks, "\n");

        javadocBuilder.add(throwsJavadocBlock);

        return Optional.of(javadocBuilder.build());
    }

    public  CodeBlock getMapVerifyStatement(Class<?> mockitoClassObject, Type currentTest){
            CodeBlock.Builder resultBuilder = CodeBlock.builder();
            if (currentTest == null || verifySetterMethodTestNeeded) {
                String mockedTarget = TestsNamingConvention.TARGET_MOCKED_OBJECT.toString();
                String setterMethodName = mapperGenerationContext.getNamingConventionsContext().getConvention(SetterMethodNameConvention.class).apply(fieldsValueProducerGenerationInput.getTargetField());
                resultBuilder
                        .addStatement("$T.verify($L, $T.times(1)).$L($L)", mockitoClassObject, mockedTarget, mockitoClassObject, setterMethodName, TestsNamingConvention.SOURCE_FIELD_VALUE)
                        .build();
            }

            return resultBuilder.build();
        }

    public abstract CodeBlock getMapStatement(MapperGenerationContext mapperGenerationContext,
                                              TypesMapperGenerationInput typesMapperGenerationInput,
                                              FieldsValueProducerGenerationInput fieldsValueProducerGenerationInput, Type currentTest, boolean verifySetterMethodTestNeeded,
                                              boolean verifyTwiceSetterMethodTestNeeded);
}