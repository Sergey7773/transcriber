package app.transcriber.generation.methods.supplier.set;

import app.transcriber.generation.conventions.*;
import app.transcriber.generation.methods.AbstractMethodsProducer;
import app.transcriber.generation.methods.supplier.ConstantSetMethodArgumentFormatter;
import app.transcriber.generation.model.ConstantMappingGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.mockito.Mockito;

import javax.lang.model.element.Modifier;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public class ConstantSetMethodProducerTestGenerator extends AbstractMethodsProducer {

    private final MapperGenerationContext mapperGenerationContext;

    private final TypesMapperGenerationInput typesMapperGenerationInput;

    private final ConstantMappingGenerationInput constantMappingGenerationInput;

    private final ConstantSetMethodArgumentFormatter constantSetMethodArgumentFormatter;

    @Override
    protected MethodSpec.Builder createMethodDefinition() {
        return MethodSpec.methodBuilder(mapperGenerationContext.getNamingConventionsContext().getConvention(SetConstantValueTestMethodNameConvention.class).apply(constantMappingGenerationInput))
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(Test.class)
                .returns(void.class);
    }

    @Override
    protected CodeBlock createMethodBody() {
        String setterName = mapperGenerationContext.getNamingConventionsContext().getConvention(SetterMethodNameConvention.class).apply(constantMappingGenerationInput.getTargetField());
        String testedMethodName = mapperGenerationContext.getNamingConventionsContext().getConvention(SetConstantValueMethodNameConvention.class).apply(constantMappingGenerationInput);
        String mapperClassName = mapperGenerationContext.getNamingConventionsContext().getConvention(MapperClassNameConvention.class).apply(typesMapperGenerationInput);
        String constantValue = constantSetMethodArgumentFormatter.format(constantMappingGenerationInput.getValue());
        Class<?> mockitoClassObject = Mockito.class;

        return CodeBlock.builder()
                .addStatement("$T $L = $T.mock($T.class) ", typesMapperGenerationInput.getTargetClass().getWrappedType(), TestsNamingConvention.TARGET_MOCKED_OBJECT, mockitoClassObject, typesMapperGenerationInput.getTargetClass().getWrappedType())
                .addStatement("$L.$L($L)", StringUtils.uncapitalize(mapperClassName),
                        testedMethodName, TestsNamingConvention.TARGET_MOCKED_OBJECT)
                .addStatement("$T.verify($L , $T.times(1)). $L($T.eq($L))", mockitoClassObject, TestsNamingConvention.TARGET_MOCKED_OBJECT, mockitoClassObject, setterName, mockitoClassObject, constantValue)
                .build();
    }

    @Override
    protected Optional<CodeBlock> createJavadoc() {
        CodeBlock.Builder javadocBuilder = CodeBlock.builder();
        super.createJavadoc().ifPresent(codeBlock -> javadocBuilder.add(codeBlock).add(CodeBlock.of("\n")));

        List<CodeBlock> javadocBlocks = new LinkedList<>();
        javadocBlocks.add(CodeBlock.of("Verifies the setting of a constant value in the $L field of mocked <i>target</i>", constantMappingGenerationInput.getTargetField().getName()));
        CodeBlock throwsJavadocBlock = CodeBlock.join(javadocBlocks, "\n");

        javadocBuilder.add(throwsJavadocBlock);

        return Optional.of(javadocBuilder.build());
    }
}