package app.transcriber.generation.methods.supplier.map.factories;

import app.transcriber.api.exceptions.MapperCreationException;
import app.transcriber.api.exceptions.MappingException;
import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.TestTypeGenerator;
import app.transcriber.generation.methods.supplier.ValueSupplierMethodListProducerFactory;
import app.transcriber.generation.methods.supplier.map.SubEntityCollectionMapThrowExceptionTestMethodGenerator;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.CodeBlock;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

@AllArgsConstructor
public class SubEntityCollectionMapTestMethodGeneratorFactory implements ValueSupplierMethodListProducerFactory<FieldsMapperGenerationInput> {

    @NonNull
    private Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter;

    protected TestTypeGenerator testTypeGenerator;

    @Override
    public List<MethodProducer> create(@NonNull MapperGenerationContext mapperGenerationContext,
                                       @NonNull TypesMapperGenerationInput typesMapperGenerationInput, @NonNull FieldsMapperGenerationInput fieldsMapperGenerationInput) {

        setTypesOfTestsToBeGenerated();
        List<Class<?>> typesOfTestsToBeGenerated = testTypeGenerator.getThrownExceptions();
        List<MethodProducer> listOfGeneratedTest = new LinkedList<>();

        for (Class<?> type : typesOfTestsToBeGenerated) {
            listOfGeneratedTest.add(new SubEntityCollectionMapThrowExceptionTestMethodGenerator(argsToCodeBlockConverter,
                    mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, type, false, false));
        }
        if (testTypeGenerator.isVerifySetterMethodTestNeeded()) {
            listOfGeneratedTest.add(new SubEntityCollectionMapThrowExceptionTestMethodGenerator(argsToCodeBlockConverter,
                    mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, null, true, false));
        }

        if (testTypeGenerator.isVerifyTwiceSetterMethodTestNeeded()) {
            listOfGeneratedTest.add(new SubEntityCollectionMapThrowExceptionTestMethodGenerator(argsToCodeBlockConverter,
                    mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, null, false, true));
        }

        return listOfGeneratedTest;
    }

    private void setTypesOfTestsToBeGenerated() {
        List<Class<?>> result = new LinkedList<>();
        result.add(MapperCreationException.class);
        result.add(MappingException.class);
        result.add(NullPointerException.class);
        testTypeGenerator.setThrownExceptions(result);
        testTypeGenerator.setVerifySetterMethodTestNeeded(true);
        testTypeGenerator.setVerifyTwiceSetterMethodTestNeeded(true);
    }
}