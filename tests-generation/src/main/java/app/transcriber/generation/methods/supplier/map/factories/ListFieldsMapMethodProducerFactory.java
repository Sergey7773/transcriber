package app.transcriber.generation.methods.supplier.map.factories;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.supplier.ValueSupplierMethodListProducerFactory;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;

@AllArgsConstructor
public class ListFieldsMapMethodProducerFactory implements ValueSupplierMethodListProducerFactory<FieldsMapperGenerationInput> {

    @NonNull
    private Map<BiPredicate<MapperGenerationContext, FieldsMapperGenerationInput>, ValueSupplierMethodListProducerFactory<FieldsMapperGenerationInput>> factoriesMapOfList;

    @Override
    public List<MethodProducer> create(MapperGenerationContext mapperGenerationContext,
                                       TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        return factoriesMapOfList.keySet().
                stream()
                .filter(methodMapperPredicate -> methodMapperPredicate.test(mapperGenerationContext, fieldsMapperGenerationInput))
                .findFirst()
                .map(x -> factoriesMapOfList.get(x).create(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput))
                .orElse(Collections.emptyList());
    }
}