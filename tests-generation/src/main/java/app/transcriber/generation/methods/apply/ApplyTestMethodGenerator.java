package app.transcriber.generation.methods.apply;

import app.transcriber.api.exceptions.*;
import app.transcriber.generation.conventions.*;
import app.transcriber.generation.methods.AbstractMethodsProducer;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.tools.GenerationType;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.mockito.Mockito;

import javax.lang.model.element.Modifier;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
public class ApplyTestMethodGenerator extends AbstractMethodsProducer {

    private final MapperGenerationContext mapperGenerationContext;

    private final TypesMapperGenerationInput typesMapperGenerationInput;

    private final String methodName;

    private final GenerationType testType;


    @Override
    protected MethodSpec.Builder createMethodDefinition() {
        String testName = mapperGenerationContext.getNamingConventionsContext().getConvention(ApplyTestMethodNameConvention.class).apply(methodName);
        return MethodSpec.methodBuilder(testName)
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(Test.class)
                .returns(void.class);
    }

    @Override
    protected List<Type> thrownExceptions() {
        List<Type> result = new LinkedList<>(super.thrownExceptions());

        result.add(MappingException.class);
        result.add(MappingOperatorCreationException.class);
        result.add(TargetModelObjectCreationException.class);
        result.add(MapperCreationException.class);
        result.add(ValueProducerCreationException.class);
        return result;
    }

    @Override
    protected CodeBlock createMethodBody() {
        String className = mapperGenerationContext.getNamingConventionsContext().getConvention(MapperClassNameConvention.class).apply(typesMapperGenerationInput);
        String classNameUncapitalized = StringUtils.uncapitalize(className);
        Type targetClass = typesMapperGenerationInput.getTargetClass().getWrappedType();
        Type sourceClass = typesMapperGenerationInput.getSourceClass().getWrappedType();
        Class<?> mockitoClassObject = Mockito.class;
        CodeBlock.Builder resultBuilder = CodeBlock.builder();
        resultBuilder
                .addStatement("$T $L = $T.mock($T.class)", targetClass, TestsNamingConvention.TARGET_MOCKED_OBJECT, mockitoClassObject, targetClass)
                .addStatement("$T $L = $T.mock($T.class)", sourceClass, TestsNamingConvention.SOURCE_MOCKED_OBJECT, mockitoClassObject, sourceClass)
                .addStatement("$T.when($L.$L($T.class)).thenReturn($L)", mockitoClassObject,
                        NamingConvention.TARGET_MODEL_OBJECT_FACTORY_MEMBER, NamingConvention.TARGET_MODEL_OBJECT_FACTORY_CREATE_FUNCTION,
                        typesMapperGenerationInput.getTargetClass().getWrappedType(), TestsNamingConvention.TARGET_MOCKED_OBJECT)
                .addStatement("$L = $T.spy($L)", classNameUncapitalized, mockitoClassObject, classNameUncapitalized)
                .add(CodeBlock.join(generateMapMethodsInvocations(classNameUncapitalized, mockitoClassObject), ""))
                .add(CodeBlock.join(generateProduceMethodsInvocations(classNameUncapitalized, mockitoClassObject), ""))
                .add(CodeBlock.join(generateSetMethodsInvocations(classNameUncapitalized, mockitoClassObject), ""))
                .addStatement("$L.$L($L)", classNameUncapitalized, NamingConvention.OPERATOR_APPLY_METHOD, TestsNamingConvention.SOURCE_MOCKED_OBJECT);
        if (testType.equals(GenerationType.FIELDS_MAPPER_GENERATION)) {
            resultBuilder.addStatement("$T.verify($L, $T.times(1)).$L($T.eq($L), $T.eq($L))", mockitoClassObject, classNameUncapitalized, mockitoClassObject, methodName, mockitoClassObject, TestsNamingConvention.SOURCE_MOCKED_OBJECT, mockitoClassObject, TestsNamingConvention.TARGET_MOCKED_OBJECT);
        }
        else {
            resultBuilder.addStatement("$T.verify($L, $T.times(1)).$L($T.eq($L))", mockitoClassObject, classNameUncapitalized, mockitoClassObject, methodName, mockitoClassObject, TestsNamingConvention.TARGET_MOCKED_OBJECT);
        }

        return resultBuilder.build();

    }

    private List<CodeBlock> generateMapMethodsInvocations(String classSimpleName, Class<?> mockitoClassObject) {
        MapMethodNameConvention mapMethodNameConvention = mapperGenerationContext.getNamingConventionsContext().getConvention(MapMethodNameConvention.class);

        return typesMapperGenerationInput.getFieldsMappersGenerationInputs()
                .stream().map(mapMethodNameConvention)
                .map(doNothingStatement -> CodeBlock.builder().addStatement("$T.doNothing().when($L).$L($T.any(), $T.any())", mockitoClassObject,
                        classSimpleName, doNothingStatement, mockitoClassObject, mockitoClassObject).build())
                .collect(Collectors.toList());
    }

    private List<CodeBlock> generateProduceMethodsInvocations(String classSimpleName, Class<?> mockitoClassObject) {
        ProduceMethodNameConvention produceMethodNameConvention = mapperGenerationContext.getNamingConventionsContext().getConvention(ProduceMethodNameConvention.class);

        return typesMapperGenerationInput.getFieldsValueProducersGenerationInputs()
                .stream().map(produceMethodNameConvention)
                .map(doNothingStatement -> CodeBlock.builder().addStatement("$T.doNothing().when($L).$L($T.any())", mockitoClassObject,
                        classSimpleName, doNothingStatement, mockitoClassObject).build())
                .collect(Collectors.toList());
    }

    private List<CodeBlock> generateSetMethodsInvocations(String classSimpleName, Class<?> mockitoClassObject) {
        SetConstantValueMethodNameConvention setConstantValueMethodNameConvention = mapperGenerationContext.getNamingConventionsContext().getConvention(SetConstantValueMethodNameConvention.class);

        return typesMapperGenerationInput.getConstantMappingsGenerationInputs()
                .stream().map(setConstantValueMethodNameConvention)
                .map(doNothingStatement -> CodeBlock.builder().addStatement("$T.doNothing().when($L).$L($T.any())", mockitoClassObject,
                        classSimpleName, doNothingStatement, mockitoClassObject).build())
                .collect(Collectors.toList());
    }

    @Override
    protected Optional<CodeBlock> createJavadoc() {
        CodeBlock.Builder javadocBuilder = CodeBlock.builder();
        super.createJavadoc().ifPresent(javadocBuilder::add);

        List<CodeBlock> javadocBlocks = new LinkedList<>();
        javadocBlocks.add(CodeBlock.of("Verifies the calling for the {@code $L} method , applying to it our mocked parameters.", methodName));
        javadocBlocks.add(CodeBlock.of("@throws $T or @throws $T or @throws $T or @throws $T or @throws $T - if any exception occurred during mocking one of the methods calling",
                MappingException.class, MappingOperatorCreationException.class, TargetModelObjectCreationException.class,
                MapperCreationException.class, ValueProducerCreationException.class));

        javadocBuilder.add(CodeBlock.join(javadocBlocks, "\n"));

        return Optional.of(javadocBuilder.build());
    }
}