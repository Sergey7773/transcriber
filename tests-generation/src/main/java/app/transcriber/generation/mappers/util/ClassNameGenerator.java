package app.transcriber.generation.mappers.util;

import app.transcriber.generation.conventions.MapperClassNameConvention;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.ClassName;

public class ClassNameGenerator {

    public String generatedClassNameToLowerCase(ClassName generatedClassName) {
        String generatedClassSimpleName = generatedClassName.simpleName();
        char[] simpleNameToLowerCase = generatedClassSimpleName.toCharArray();
        simpleNameToLowerCase[0] = Character.toLowerCase(simpleNameToLowerCase[0]);
        return new String(simpleNameToLowerCase);
    }

    public ClassName getMapperClassName(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        String mapperGeneratorClassType = mapperGenerationContext.getNamingConventionsContext().getConvention(MapperClassNameConvention.class).apply(typesMapperGenerationInput);
        return ClassName.get(mapperGenerationContext.getOutputPackage(), mapperGeneratorClassType);
    }
}
