package app.transcriber.generation.mappers.util;

import app.transcriber.api.interfaces.Mapper;
import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.adapters.ClassAdapter;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import org.mockito.Mock;

import javax.lang.model.element.Modifier;

public class ParameterizedTypeFieldGenerator {

    public FieldSpec generateParameterizedTypeFieldParams(MapperGenerationContext mapperGenerationContext) {

        String sourceSubEntityName = NamingConvention.SOURCE_SUB_ENTITY_OBJECT.toString();
        String targetSubEntityName = NamingConvention.TARGET_SUB_ENTITY_OBJECT.toString();
        String sourceSubEntityNameFirstCharInLowerCase = sourceSubEntityName.substring(0, 1).toLowerCase() + sourceSubEntityName.substring(1);
        ClassAdapter sourceFieldType = mapperGenerationContext.getSourceClasses().get(sourceSubEntityName);
        ClassAdapter targetFieldType = mapperGenerationContext.getTargetClasses().get(targetSubEntityName);
        if (sourceFieldType != null && targetFieldType != null) {
            ParameterizedTypeName mapperType = ParameterizedTypeName.get(Mapper.class, sourceFieldType.getWrappedType(), targetFieldType.getWrappedType());
            String parameterizedTypeName = getParameterizedTypeName(sourceSubEntityNameFirstCharInLowerCase, targetSubEntityName);
            return FieldSpec.builder(mapperType, parameterizedTypeName, Modifier.PRIVATE).addAnnotation(Mock.class)
                    .addJavadoc("Used to create instance of $T", mapperType).build();
        }
        return null;
    }


    public String getParameterizedTypeName(String sourceFieldTypeName, String targetFieldType) {
        return sourceFieldTypeName + "To" + targetFieldType + "Mapper";
    }
}