package app.transcriber.generation.mappers.util;

import app.transcriber.generation.conventions.NamingConvention;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import org.junit.Before;

import javax.lang.model.element.Modifier;
import java.util.List;
import java.util.Optional;

public class TestSetupMethodGenerator {

    public Optional<MethodSpec> generateTestSetupMethod(ClassName generatedClassName, List<String> fieldsNames, FieldSpec mapperField) {

        MethodSpec resultBuilder = MethodSpec.methodBuilder(NamingConvention.SETUP.toString())
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(Before.class)
                .returns(void.class).addStatement("$N = new $T($L)",
                        mapperField, generatedClassName,
                        String.join(", ", fieldsNames)).build();

        return Optional.of(resultBuilder);
    }

}

