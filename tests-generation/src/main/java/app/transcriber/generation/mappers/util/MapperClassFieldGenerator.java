package app.transcriber.generation.mappers.util;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;

import javax.lang.model.element.Modifier;

public class MapperClassFieldGenerator {
    public FieldSpec generateTestedClassField(ClassName generatedClassName, String generatedClassSimpleName) {
        return FieldSpec.builder(generatedClassName, generatedClassSimpleName, Modifier.PRIVATE)
                .addJavadoc("Used to create reference to the generated class").build();
    }
}
