package app.transcriber.generation.mappers.constructor;

import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class AllArgsConstructorTestGeneratorFactory {

    public AllArgsConstructorTestGenerator create(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, List<String> fieldsNames, String nullField) {
        return new AllArgsConstructorTestGenerator(mapperGenerationContext, typesMapperGenerationInput, fieldsNames, nullField);
    }
}