package app.transcriber.generation.mappers.constructor;

import app.transcriber.generation.fields.MapperFieldsGenerator;
import app.transcriber.generation.mappers.util.FieldsNameGenerator;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.MethodSpec;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.mockito.Mock;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@AllArgsConstructor
public class ConstructorTestsMethodGenerator {

    @NonNull
    private final AllArgsConstructorTestGeneratorFactory allArgsConstructorTestGeneratorFactory;

    public List<MethodSpec> generate(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, MapperFieldsGenerator mapperFieldsGenerator, FieldsNameGenerator fieldsNameGenerator) {
        List<MethodSpec> constructorMethods = new LinkedList<>();
        List<String> fieldNames = fieldsNameGenerator.getFieldsNames(
                mapperFieldsGenerator.buildFieldsSpecForClassFields(mapperGenerationContext, typesMapperGenerationInput, Collections.singleton(Mock.class)));
        for (int i = 0; i < fieldNames.size(); i++) {
            List<String> fieldsToPassAsArgs = new LinkedList<>(fieldNames);
            fieldsToPassAsArgs.set(i, null);
            String nullField = fieldNames.get(i);
            constructorMethods.add(allArgsConstructorTestGeneratorFactory.create(mapperGenerationContext, typesMapperGenerationInput, fieldsToPassAsArgs, nullField).get());
        }
        return constructorMethods;
    }
}