package app.transcriber.generation.mappers.constructor;

import app.transcriber.generation.conventions.AllArgsConstructorTestNameConvention;
import app.transcriber.generation.conventions.MapperClassNameConvention;
import app.transcriber.generation.methods.AbstractMethodsProducer;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import lombok.AllArgsConstructor;
import org.junit.Test;

import javax.lang.model.element.Modifier;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public class AllArgsConstructorTestGenerator extends AbstractMethodsProducer {

    private final MapperGenerationContext mapperGenerationContext;

    private final TypesMapperGenerationInput typesMapperGenerationInput;

    private final List<String> fieldsName;

    private final String nullField;

    @Override
    protected MethodSpec.Builder createMethodDefinition() {
        String methodName = mapperGenerationContext.getNamingConventionsContext().getConvention(AllArgsConstructorTestNameConvention.class).apply(nullField, IllegalArgumentException.class);
        AnnotationSpec allArgsConstructorTestAnnotation =
                AnnotationSpec.builder(Test.class)
                        .addMember("expected", "$T.class", IllegalArgumentException.class)
                        .build();

        return MethodSpec.methodBuilder(methodName)
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(allArgsConstructorTestAnnotation)
                .returns(void.class);
    }

    @Override
    protected CodeBlock createMethodBody() {
        String className = mapperGenerationContext.getNamingConventionsContext().getConvention(MapperClassNameConvention.class).apply(typesMapperGenerationInput);
        return CodeBlock.builder()
                .addStatement("new $L($L)",
                        className,
                        String.join(", ", fieldsName))
                .build();
    }

    @Override
    protected Optional<CodeBlock> createJavadoc() {
        String className = mapperGenerationContext.getNamingConventionsContext().getConvention(MapperClassNameConvention.class).apply(typesMapperGenerationInput);
        CodeBlock.Builder javadocBuilder = CodeBlock.builder();
        super.createJavadoc().ifPresent(javadocBuilder::add);

        List<CodeBlock> javadocBlocks = new LinkedList<>();
        javadocBlocks.add(CodeBlock.of("Applies a constructor logic to an object of type {@code $L} with one null argument", className));
        javadocBlocks.add(CodeBlock.of("@throws $T - Since all the constructor arguments must be non-null", IllegalArgumentException.class));

        javadocBuilder.add(CodeBlock.join(javadocBlocks, "\n"));

        return Optional.of(javadocBuilder.build());
    }
}