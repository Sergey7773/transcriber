package app.transcriber.generation.mappers.util;

import com.squareup.javapoet.FieldSpec;

import java.util.ArrayList;
import java.util.List;

public class FieldsNameGenerator {

    public List<String> getFieldsNames(List<FieldSpec> requiredFields) {
        List<String> fieldsName = new ArrayList<>(requiredFields.size());
        for (FieldSpec currentRequiredField : requiredFields) {
            fieldsName.add(currentRequiredField.name);
        }
        return fieldsName;
    }
}