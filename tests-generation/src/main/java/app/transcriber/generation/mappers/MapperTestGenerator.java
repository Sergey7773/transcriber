package app.transcriber.generation.mappers;

import app.transcriber.generation.TranscriberGenerator;
import app.transcriber.generation.conventions.MapperClassNameConvention;
import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.fields.MapperFieldsGenerator;
import app.transcriber.generation.mappers.constructor.ConstructorTestsMethodGenerator;
import app.transcriber.generation.mappers.util.*;
import app.transcriber.generation.methods.apply.ApplyTestListMethodGenerator;
import app.transcriber.generation.methods.supplier.SupplierTestMethodGenerator;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.*;
import lombok.Builder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.lang.model.element.Modifier;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Builder
public class MapperTestGenerator implements TranscriberGenerator {

    private final MapperFieldsGenerator mapperFieldsGenerator;

    private final ConstructorTestsMethodGenerator constructorTestsMethodGenerator;

    private final SupplierTestMethodGenerator supplierTestMethodGenerator;

    private final ApplyTestListMethodGenerator applyTestListMethodGenerator;

    private final FieldsNameGenerator fieldsNameGenerator;

    private final ParameterizedTypeFieldGenerator parameterizedTypeFieldGenerator;

    private final TestSetupMethodGenerator testSetupMethodGenerator;

    private final MapperClassFieldGenerator mapperClassFieldGenerator;

    private final ClassNameGenerator classNameGenerator;

    public List<JavaFile> generate(MapperGenerationContext generationContext) {
        return Optional.ofNullable(generationContext.getTypesMapperGenerationInputs())
                .orElse(Collections.emptyList())
                .stream()
                .map(typesMapperGenerationInput -> this.generateMapperTest(generationContext, typesMapperGenerationInput, fieldsNameGenerator, parameterizedTypeFieldGenerator, testSetupMethodGenerator))
                .collect(Collectors.toList());
    }

    public JavaFile generateMapperTest(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, FieldsNameGenerator fieldsNameGenerator, ParameterizedTypeFieldGenerator parameterizedTypeFieldGenerator, TestSetupMethodGenerator testSetupMethodGenerator) {
        String mapperClassName = mapperGenerationContext.getNamingConventionsContext().getConvention(MapperClassNameConvention.class).getMapperTestClassName(typesMapperGenerationInput);
        AnnotationSpec allArgsConstructorTestAnnotation =
                AnnotationSpec.builder(RunWith.class)
                        .addMember("value", "$T.class", MockitoJUnitRunner.class)
                        .build();

        TypeSpec.Builder mapperTestSpecBuilder = TypeSpec.classBuilder(mapperClassName)
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(allArgsConstructorTestAnnotation);

        ClassName generatedClassName = classNameGenerator.getMapperClassName(mapperGenerationContext, typesMapperGenerationInput);
        String generatedClassSimpleName = classNameGenerator.generatedClassNameToLowerCase(generatedClassName);

        List<FieldSpec> mapperConstructorFields = mapperFieldsGenerator.buildFieldsSpecForClassFields(mapperGenerationContext, typesMapperGenerationInput, Collections.singletonList(Mock.class));
        mapperTestSpecBuilder.addFields(mapperConstructorFields);

        if (!typesMapperGenerationInput.getSourceClass().getSimpleName().equals(NamingConvention.SOURCE_SUB_ENTITY_OBJECT.toString())) {
            FieldSpec parameterizedTypeFieldParams = parameterizedTypeFieldGenerator.generateParameterizedTypeFieldParams(mapperGenerationContext);
            if (parameterizedTypeFieldParams != null) {
                mapperTestSpecBuilder.addField(parameterizedTypeFieldParams);
            }
        }


        FieldSpec testedObjectField = mapperClassFieldGenerator.generateTestedClassField(generatedClassName, generatedClassSimpleName);
        mapperTestSpecBuilder.addField(testedObjectField);

        if (!mapperConstructorFields.isEmpty()) {
            List<String> fieldsNames = fieldsNameGenerator.getFieldsNames(mapperConstructorFields);
            testSetupMethodGenerator.generateTestSetupMethod(generatedClassName, fieldsNames, testedObjectField).ifPresent(mapperTestSpecBuilder::addMethod);
        }

        mapperTestSpecBuilder.addMethods(constructorTestsMethodGenerator.generate(mapperGenerationContext, typesMapperGenerationInput, mapperFieldsGenerator, fieldsNameGenerator));
        mapperTestSpecBuilder.addMethods(applyTestListMethodGenerator.generateApplyMethods(mapperGenerationContext, typesMapperGenerationInput));
        mapperTestSpecBuilder.addMethods(supplierTestMethodGenerator.generate(mapperGenerationContext, typesMapperGenerationInput));

        addJavadocToClass(mapperTestSpecBuilder, typesMapperGenerationInput);

        return JavaFile.builder(mapperGenerationContext.getOutputPackage(), mapperTestSpecBuilder.build())
                .skipJavaLangImports(true)
                .build();
    }

    private void addJavadocToClass(TypeSpec.Builder mapperTestSpecBuilder, TypesMapperGenerationInput typesMapperGenerationInput) {
        mapperTestSpecBuilder.addJavadoc("The generated logic which is responsible for testing converted {@code $T} instances to {@code $T} instances.",
                typesMapperGenerationInput.getSourceClass().getWrappedType(), typesMapperGenerationInput.getTargetClass().getWrappedType());
    }
}