package app.transcriber.example.generated;

import app.transcriber.api.exceptions.*;
import app.transcriber.api.interfaces.*;
import app.transcriber.example.source.Source;
import app.transcriber.example.source.SourceSubEntity;
import app.transcriber.example.target.Target;
import app.transcriber.example.target.TargetSubEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.function.Function;
import java.util.function.Supplier;

@RunWith(MockitoJUnitRunner.class)
public class SourceToTargetMapperTest {

    private SourceToTargetMapper sourceToTargetMapper;

    @Mock
    private TargetModelObjectFactory targetModelObjectFactory;

    @Mock
    private MapperFactory mapperFactory;

    @Mock
    private ValueProducerFactory valueProducerFactory;

    @Mock
    private MappingOperatorFactory mappingOperatorFactory;

    @Mock
    private Mapper<SourceSubEntity, TargetSubEntity> sourceSubEntityToTargetSubEntityMapper;

    @Before
    public void setup() throws TargetModelObjectCreationException {
        sourceToTargetMapper = new SourceToTargetMapper(targetModelObjectFactory,mappingOperatorFactory,mapperFactory,
                valueProducerFactory);
    }

    ////
////    @Test(expected = NullPointerException.class)
////    public void allArgsConstructor_onNullTargetModelObjectFactory_throwsIllegalArgumentException(){
////        new SourceToTargetMapper(null, mapperFactory,mappingOperatorFactory,
////                valueProducerFactory);
////    }
////
////    @Test(expected = NullPointerException.class)
////    public void allArgsConstructor_onNullMappingOperatorFactory_throwsIllegalArgumentException() {
////        new SourceToTargetMapper(targetModelObjectFactory, mapperFactory,null,
////                valueProducerFactory);
////    }
////
////    @Test(expected = NullPointerException.class)
////    public void allArgsConstructor_onNullMapperFactory_throwsIllegalArgumentException() {
////        new SourceToTargetMapper(targetModelObjectFactory, null,mappingOperatorFactory,
////                valueProducerFactory);
////    }
////
////    @Test(expected = NullPointerException.class)
////    public void allArgsConstructor_onNullValueProducerFactory_throwsIllegalArgumentException() {
////        new SourceToTargetMapper(targetModelObjectFactory, mapperFactory,mappingOperatorFactory,
////                null);
////    }
//
    @Test
    public void TestApply() throws MappingException, MappingOperatorCreationException, TargetModelObjectCreationException, MapperCreationException, ValueProducerCreationException {
        Source mockedSource = Mockito.mock(Source.class);
        Target mockedTarget = Mockito.mock(Target.class);
        Mockito.when(targetModelObjectFactory.create(Target.class)).thenReturn(mockedTarget);

        sourceToTargetMapper = Mockito.spy(sourceToTargetMapper);
        Mockito.doNothing().when(sourceToTargetMapper).mapSourceSubEntityToTargetSubEntity(Mockito.any(), Mockito.any());
        Mockito.doNothing().when(sourceToTargetMapper).mapPrimitiveSourceColToPrimitiveTargetCol(Mockito.any(), Mockito.any());
        Mockito.doNothing().when(sourceToTargetMapper).mapSubEntitiesSourceColToSubEntitiesTargetCol(Mockito.any(), Mockito.any());
        Mockito.doNothing().when(sourceToTargetMapper).mapSourceSubEntityFieldToSubEntityFetchedField(Mockito.any(), Mockito.any());
        Mockito.doNothing().when(sourceToTargetMapper).mapNameToFirstName(Mockito.any(), Mockito.any());
        Mockito.doNothing().when(sourceToTargetMapper).mapMailToEMail(Mockito.any(), Mockito.any());
        Mockito.doNothing().when(sourceToTargetMapper).produceProducedFieldValue(Mockito.any());
        Mockito.doNothing().when(sourceToTargetMapper).setConstantFieldConstantValue(Mockito.any());

        sourceToTargetMapper.apply(mockedSource);
        Mockito.verify(sourceToTargetMapper, Mockito.times(1))
                .mapSourceSubEntityToTargetSubEntity(Mockito.eq(mockedSource), Mockito.eq(mockedTarget));
    }



    @Test
    public void TestmappingNameToFirstName() throws MappingOperatorCreationException {
        Source mockedSource = Mockito.mock(Source.class);
        Target mockedTarget = Mockito.mock(Target.class);

        String mappedValue = "something";
        String mappedValueSubstring = "som";
        String mappedValueReverse = "mos";

        Function<Object, Object> substringFunction = (Object) -> mappedValueSubstring;
        Function<Object, Object> reverseFunction = (Object) -> mappedValueReverse;


        Mockito.when(mockedSource.getName()).thenReturn(mappedValue);
        Mockito.when(mappingOperatorFactory.createOperator(Mockito.eq("Substring"), Mockito.eq(Arrays.asList(1, 3)))).thenReturn(substringFunction);
        Mockito.when(mappingOperatorFactory.createOperator(Mockito.eq("Reverse"), Mockito.eq(Collections.emptyList()))).thenReturn(reverseFunction);

        sourceToTargetMapper.mapNameToFirstName(mockedSource,mockedTarget);
        Mockito.verify(mockedTarget,Mockito.times(1)).setFirstName(mappedValueReverse);

    }

//    @Test
//    public void TestmappingNameToFirstName22() throws MappingOperatorCreationException {
//        Source mockedSource = Mockito.mock(Source.class);
//        Target mockedTarget = Mockito.mock(Target.class);
//
//        String mappedValue = "something";
//        String mappedValueSubstring = "som";
//        String mappedValueReverse = "mos";
//
//        Function<Object, Object> substringFunction = (Object) -> mappedValueSubstring;
//        Function<Object, Object> reverseFunction = (Object) -> mappedValueReverse;
//
//
//        Mockito.when(mockedSource.getName()).thenReturn(mappedValue);
//        Mockito.when(mappingOperatorFactory.createOperator(Mockito.eq(Mockito.any()), Mockito.eq(Mockito.any()))).thenReturn(substringFunction);
//        Mockito.when(mappingOperatorFactory.createOperator(Mockito.eq(Mockito.any()), Mockito.eq(Mockito.any()))).thenReturn(reverseFunction);
//
//        sourceToTargetMapper.mapNameToFirstName(mockedSource,mockedTarget);
//        Mockito.verify(mockedTarget,Mockito.times(1)).setFirstName(mappedValueReverse);
//
//    }

    @Test
    public void TestMappingSourceSubEntityToTargetSubEntity() throws MapperCreationException, MappingException {
        //initialize
        Source mockedSource = Mockito.mock(Source.class);
        Target mockedTarget = Mockito.mock(Target.class);
        TargetSubEntity mockedTargetSubEntity = Mockito.mock(TargetSubEntity.class);


        //setup
        Mockito.when(mapperFactory.createMapper(SourceSubEntity.class, TargetSubEntity.class)).thenReturn(sourceSubEntityToTargetSubEntityMapper);
        Mockito.when(sourceSubEntityToTargetSubEntityMapper.apply(mockedSource.getSourceSubEntity())).thenReturn(mockedTargetSubEntity);

        //execute
        sourceToTargetMapper.mapSourceSubEntityToTargetSubEntity(mockedSource, mockedTarget);

        //verify
        Mockito.verify(mockedTarget, Mockito.times(1)).setTargetSubEntity(Mockito.eq(mockedTargetSubEntity));
    }

    @Test(expected = NullPointerException.class)
    public void TestMappingSourceSubEntityToTargetSubEntity_ThrowsNullPointerExexption() throws MappingException, MapperCreationException {
        //initialize
        Source mockedSource = Mockito.mock(Source.class);
        Target mockedTarget = Mockito.mock(Target.class);
        TargetSubEntity mockedTargetSubEntity = Mockito.mock(TargetSubEntity.class);


        //setup
        Mockito.when(mapperFactory.createMapper(SourceSubEntity.class, TargetSubEntity.class)).thenReturn(sourceSubEntityToTargetSubEntityMapper);
        Mockito.when(sourceSubEntityToTargetSubEntityMapper.apply(Mockito.any()))
                .thenThrow(NullPointerException.class);

        //execute
        sourceToTargetMapper.mapSourceSubEntityToTargetSubEntity(mockedSource, mockedTarget);

        //verify
        Mockito.verify(mockedTarget, Mockito.times(1)).setTargetSubEntity(Mockito.eq(mockedTargetSubEntity));
    }

    @Test(expected = MapperCreationException.class)
    public void TestMappingSourceSubEntityToTargetSubEntity_ThrowsMapperCreationException() throws MapperCreationException, MappingException {
        //initialize
        Source mockedSource = Mockito.mock(Source.class);
        Target mockedTarget = Mockito.mock(Target.class);

        //setup
        Mockito.when(mapperFactory.createMapper(SourceSubEntity.class, TargetSubEntity.class)).thenThrow(MapperCreationException.class);

        //execute
        sourceToTargetMapper.mapSourceSubEntityToTargetSubEntity(mockedSource, mockedTarget);
    }

    @Test(expected = MappingException.class)
    public void TestMappingSourceSubEntityToTargetSubEntity_ThrowsMappingException() throws MapperCreationException, MappingException {
        //initialize
        Source mockedSource = Mockito.mock(Source.class);
        Target mockedTarget = Mockito.mock(Target.class);
        TargetSubEntity mockedTargetSubEntity = Mockito.mock(TargetSubEntity.class);


        //setup
        Mockito.when(mapperFactory.createMapper(SourceSubEntity.class, TargetSubEntity.class)).thenReturn(sourceSubEntityToTargetSubEntityMapper);
        Mockito.when(sourceSubEntityToTargetSubEntityMapper.apply(mockedSource.getSourceSubEntity())).thenThrow(MappingException.class);

        //execute
        sourceToTargetMapper.mapSourceSubEntityToTargetSubEntity(mockedSource, mockedTarget);

        //verify
        Mockito.verify(mockedTarget, Mockito.times(1)).setTargetSubEntity(Mockito.eq(mockedTargetSubEntity));
    }

    @Test
    public void TestMappingSourceSubEntityFieldToTargetSubEntityFetchedField() {

        Source mockedSource = Mockito.mock(Source.class);
        Target mockedTarget = Mockito.mock(Target.class);

        SourceSubEntity mockedSourceSubEntity = Mockito.mock(SourceSubEntity.class);
        Mockito.when(mockedSource.getSourceSubEntity()).thenReturn(mockedSourceSubEntity);

        sourceToTargetMapper.mapSourceSubEntityFieldToSubEntityFetchedField(mockedSource, mockedTarget);
        Mockito.verify(mockedTarget, Mockito.times(1))
                .setSubEntityFetchedField(Mockito.eq(mockedSourceSubEntity.getSourceSubEntityField()));

    }


    @Test
    public void TestMappingPrimitiveSourceColToPrimitiveTargetCol() {

        Source mockedSource = Mockito.mock(Source.class);
        Target mockedTarget = Mockito.mock(Target.class);

        sourceToTargetMapper.mapPrimitiveSourceColToPrimitiveTargetCol(mockedSource, mockedTarget);
        Mockito.verify(mockedTarget, Mockito.times(1)).setPrimitiveTargetCol(mockedSource.getPrimitiveSourceCol());
    }

    @Test
    public void TestSettingMappedMailFromSourceToTargetEMail() {

        Source mockedSource = Mockito.mock(Source.class);
        Target mockedTarget = Mockito.mock(Target.class);

        sourceToTargetMapper.mapMailToEMail(mockedSource, mockedTarget);
        Mockito.verify(mockedTarget, Mockito.times(1)).setEMail(mockedSource.getMail());

    }

    @Test
    public void TestSettingConstantFieldConstantValue() {

        Target mockedTarget = Mockito.mock(Target.class);
        sourceToTargetMapper.setConstantFieldConstantValue(mockedTarget);
        Mockito.verify(mockedTarget, Mockito.times(1)).setConstantField(Mockito.eq("3"));
    }

    @Test
    public void TestMappingSubEntitiesSourceColToSubEntitiesTargetCol() throws MapperCreationException, MappingException {

        Source mockedSource = Mockito.mock(Source.class);
        Target mockedTarget = Mockito.mock(Target.class);
        SourceSubEntity sourceSubEntity = Mockito.mock(SourceSubEntity.class);


        Collection<SourceSubEntity> subEntitiesSourceCol = Arrays.asList(sourceSubEntity, sourceSubEntity);

        Mockito.when(mockedSource.getSubEntitiesSourceCol()).thenReturn(subEntitiesSourceCol);
        Mockito.when(mapperFactory.createMapper(SourceSubEntity.class, TargetSubEntity.class)).thenReturn(sourceSubEntityToTargetSubEntityMapper);

        sourceToTargetMapper.mapSubEntitiesSourceColToSubEntitiesTargetCol(mockedSource, mockedTarget);
        Mockito.verify(mockedTarget, Mockito.times(1)).setSubEntitiesTargetCol(Mockito.any());

    }

    @Test(expected = NullPointerException.class)
    public void TestMappingSubEntitiesSourceColToSubEntitiesTargetCol_throwsNullPointerException() throws MapperCreationException, MappingException {

        Source mockedSource = Mockito.mock(Source.class);
        Target mockedTarget = Mockito.mock(Target.class);
        SourceSubEntity sourceSubEntity = Mockito.mock(SourceSubEntity.class);


        Collection<SourceSubEntity> subEntitiesSourceCol = Arrays.asList(sourceSubEntity, sourceSubEntity);

        Mockito.when(mockedSource.getSubEntitiesSourceCol()).thenReturn(subEntitiesSourceCol);
        Mockito.when(mapperFactory.createMapper(SourceSubEntity.class, TargetSubEntity.class)).thenReturn(sourceSubEntityToTargetSubEntityMapper);
        Mockito.when(sourceSubEntityToTargetSubEntityMapper.apply(sourceSubEntity)).thenThrow(NullPointerException.class);

        sourceToTargetMapper.mapSubEntitiesSourceColToSubEntitiesTargetCol(mockedSource, mockedTarget);
        Mockito.verify(mockedTarget, Mockito.times(1)).setSubEntitiesTargetCol(Mockito.any());
    }

    @Test(expected = MapperCreationException.class)
    public void TestMappingSubEntitiesSourceColToSubEntitiesTargetCol_throwsMapperCreationException() throws MapperCreationException, MappingException {

        Source mockedSource = Mockito.mock(Source.class);
        Target mockedTarget = Mockito.mock(Target.class);

        Mockito.when(mapperFactory.createMapper(SourceSubEntity.class, TargetSubEntity.class)).thenThrow(MapperCreationException.class);

        sourceToTargetMapper.mapSubEntitiesSourceColToSubEntitiesTargetCol(mockedSource, mockedTarget);
    }

    @Test(expected = MappingException.class)
    public void TestMappingSubEntitiesSourceColToSubEntitiesTargetCol_throwsMappingException() throws MapperCreationException, MappingException {

        Source mockedSource = Mockito.mock(Source.class);
        Target mockedTarget = Mockito.mock(Target.class);
        SourceSubEntity sourceSubEntity = Mockito.mock(SourceSubEntity.class);


        Collection<SourceSubEntity> subEntitiesSourceCol = Arrays.asList(sourceSubEntity, sourceSubEntity);

        Mockito.when(mockedSource.getSubEntitiesSourceCol()).thenReturn(subEntitiesSourceCol);
        Mockito.when(mapperFactory.createMapper(SourceSubEntity.class, TargetSubEntity.class)).thenReturn(sourceSubEntityToTargetSubEntityMapper);
        Mockito.when(sourceSubEntityToTargetSubEntityMapper.apply(sourceSubEntity)).thenThrow(MappingException.class);

        sourceToTargetMapper.mapSubEntitiesSourceColToSubEntitiesTargetCol(mockedSource, mockedTarget);
    }

    @Test
    public void TestMappingSubEntitiesSourceColToSubEntitiesTargetCol2() throws MapperCreationException, MappingException {

        Source mockedSource = Mockito.mock(Source.class);
        Target mockedTarget = Mockito.mock(Target.class);
        SourceSubEntity sourceSubEntity = Mockito.mock(SourceSubEntity.class);

        Collection<SourceSubEntity> subEntitiesSourceCol = Arrays.asList(sourceSubEntity, sourceSubEntity);

        Mockito.when(mockedSource.getSubEntitiesSourceCol()).thenReturn(subEntitiesSourceCol);
        Mockito.when(mapperFactory.createMapper(SourceSubEntity.class, TargetSubEntity.class)).thenReturn(sourceSubEntityToTargetSubEntityMapper);

        sourceToTargetMapper.mapSubEntitiesSourceColToSubEntitiesTargetCol(mockedSource, mockedTarget);
        Mockito.verify(sourceSubEntityToTargetSubEntityMapper, Mockito.times(2)).apply(Mockito.eq(sourceSubEntity));
    }

    @Test
    public void TestProducingProducedFieldValue() throws ValueProducerCreationException {

        Target mockedTarget = Mockito.mock(Target.class);
        String mappedValue = "SomeString";

        Supplier<Object> supplier = () -> mappedValue;

        Mockito.when(valueProducerFactory.createValueProducer("IdProducer", Collections.emptyList())).thenReturn(supplier);

        sourceToTargetMapper.produceProducedFieldValue(mockedTarget);
        Mockito.verify(mockedTarget, Mockito.times(1)).setProducedField(Mockito.any());

    }

    @Test(expected = ValueProducerCreationException.class)
    public void TestProducingProducedFieldValue_throwsValueProducerCreationException() throws ValueProducerCreationException {

        Target mockedTarget = Mockito.mock(Target.class);

        Mockito.when(valueProducerFactory.createValueProducer("IdProducer", Collections.emptyList()))
                .thenThrow(ValueProducerCreationException.class);

        sourceToTargetMapper.produceProducedFieldValue(mockedTarget);
    }

    @Test(expected = NullPointerException.class)
    public void TestProducingProducedFieldValue_throwsNullPointerException() throws ValueProducerCreationException {

        Target mockedTarget = Mockito.mock(Target.class);

        Mockito.when(valueProducerFactory.createValueProducer("IdProducer", Collections.emptyList()))
                .thenReturn(null);

        sourceToTargetMapper.produceProducedFieldValue(mockedTarget);
        Mockito.verify(mockedTarget, Mockito.times(1)).setProducedField(Mockito.any());
    }
}