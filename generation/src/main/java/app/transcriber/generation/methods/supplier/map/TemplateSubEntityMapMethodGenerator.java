package app.transcriber.generation.methods.supplier.map;

import app.transcriber.api.exceptions.MapperCreationException;
import app.transcriber.api.exceptions.MappingException;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.CodeBlock;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public abstract class TemplateSubEntityMapMethodGenerator extends TemplateFieldsMapMethodsGenerator {

    protected TemplateSubEntityMapMethodGenerator(MapperGenerationContext mapperGenerationContext,
                                               TypesMapperGenerationInput typesMapperGenerationInput,
                                               FieldsMapperGenerationInput fieldsMapperGenerationInput,
                                               Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter) {
        super(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, argsToCodeBlockConverter);
    }

    @Override
    protected Optional<CodeBlock> createJavadoc() {
        CodeBlock.Builder javadocBuilder = CodeBlock.builder();
        super.createJavadoc().ifPresent(codeBlock -> javadocBuilder.add(codeBlock).add(CodeBlock.of("\n")));

        List<CodeBlock> javadocBlocks = new LinkedList<>();
        javadocBlocks.add(CodeBlock.of("@throws $T if an exception occurred during the creation of a required mapper", MapperCreationException.class));
        javadocBlocks.add(CodeBlock.of("@throws $T if an exception occurred during the mapping of the sub entity", MappingException.class));
        CodeBlock throwsJavadocBlock = CodeBlock.join(javadocBlocks, "\n");

        javadocBuilder.add(throwsJavadocBlock);

        return Optional.of(javadocBuilder.build());
    }

    @Override
    protected List<Type> thrownExceptions() {
        List<Type> result = new LinkedList<>(super.thrownExceptions());

        result.add(MapperCreationException.class);
        result.add(MappingException.class);

        return result;
    }
}
