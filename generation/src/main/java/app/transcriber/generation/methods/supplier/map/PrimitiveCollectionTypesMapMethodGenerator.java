package app.transcriber.generation.methods.supplier.map;

import app.transcriber.generation.conventions.GetterMethodNameConvention;
import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.methods.GettersChainGenerator;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import app.transcriber.tools.ReflectionApiUtils;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.UnaryOperator;


public class PrimitiveCollectionTypesMapMethodGenerator extends TemplateFieldsMapMethodsGenerator {

    private UnaryOperator<ClassAdapter> targetCollectionTypeResolver;

    public PrimitiveCollectionTypesMapMethodGenerator(Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter,
                                                      MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput,
                                                      FieldsMapperGenerationInput fieldsMapperGenerationInput, UnaryOperator<ClassAdapter> targetCollectionTypeResolver) {
        super(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, argsToCodeBlockConverter);
        this.targetCollectionTypeResolver = targetCollectionTypeResolver;
    }

    @Override
    public CodeBlock getMapStatement(MapperGenerationContext mapperGenerationContext,
                                     TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput) {

        ParameterizedTypeName targetCollectionType = ParameterizedTypeName.get(Collection.class,
                ReflectionApiUtils.getGenericTypeArgument(fieldsMapperGenerationInput.getTargetField()));

        CodeBlock gettersChain = new GettersChainGenerator().createGettersChain(NamingConvention.SOURCE_OBJECT.toString(),
                fieldsMapperGenerationInput.getSourceFields(),
                mapperGenerationContext, CodeBlock.of("$T.emptyList()", Collections.class));

        ClassAdapter targetFieldType = fieldsMapperGenerationInput.getTargetField().getType();
        TypeName targetInstantiatedCollectionType = TypeName.get(targetCollectionTypeResolver.apply(targetFieldType).getWrappedType());

        String targetFieldGetterName = mapperGenerationContext.getNamingConventionsContext().getConvention(GetterMethodNameConvention.class).apply(fieldsMapperGenerationInput.getTargetField());

        return CodeBlock.builder()
                .addStatement("$T $L = $T.ofNullable($L.$L()).orElse(new $T<>())", targetCollectionType, NamingConvention.MAPPED_VALUE,
                        Optional.class, NamingConvention.TARGET_OBJECT, targetFieldGetterName, targetInstantiatedCollectionType)
                .addStatement("$L.addAll($L)", NamingConvention.MAPPED_VALUE, gettersChain)
                .build();
    }
}