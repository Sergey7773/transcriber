package app.transcriber.generation.methods.apply;

import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;

public class ApplyMethodGeneratorFactory {

    public ApplyMethodGenerator create(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        return new ApplyMethodGenerator(mapperGenerationContext, typesMapperGenerationInput);
    }
}
