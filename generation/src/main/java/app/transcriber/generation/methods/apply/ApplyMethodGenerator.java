package app.transcriber.generation.methods.apply;

import app.transcriber.api.exceptions.MappingException;
import app.transcriber.generation.conventions.MapMethodNameConvention;
import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.conventions.ProduceMethodNameConvention;
import app.transcriber.generation.conventions.SetConstantValueMethodNameConvention;
import app.transcriber.generation.methods.AbstractMethodsProducer;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import lombok.AllArgsConstructor;

import javax.lang.model.element.Modifier;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
public class ApplyMethodGenerator extends AbstractMethodsProducer {

    private MapperGenerationContext mapperGenerationContext;

    private TypesMapperGenerationInput typesMapperGenerationInput;

    @Override
    protected MethodSpec.Builder createMethodDefinition() {
        return MethodSpec.methodBuilder(NamingConvention.OPERATOR_APPLY_METHOD.toString())
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(Override.class)
                .returns(typesMapperGenerationInput.getTargetClass().getWrappedType());
    }

    @Override
    protected List<ParameterSpec> createMethodParameters() {
        List<ParameterSpec> result = new LinkedList<>(super.createMethodParameters());

        result.add(ParameterSpec.builder(typesMapperGenerationInput.getSourceClass().getWrappedType(), NamingConvention.SOURCE_OBJECT.toString()).build());

        return result;
    }

    @Override
    protected List<Type> thrownExceptions() {
        List<Type> result = new LinkedList<>(super.thrownExceptions());

        result.add(MappingException.class);

        return result;
    }

    @Override
    protected CodeBlock createMethodBody() {
        return CodeBlock.builder().beginControlFlow("try")
                .addStatement("$T $L = $L.$L($T.class)", typesMapperGenerationInput.getTargetClass().getWrappedType(), NamingConvention.TARGET_OBJECT,
                        NamingConvention.TARGET_MODEL_OBJECT_FACTORY_MEMBER, NamingConvention.TARGET_MODEL_OBJECT_FACTORY_CREATE_FUNCTION,
                        typesMapperGenerationInput.getTargetClass().getWrappedType())
                .add(CodeBlock.join(generateMapMethodsInvocations(), ""))
                .add(CodeBlock.join(generateProduceMethodsInvocations(), ""))
                .add(CodeBlock.join(generateSetMethodsInvocations(), ""))
                .addStatement("return $L", NamingConvention.TARGET_OBJECT)
                .nextControlFlow("catch ($T e)", Exception.class)
                .addStatement(CodeBlock.builder().add("throw new $T($L, e)", MappingException.class, NamingConvention.SOURCE_OBJECT).build())
                .endControlFlow()
                .build();
    }

    private List<CodeBlock> generateMapMethodsInvocations() {
        MapMethodNameConvention mapMethodNameConvention = mapperGenerationContext.getNamingConventionsContext().getConvention(MapMethodNameConvention.class);

        return typesMapperGenerationInput.getFieldsMappersGenerationInputs()
                .stream().map(mapMethodNameConvention::apply)
                .map(methodName -> CodeBlock.builder().addStatement("$L($L, $L)",
                        methodName, NamingConvention.SOURCE_OBJECT.toString(),
                        NamingConvention.TARGET_OBJECT.toString()).build())
                .collect(Collectors.toList());
    }

    private List<CodeBlock> generateProduceMethodsInvocations() {
        ProduceMethodNameConvention produceMethodNameConvention = mapperGenerationContext.getNamingConventionsContext().getConvention(ProduceMethodNameConvention.class);

        return typesMapperGenerationInput.getFieldsValueProducersGenerationInputs()
                .stream().map(produceMethodNameConvention::apply)
                .map(methodName -> CodeBlock.builder().addStatement("$L($L)",
                        methodName, NamingConvention.TARGET_OBJECT).build())
                .collect(Collectors.toList());
    }

    private List<CodeBlock> generateSetMethodsInvocations() {
        SetConstantValueMethodNameConvention setConstantValueMethodNameConvention = mapperGenerationContext.getNamingConventionsContext().getConvention(SetConstantValueMethodNameConvention.class);

        return typesMapperGenerationInput.getConstantMappingsGenerationInputs()
                .stream().map(setConstantValueMethodNameConvention::apply)
                .map(methodName -> CodeBlock.builder().addStatement("$L($L)",
                        methodName, NamingConvention.TARGET_OBJECT).build())
                .collect(Collectors.toList());
    }

    @Override
    protected Optional<CodeBlock> createJavadoc() {
        CodeBlock.Builder javadocBuilder = CodeBlock.builder();
        super.createJavadoc().ifPresent(javadocBuilder::add);

        List<CodeBlock> javadocBlocks = new LinkedList<>();
        javadocBlocks.add(CodeBlock.of("Applies a mapping logic to an object of type {@code $T}, and returns an object of type {@code $T} which is the result of the mapping logic.",
                typesMapperGenerationInput.getSourceClass().getWrappedType(), typesMapperGenerationInput.getTargetClass().getWrappedType()));
        javadocBlocks.add(CodeBlock.of("@param source the given instance of type {@code $T}", typesMapperGenerationInput.getSourceClass().getWrappedType()));
        javadocBlocks.add(CodeBlock.of("@returns A new instance of type {@code $T}", typesMapperGenerationInput.getTargetClass().getWrappedType()));
        javadocBlocks.add(CodeBlock.of("@throws $T - if any exception occurred during the mapping process", MappingException.class));

        javadocBuilder.add(CodeBlock.join(javadocBlocks, "\n"));

        return Optional.of(javadocBuilder.build());
    }
}
