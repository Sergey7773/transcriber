package app.transcriber.generation.methods.supplier.map.factories;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.supplier.ValueSupplierMethodProducerFactory;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.Map;
import java.util.Optional;
import java.util.function.BiPredicate;

@AllArgsConstructor
public class FieldsMapMethodProducerFactory implements ValueSupplierMethodProducerFactory<FieldsMapperGenerationInput> {

    @NonNull
    private Map<BiPredicate<MapperGenerationContext, FieldsMapperGenerationInput>, ValueSupplierMethodProducerFactory<FieldsMapperGenerationInput>> factoriesMap;

    @Override
    public Optional<MethodProducer> create(MapperGenerationContext mapperGenerationContext,
                                           TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        return factoriesMap.keySet().
                stream()
                .filter(methodMapperPredicate -> methodMapperPredicate.test(mapperGenerationContext, fieldsMapperGenerationInput))
                .findFirst()
                .map(x -> factoriesMap.get(x).create(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput))
                .orElse(Optional.empty());
    }
}
