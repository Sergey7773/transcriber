package app.transcriber.generation.methods.supplier.map;

import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.methods.GettersChainGenerator;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.CodeBlock;

import java.util.List;
import java.util.function.Function;

public class PrimitiveTypesMapMethodGenerator extends TemplateFieldsMapMethodsGenerator {

    public PrimitiveTypesMapMethodGenerator(Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter,
                                            MapperGenerationContext mapperGenerationContext,
                                            TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        super(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, argsToCodeBlockConverter);
    }

    @Override
    public CodeBlock getMapStatement(MapperGenerationContext mapperGenerationContext,
                                     TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        CodeBlock gettersChain = new GettersChainGenerator().createGettersChain(NamingConvention.SOURCE_OBJECT.toString(),
                fieldsMapperGenerationInput.getSourceFields(),
                mapperGenerationContext);

        return CodeBlock.builder()
                .addStatement("$T $L = $L", fieldsMapperGenerationInput.getTargetField().getType().getWrappedType(),
                        NamingConvention.MAPPED_VALUE, gettersChain)
                .build();
    }
}