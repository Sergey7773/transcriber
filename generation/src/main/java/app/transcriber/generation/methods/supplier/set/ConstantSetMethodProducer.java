package app.transcriber.generation.methods.supplier.set;

import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.conventions.SetConstantValueMethodNameConvention;
import app.transcriber.generation.conventions.SetterMethodNameConvention;
import app.transcriber.generation.methods.AbstractMethodsProducer;
import app.transcriber.generation.methods.supplier.ConstantSetMethodArgumentFormatter;
import app.transcriber.generation.model.ConstantMappingGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import lombok.AllArgsConstructor;

import javax.lang.model.element.Modifier;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public class ConstantSetMethodProducer extends AbstractMethodsProducer {

    private MapperGenerationContext mapperGenerationContext;

    private TypesMapperGenerationInput typesMapperGenerationInput;

    private ConstantMappingGenerationInput constantMappingGenerationInput;

    private ConstantSetMethodArgumentFormatter constantSetMethodArgumentFormatter;

    @Override
    protected MethodSpec.Builder createMethodDefinition() {
        return MethodSpec.methodBuilder(mapperGenerationContext.getNamingConventionsContext().getConvention(SetConstantValueMethodNameConvention.class).apply(constantMappingGenerationInput))
                .addModifiers(Modifier.PROTECTED)
                .returns(void.class);
    }

    @Override
    protected CodeBlock createMethodBody() {
        String setterName = mapperGenerationContext.getNamingConventionsContext().getConvention(SetterMethodNameConvention.class).apply(constantMappingGenerationInput.getTargetField());
        String constantValue = constantSetMethodArgumentFormatter.format(constantMappingGenerationInput.getValue());

        return CodeBlock.builder()
                .addStatement("$L.$L($L)", NamingConvention.TARGET_OBJECT,
                        setterName, constantValue)
                .build();
    }

    @Override
    protected List<ParameterSpec> createMethodParameters() {
        List<ParameterSpec> result = new LinkedList<>(super.createMethodParameters());

        result.add(ParameterSpec.builder(typesMapperGenerationInput.getTargetClass().getWrappedType(), NamingConvention.TARGET_OBJECT.toString()).build());

        return result;
    }

    @Override
    protected Optional<CodeBlock> createJavadoc() {
        CodeBlock.Builder javadocBuilder = CodeBlock.builder();
        super.createJavadoc().ifPresent(codeBlock -> javadocBuilder.add(codeBlock).add(CodeBlock.of("\n")));

        List<CodeBlock> javadocBlocks = new LinkedList<>();
        javadocBlocks.add(CodeBlock.of("Sets a constant value in the $L field of <i>target</i>", constantMappingGenerationInput.getTargetField().getName()));
        javadocBlocks.add(CodeBlock.of("@param target the given instance of type {@code $T}", typesMapperGenerationInput.getTargetClass().getWrappedType()));
        CodeBlock throwsJavadocBlock = CodeBlock.join(javadocBlocks, "\n");

        javadocBuilder.add(throwsJavadocBlock);

        return Optional.of(javadocBuilder.build());
    }
}
