package app.transcriber.generation.methods;

import app.transcriber.generation.model.adapters.ClassAdapter;
import lombok.AllArgsConstructor;

import java.util.Map;
import java.util.function.UnaryOperator;

@AllArgsConstructor
public class CollectionTypeToImplementationTypeResolver implements UnaryOperator<ClassAdapter> {

    private final Map<ClassAdapter, ClassAdapter> collectionInterfaceToImplementationMap;

    private final ClassAdapter defaultValue;

    @Override
    public ClassAdapter apply(ClassAdapter aClass) {
        return collectionInterfaceToImplementationMap.getOrDefault(aClass, defaultValue);
    }
}
