package app.transcriber.generation.methods.supplier.map;

import app.transcriber.api.interfaces.Mapper;
import app.transcriber.generation.conventions.GetterMethodNameConvention;
import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.conventions.SubEntityMapperVariableNameConvention;
import app.transcriber.generation.methods.GettersChainGenerator;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import app.transcriber.generation.model.adapters.FieldAdapter;
import app.transcriber.tools.ListAccessUtils;
import app.transcriber.tools.ReflectionApiUtils;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class SubEntityCollectionMapMethodGenerator extends TemplateSubEntityMapMethodGenerator {

    private UnaryOperator<ClassAdapter> targetCollectionTypeResolver;

    public SubEntityCollectionMapMethodGenerator(Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter,
                                                 MapperGenerationContext mapperGenerationContext,
                                                 TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput,
                                                 UnaryOperator<ClassAdapter> targetCollectionTypeResolver) {
        super(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, argsToCodeBlockConverter);
        this.targetCollectionTypeResolver = targetCollectionTypeResolver;
    }

    @Override
    public CodeBlock getMapStatement(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        String mapperVariableName = mapperGenerationContext.getNamingConventionsContext().getConvention(SubEntityMapperVariableNameConvention.class).apply(fieldsMapperGenerationInput);

        FieldAdapter lastFieldSourceInList = ListAccessUtils.getOptionalLastElementOf(fieldsMapperGenerationInput.getSourceFields())
                .orElseThrow(() -> new IllegalArgumentException("Source fields list must not be null"));

        Type targetSubEntityType = ReflectionApiUtils.getGenericTypeArgument(fieldsMapperGenerationInput.getTargetField());
        Type sourceSubEntityType = ReflectionApiUtils.getGenericTypeArgument(lastFieldSourceInList);

        ParameterizedTypeName mapperType = ParameterizedTypeName.get(Mapper.class, sourceSubEntityType, targetSubEntityType);

        ClassAdapter targetFieldType = fieldsMapperGenerationInput.getTargetField().getType();
        ParameterizedTypeName targetDefinedCollectionType = ParameterizedTypeName.get((Class<?>)targetFieldType.getWrappedType(), targetSubEntityType);
        TypeName targetInstantiatedCollectionType = TypeName.get(targetCollectionTypeResolver.apply(targetFieldType).getWrappedType());

        CodeBlock gettersChain = new GettersChainGenerator().createGettersChain(NamingConvention.SOURCE_OBJECT.toString(),
                fieldsMapperGenerationInput.getSourceFields(),
                mapperGenerationContext, CodeBlock.of("$T.emptyList()", Collections.class));

        String targetFieldGetterName = mapperGenerationContext.getNamingConventionsContext().getConvention(GetterMethodNameConvention.class).apply(fieldsMapperGenerationInput.getTargetField());

        return CodeBlock.builder()
                .addStatement("$T $L = $L.$L($T.class, $T.class)", mapperType, mapperVariableName, NamingConvention.MAPPER_FACTORY_MEMBER,
                        NamingConvention.MAPPER_FACTORY_CREATE_FUNCTION, sourceSubEntityType, targetSubEntityType)
                .addStatement("$T $L = $T.ofNullable($L.$L()).orElse(new $T<>())", targetDefinedCollectionType, NamingConvention.MAPPED_VALUE, Optional.class,
                        NamingConvention.TARGET_OBJECT, targetFieldGetterName, targetInstantiatedCollectionType)
                .beginControlFlow("for ($T e : $L)", sourceSubEntityType, gettersChain)
                .addStatement("$L.add($L.$L(e))", NamingConvention.MAPPED_VALUE, mapperVariableName, NamingConvention.OPERATOR_APPLY_METHOD)
                .endControlFlow()
                .build();
    }

}
