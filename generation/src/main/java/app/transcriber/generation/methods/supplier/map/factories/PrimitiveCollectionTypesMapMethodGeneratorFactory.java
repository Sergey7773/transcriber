package app.transcriber.generation.methods.supplier.map.factories;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.supplier.ValueSupplierMethodProducerFactory;
import app.transcriber.generation.methods.supplier.map.PrimitiveCollectionTypesMapMethodGenerator;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import com.squareup.javapoet.CodeBlock;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.UnaryOperator;

@AllArgsConstructor
public class PrimitiveCollectionTypesMapMethodGeneratorFactory implements ValueSupplierMethodProducerFactory<FieldsMapperGenerationInput> {

    @NonNull
    private Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter;

    @NonNull
    private UnaryOperator<ClassAdapter> targetCollectionTypeResolver;

    @Override
    public Optional<MethodProducer> create(@NonNull MapperGenerationContext mapperGenerationContext,
                                           @NonNull TypesMapperGenerationInput typesMapperGenerationInput, @NonNull FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        return Optional.of(new PrimitiveCollectionTypesMapMethodGenerator(argsToCodeBlockConverter,
                mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, targetCollectionTypeResolver));
    }
}
