package app.transcriber.generation.methods.supplier.map;

import app.transcriber.api.interfaces.Mapper;
import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.conventions.SubEntityMapperVariableNameConvention;
import app.transcriber.generation.methods.GettersChainGenerator;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import app.transcriber.generation.model.adapters.FieldAdapter;
import app.transcriber.tools.ListAccessUtils;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.ParameterizedTypeName;

import java.util.List;
import java.util.function.Function;

public class SubEntityMapMethodGenerator extends TemplateSubEntityMapMethodGenerator {

    public SubEntityMapMethodGenerator(Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter,
                                       MapperGenerationContext mapperGenerationContext,
                                       TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        super(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput, argsToCodeBlockConverter);
    }

    @Override
    public CodeBlock getMapStatement(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput, FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        String mapperVariableName = mapperGenerationContext.getNamingConventionsContext().getConvention(SubEntityMapperVariableNameConvention.class).apply(fieldsMapperGenerationInput);

        FieldAdapter lastFieldSourceInList = ListAccessUtils.getOptionalLastElementOf(fieldsMapperGenerationInput.getSourceFields())
                .orElseThrow(() -> new IllegalArgumentException("Source fields list must not be null"));

        ClassAdapter sourceFieldType = lastFieldSourceInList.getType();
        ClassAdapter targetFieldType = fieldsMapperGenerationInput.getTargetField().getType();
        ParameterizedTypeName mapperType = ParameterizedTypeName.get(Mapper.class, sourceFieldType.getWrappedType(), targetFieldType.getWrappedType());

        CodeBlock gettersChain = new GettersChainGenerator().createGettersChain(NamingConvention.SOURCE_OBJECT.toString(),
                fieldsMapperGenerationInput.getSourceFields(),
                mapperGenerationContext);

        return CodeBlock.builder()
                .addStatement("$T $L = $L.$L($T.class, $T.class)", mapperType, mapperVariableName, NamingConvention.MAPPER_FACTORY_MEMBER,
                        NamingConvention.MAPPER_FACTORY_CREATE_FUNCTION, sourceFieldType.getWrappedType(), targetFieldType.getWrappedType())
                .addStatement("$T $L = $L.$L($L)", targetFieldType.getWrappedType(), NamingConvention.MAPPED_VALUE, mapperVariableName,
                        NamingConvention.OPERATOR_APPLY_METHOD, gettersChain)
                .build();
    }

}