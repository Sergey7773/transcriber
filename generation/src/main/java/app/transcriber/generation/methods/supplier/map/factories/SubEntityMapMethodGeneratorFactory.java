package app.transcriber.generation.methods.supplier.map.factories;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.supplier.ValueSupplierMethodProducerFactory;
import app.transcriber.generation.methods.supplier.map.SubEntityMapMethodGenerator;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.CodeBlock;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@AllArgsConstructor
public class SubEntityMapMethodGeneratorFactory implements ValueSupplierMethodProducerFactory<FieldsMapperGenerationInput> {

    @NonNull
    private Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter;

    @Override
    public Optional<MethodProducer> create(@NonNull MapperGenerationContext mapperGenerationContext,
                                           @NonNull TypesMapperGenerationInput typesMapperGenerationInput, @NonNull FieldsMapperGenerationInput fieldsMapperGenerationInput) {
        return Optional.of(new SubEntityMapMethodGenerator(argsToCodeBlockConverter,
                mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput));
    }
}
