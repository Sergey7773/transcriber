package app.transcriber.generation.methods.supplier.map;

import app.transcriber.api.exceptions.MappingOperatorCreationException;
import app.transcriber.generation.conventions.MapMethodNameConvention;
import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.conventions.SetterMethodNameConvention;
import app.transcriber.generation.methods.AbstractMethodsProducer;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.tools.ListAccessUtils;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import lombok.AllArgsConstructor;

import javax.lang.model.element.Modifier;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@AllArgsConstructor
public abstract class TemplateFieldsMapMethodsGenerator extends AbstractMethodsProducer {

    protected MapperGenerationContext mapperGenerationContext;
    protected TypesMapperGenerationInput typesMapperGenerationInput;
    protected FieldsMapperGenerationInput fieldsMapperGenerationInput;
    private final Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter;

    @Override
    protected MethodSpec.Builder createMethodDefinition() {
        String methodName = mapperGenerationContext.getNamingConventionsContext().getConvention(MapMethodNameConvention.class).apply(fieldsMapperGenerationInput);
        return MethodSpec.methodBuilder(methodName)
                .addModifiers(Modifier.PROTECTED)
                .returns(void.class);
    }

    @Override
    protected List<Type> thrownExceptions() {
        List<Type> result = new LinkedList<>(super.thrownExceptions());

        if (!fieldsMapperGenerationInput.getMappingOperatorsGenerationInputs().isEmpty()) {
            result.add(MappingOperatorCreationException.class);
        }

        return result;
    }

    @Override
    protected List<ParameterSpec> createMethodParameters() {
        List<ParameterSpec> result = new LinkedList<>(super.createMethodParameters());

        result.add(ParameterSpec.builder(typesMapperGenerationInput.getSourceClass().getWrappedType(), NamingConvention.SOURCE_OBJECT.toString()).build());
        result.add(ParameterSpec.builder(typesMapperGenerationInput.getTargetClass().getWrappedType(), NamingConvention.TARGET_OBJECT.toString()).build());

        return result;
    }

    @Override
    protected CodeBlock createMethodBody() {
        String setterMethodName = mapperGenerationContext.getNamingConventionsContext().getConvention(SetterMethodNameConvention.class).apply(fieldsMapperGenerationInput.getTargetField());

        CodeBlock.Builder resultBuilder = CodeBlock.builder()
                .add(getMapStatement(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput));

        fieldsMapperGenerationInput.getMappingOperatorsGenerationInputs().stream()
                .map(mappingOperatorsMetadata ->
                        CodeBlock.builder()
                                .addStatement("$L = ($T) $L.$L($S, $L).$L($L)", NamingConvention.MAPPED_VALUE,
                                        mappingOperatorsMetadata.getMapperTargetType().getWrappedType(), NamingConvention.MAPPING_OPERATOR_FACTORY_MEMBER,
                                        NamingConvention.MAPPING_OPERATOR_FACTORY_CREATE_FUNCTION, mappingOperatorsMetadata.getOperatorName(),
                                        argsToCodeBlockConverter.apply(mappingOperatorsMetadata.getCreationParametersGenerationInputs()), NamingConvention.OPERATOR_APPLY_METHOD,
                                        NamingConvention.MAPPED_VALUE).build())
                .forEach(resultBuilder::add);

        resultBuilder.addStatement("$L.$L($L)", NamingConvention.TARGET_OBJECT, setterMethodName, NamingConvention.MAPPED_VALUE);

        return resultBuilder.build();
    }

    @Override
    protected Optional<CodeBlock> createJavadoc() {
        CodeBlock.Builder javadocBuilder = CodeBlock.builder();
        super.createJavadoc().ifPresent(codeBlock -> javadocBuilder.add(codeBlock).add(CodeBlock.of("\n")));

        List<CodeBlock> javadocBlocks = new LinkedList<>();
        javadocBlocks.add(CodeBlock.of("Maps between the $L field of <i>source</i> and $L field of <i>target</i>",
                ListAccessUtils.getOptionalLastElementOf(fieldsMapperGenerationInput.getSourceFields()).orElseThrow(() -> new IllegalArgumentException("Source fields list must not be empty")).getName(),
                fieldsMapperGenerationInput.getTargetField().getName()));
        javadocBlocks.add(CodeBlock.of("@param source the given instance of type {@code $T}", typesMapperGenerationInput.getSourceClass().getWrappedType()));
        javadocBlocks.add(CodeBlock.of("@param target the given instance of type {@code $T}", typesMapperGenerationInput.getTargetClass().getWrappedType()));

        javadocBuilder.add(CodeBlock.join(javadocBlocks, "\n"));

        return Optional.of(javadocBuilder.build());
    }

    public abstract CodeBlock getMapStatement(MapperGenerationContext mapperGenerationContext,
                                              TypesMapperGenerationInput typesMapperGenerationInput,
                                              FieldsMapperGenerationInput fieldsMapperGenerationInput);

}