package app.transcriber.generation.methods.supplier.produce;

import app.transcriber.api.exceptions.ValueProducerCreationException;
import app.transcriber.generation.conventions.NamingConvention;
import app.transcriber.generation.conventions.ProduceMethodNameConvention;
import app.transcriber.generation.conventions.SetterMethodNameConvention;
import app.transcriber.generation.methods.AbstractMethodsProducer;
import app.transcriber.generation.model.FieldsValueProducerGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import lombok.AllArgsConstructor;

import javax.lang.model.element.Modifier;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@AllArgsConstructor
public class ValueProducerMethodProducer extends AbstractMethodsProducer {

    private Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter;

    private MapperGenerationContext mapperGenerationContext;

    private TypesMapperGenerationInput typesMapperGenerationInput;

    private FieldsValueProducerGenerationInput fieldsValueProducerGenerationInput;

    @Override
    protected MethodSpec.Builder createMethodDefinition() {
        String methodName = mapperGenerationContext.getNamingConventionsContext().getConvention(ProduceMethodNameConvention.class).apply(fieldsValueProducerGenerationInput);

        return MethodSpec.methodBuilder(methodName)
                .addModifiers(Modifier.PROTECTED)
                .returns(void.class);

    }

    @Override
    protected CodeBlock createMethodBody() {
        String setterMethodName = mapperGenerationContext.getNamingConventionsContext().getConvention(SetterMethodNameConvention.class).apply(fieldsValueProducerGenerationInput.getTargetField());

        ClassAdapter targetFieldType = fieldsValueProducerGenerationInput.getTargetField().getType();
        String mappedValue = NamingConvention.MAPPED_VALUE.toString();
        CodeBlock producerCreationParameters = argsToCodeBlockConverter.apply(fieldsValueProducerGenerationInput.getCreationParametersGenerationInputs());

        return CodeBlock.builder()
                .addStatement("$T $L = ($T) $L.$L($S, $L).$L()", targetFieldType.getWrappedType(), mappedValue, targetFieldType.getWrappedType(),
                        NamingConvention.VALUE_PRODUCER_FACTORY_MEMBER, NamingConvention.VALUE_PRODUCER_FACTORY_CREATE_FUNCTION,
                        fieldsValueProducerGenerationInput.getValueProducerName(), producerCreationParameters,
                        NamingConvention.VALUE_PRODUCER_GET_FUNCTION)
                .addStatement("$L.$L($L)", NamingConvention.TARGET_OBJECT, setterMethodName, mappedValue)
                .build();
    }


    @Override
    protected List<ParameterSpec> createMethodParameters() {
        List<ParameterSpec> result = new LinkedList<>(super.createMethodParameters());

        result.add(ParameterSpec.builder(typesMapperGenerationInput.getTargetClass().getWrappedType(), NamingConvention.TARGET_OBJECT.toString()).build());

        return result;
    }

    @Override
    protected List<Type> thrownExceptions() {
        List<Type> result = new LinkedList<>(super.thrownExceptions());

        result.add(ValueProducerCreationException.class);

        return result;
    }

    @Override
    protected Optional<CodeBlock> createJavadoc() {
        CodeBlock.Builder javadocBuilder = CodeBlock.builder();
        super.createJavadoc().ifPresent(codeBlock -> javadocBuilder.add(codeBlock).add(CodeBlock.of("\n")));

        List<CodeBlock> javadocBlocks = new LinkedList<>();
        javadocBlocks.add(CodeBlock.of("Sets the $L field in <i>target</i>, according to the value produced by the $S value producer",
                fieldsValueProducerGenerationInput.getTargetField().getName(), fieldsValueProducerGenerationInput.getValueProducerName()));
        javadocBlocks.add(CodeBlock.of("@param target the given instance of type {@code $T}", typesMapperGenerationInput.getTargetClass().getWrappedType()));
        javadocBlocks.add(CodeBlock.of("@throws $T if an exception occurred during the creation of a required valueProducer", ValueProducerCreationException.class));
        CodeBlock throwsJavadocBlock = CodeBlock.join(javadocBlocks, "\n");

        javadocBuilder.add(throwsJavadocBlock);

        return Optional.of(javadocBuilder.build());
    }
}
