package app.transcriber.generation.methods.supplier.produce;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.supplier.ValueSupplierMethodProducerFactory;
import app.transcriber.generation.model.FieldsValueProducerGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.CodeBlock;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@AllArgsConstructor
public class ValueProducerMethodProducerFactory implements ValueSupplierMethodProducerFactory<FieldsValueProducerGenerationInput> {

    private Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter;

    @Override
    public Optional<MethodProducer> create(MapperGenerationContext mapperGenerationContext,
                                           TypesMapperGenerationInput typesMapperGenerationInput, FieldsValueProducerGenerationInput fieldsValueProducerGenerationInput) {
        return Optional.of(new ValueProducerMethodProducer(argsToCodeBlockConverter,
                mapperGenerationContext, typesMapperGenerationInput, fieldsValueProducerGenerationInput));
    }
}
