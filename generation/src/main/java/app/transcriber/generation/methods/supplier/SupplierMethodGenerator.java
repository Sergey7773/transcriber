package app.transcriber.generation.methods.supplier;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.supplier.map.factories.FieldsMapMethodProducerFactory;
import app.transcriber.generation.methods.supplier.produce.ValueProducerMethodProducerFactory;
import app.transcriber.generation.methods.supplier.set.ConstantSetMethodProducerFactory;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.MethodSpec;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * <p>
 * Responsible for creating {@link MethodSpec} objects which represent methods of the following types
 *     <ul>
 *         <li>Methods which supply the value of a target field according to a logic defined by some value producer</li>
 *         <li>Methods which supply the value of a target field by setting a constant value</li>
 *         <li>Methods which supply the value of a target field by performing a mapping from a source field </li>
 *     </ul>
 * </p>
 */
@AllArgsConstructor
public class SupplierMethodGenerator {

    @NonNull
    private ValueProducerMethodProducerFactory valueProducerMethodProducerFactory;

    @NonNull
    private ConstantSetMethodProducerFactory constantSetMethodProducerFactory;

    @NonNull
    private FieldsMapMethodProducerFactory fieldsMapMethodProducerFactory;

    /**
     * Creates {@link MethodSpec} objects according to the given <i>mapperGenerationContext</i> and <i>typesMapperGenerationInput</i>.
     *
     * @param mapperGenerationContext
     * @param typesMapperGenerationInput
     * @return A list of {@link MethodSpec} objects, containing all the methods required by a {@link app.transcriber.api.interfaces.Mapper} class except <i>apply</i> and the all args constructor.
     * @throws IllegalArgumentException if <i>mapperGenerationContext</i> or <i>typesMapperGenerationInput</i> are null
     */
    public List<MethodSpec> generate(@NonNull MapperGenerationContext mapperGenerationContext, @NonNull TypesMapperGenerationInput typesMapperGenerationInput) {
        List<MethodSpec> result = new LinkedList<>();
        result.addAll(generateMapMethods(mapperGenerationContext, typesMapperGenerationInput));
        result.addAll(generateProduceMethods(mapperGenerationContext, typesMapperGenerationInput));
        result.addAll(generateSetMethods(mapperGenerationContext, typesMapperGenerationInput));
        return result;

    }

    private List<MethodSpec> generateMapMethods(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        return generateMethods(typesMapperGenerationInput.getFieldsMappersGenerationInputs(),
                mapperGenerationContext, typesMapperGenerationInput, fieldsMapMethodProducerFactory);
    }

    private List<MethodSpec> generateProduceMethods(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        return generateMethods(typesMapperGenerationInput.getFieldsValueProducersGenerationInputs(),
                mapperGenerationContext, typesMapperGenerationInput, valueProducerMethodProducerFactory);
    }

    private List<MethodSpec> generateSetMethods(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        return generateMethods(typesMapperGenerationInput.getConstantMappingsGenerationInputs(),
                mapperGenerationContext, typesMapperGenerationInput, constantSetMethodProducerFactory);
    }

    private <T> List<MethodSpec> generateMethods(Collection<T> elements, MapperGenerationContext mapperGenerationContext,
                                                 TypesMapperGenerationInput typesMapperGenerationInput, ValueSupplierMethodProducerFactory<T> methodProducerFactory) {
        return elements
                .stream()
                .map(t -> methodProducerFactory.create(mapperGenerationContext, typesMapperGenerationInput, t))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(MethodProducer::get)
                .collect(Collectors.toList());
    }

}
