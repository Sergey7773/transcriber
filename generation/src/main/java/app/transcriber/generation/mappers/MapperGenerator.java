package app.transcriber.generation.mappers;

import app.transcriber.api.annotations.MapperMetadata;
import app.transcriber.api.interfaces.Mapper;
import app.transcriber.generation.TranscriberGenerator;
import app.transcriber.generation.conventions.MapperClassNameConvention;
import app.transcriber.generation.fields.MapperFieldsGenerator;
import app.transcriber.generation.methods.apply.ApplyMethodGeneratorFactory;
import app.transcriber.generation.methods.supplier.SupplierMethodGenerator;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.*;
import lombok.AllArgsConstructor;

import javax.lang.model.element.Modifier;
import java.util.*;
import java.util.stream.Collectors;

@AllArgsConstructor
public class MapperGenerator implements TranscriberGenerator {

    private final ApplyMethodGeneratorFactory applyMethodGeneratorFactory;
    private final SupplierMethodGenerator supplierMethodGenerator;
    private final MapperFieldsGenerator mapperFieldsGenerator;




    @Override
    public List<JavaFile> generate(MapperGenerationContext generationContext) {
        return Optional.ofNullable(generationContext.getTypesMapperGenerationInputs())
                .orElse(Collections.emptyList())
                .stream()
                .map(typesMapperGenerationInput -> this.generateMapper(generationContext, typesMapperGenerationInput))
                .collect(Collectors.toList());
    }

    public JavaFile generateMapper(MapperGenerationContext mapperGenerationContext, TypesMapperGenerationInput typesMapperGenerationInput) {
        String mapperClassName = mapperGenerationContext.getNamingConventionsContext().getConvention(MapperClassNameConvention.class).apply(typesMapperGenerationInput);
        TypeSpec.Builder mapperSpecBuilder = TypeSpec.classBuilder(mapperClassName)
                .addModifiers(Modifier.PUBLIC)
                .addSuperinterface(ParameterizedTypeName.get(Mapper.class,
                        typesMapperGenerationInput.getSourceClass().getWrappedType(),
                        typesMapperGenerationInput.getTargetClass().getWrappedType()));

        List<FieldSpec> mapperFields = mapperFieldsGenerator.buildFieldsSpecForClassFields(mapperGenerationContext, typesMapperGenerationInput, Collections.emptyList());
        mapperSpecBuilder.addFields(mapperFields);

        generateConstructorSpec(mapperFields).ifPresent(mapperSpecBuilder::addMethod);

        mapperSpecBuilder.addMethod(applyMethodGeneratorFactory.create(mapperGenerationContext, typesMapperGenerationInput).get());
        mapperSpecBuilder.addMethods(supplierMethodGenerator.generate(mapperGenerationContext, typesMapperGenerationInput));

        addJavadoc(mapperSpecBuilder, typesMapperGenerationInput);

        AnnotationSpec mapperMetadataAnnotation =
                AnnotationSpec.builder(MapperMetadata.class)
                        .addMember("sourceClass", "$T.class", typesMapperGenerationInput.getSourceClass().getWrappedType())
                        .addMember("targetClass", "$T.class", typesMapperGenerationInput.getTargetClass().getWrappedType())
                        .build();

        mapperSpecBuilder.addAnnotation(mapperMetadataAnnotation);

        return JavaFile.builder(mapperGenerationContext.getOutputPackage(), mapperSpecBuilder.build())
                .skipJavaLangImports(true)
                .build();
    }

    private void addJavadoc(TypeSpec.Builder mapperSpecBuilder, TypesMapperGenerationInput typesMapperGenerationInput) {
        mapperSpecBuilder.addJavadoc("The generated logic which is responsible for converting {@code $T} instances to {@code $T} instances.",
                typesMapperGenerationInput.getSourceClass().getWrappedType(), typesMapperGenerationInput.getTargetClass().getWrappedType());
    }

    private Optional<MethodSpec> generateConstructorSpec(List<FieldSpec> requiredFields) {
        if (requiredFields.isEmpty()) {
            return Optional.empty();
        }

        MethodSpec.Builder resultBuilder = MethodSpec.constructorBuilder()
                .addModifiers(Modifier.PUBLIC);

        Map<FieldSpec, ParameterSpec> parameterSpecMap = new LinkedHashMap<>();
        requiredFields.forEach(e -> parameterSpecMap.put(e, convertFieldSpecToParameterSpec(e)));

        resultBuilder.addParameters(parameterSpecMap.values());

        parameterSpecMap.entrySet()
                .stream()
                .map(e -> createAssignmentBlock(e.getKey(), e.getValue()))
                .forEach(resultBuilder::addCode);

        return Optional.of(resultBuilder.build());
    }

    private ParameterSpec convertFieldSpecToParameterSpec(FieldSpec fieldSpec) {
        return ParameterSpec.builder(fieldSpec.type, fieldSpec.name).build();
    }

    private CodeBlock createAssignmentBlock(FieldSpec fieldSpec, ParameterSpec parameterSpec) {
        return CodeBlock.builder()
                .beginControlFlow("if($N == null)", parameterSpec.name)
                .addStatement("throw new $T($L)", IllegalArgumentException.class, "\"" + parameterSpec.name + " must be a non-null value\"")
                .endControlFlow()
                .addStatement("this.$N = $N", fieldSpec, parameterSpec).build();
    }

}
