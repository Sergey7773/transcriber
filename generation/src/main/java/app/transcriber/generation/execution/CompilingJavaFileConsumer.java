package app.transcriber.generation.execution;

import com.squareup.javapoet.JavaFile;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import javax.tools.JavaCompiler;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@AllArgsConstructor
@Slf4j
public class CompilingJavaFileConsumer implements JavaFileConsumer {

    @NonNull
    private OutputFilePathCalculator outputFilePathCalculator;

    @NonNull
    private String mapperClassesDirectory;

    @NonNull
    private JavaCompiler javaCompiler;

    @Override
    public void accept(JavaFile javaFile) {
        try {
            String outputFilepath = outputFilePathCalculator.apply(javaFile.typeSpec.name);
            File destinationFile = new File(outputFilepath);
            try (FileWriter writer = new FileWriter(destinationFile)) {
                writer.write(javaFile.toString());
            }

            javaCompiler.run(null, null, null, "-d", mapperClassesDirectory, outputFilepath);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }
}
