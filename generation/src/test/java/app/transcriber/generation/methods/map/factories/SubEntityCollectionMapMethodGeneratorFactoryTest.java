package app.transcriber.generation.methods.map.factories;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.supplier.map.SubEntityCollectionMapMethodGenerator;
import app.transcriber.generation.methods.supplier.map.factories.SubEntityCollectionMapMethodGeneratorFactory;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import com.squareup.javapoet.CodeBlock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.UnaryOperator;

@RunWith(MockitoJUnitRunner.class)
public class SubEntityCollectionMapMethodGeneratorFactoryTest {

    private SubEntityCollectionMapMethodGeneratorFactory subEntityCollectionMapMethodGeneratorFactory;

    @Mock
    private Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter;

    @Mock
    private MapperGenerationContext mapperGenerationContext;

    @Mock
    private TypesMapperGenerationInput typesMapperGenerationInput;

    @Mock
    private FieldsMapperGenerationInput fieldsMapperGenerationInput;

    @Mock
    private UnaryOperator<ClassAdapter> targetCollectionTypeResolver;

    @Before
    public void setUp() {
        subEntityCollectionMapMethodGeneratorFactory = new SubEntityCollectionMapMethodGeneratorFactory(argsToCodeBlockConverter, targetCollectionTypeResolver);
    }

    @Test(expected = IllegalArgumentException.class)
    public void allArgsConstructor_onNullArgsAsListPrinterFunction_throwsIllegalArgumentException() {
        new SubEntityCollectionMapMethodGeneratorFactory(null, targetCollectionTypeResolver);
    }

    @Test(expected = IllegalArgumentException.class)
    public void allArgsConstructor_onNullTargetCollectionTypeResolver_throwsIllegalArgumentException() {
        new SubEntityCollectionMapMethodGeneratorFactory(argsToCodeBlockConverter, null);
    }


    @Test(expected = IllegalArgumentException.class)
    public void create_onNullMapperGenerationContext_throwsIllegalArgumentException() {
        subEntityCollectionMapMethodGeneratorFactory.create(null,
                typesMapperGenerationInput, fieldsMapperGenerationInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_onNullTypesMapperMetadata_throwsIllegalArgumentException() {
        subEntityCollectionMapMethodGeneratorFactory.create(mapperGenerationContext,
                null, fieldsMapperGenerationInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_onNullFieldsMapperMetadata_throwsIllegalArgumentException() {
        subEntityCollectionMapMethodGeneratorFactory.create(mapperGenerationContext,
                typesMapperGenerationInput, null);
    }

    @Test
    public void create_onNonNullParameters_returnOptionalWithSubEntityCollectionMapMethodGeneratorValue() {
        Optional<MethodProducer> result =
                subEntityCollectionMapMethodGeneratorFactory.create(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput);

        Assert.assertTrue(result.isPresent());

        result.ifPresent(methodProducer -> Assert.assertTrue(methodProducer instanceof SubEntityCollectionMapMethodGenerator));
    }

}