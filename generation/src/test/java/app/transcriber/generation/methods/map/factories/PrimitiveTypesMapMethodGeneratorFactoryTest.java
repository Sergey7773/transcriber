package app.transcriber.generation.methods.map.factories;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.supplier.map.PrimitiveTypesMapMethodGenerator;
import app.transcriber.generation.methods.supplier.map.factories.PrimitiveTypesMapMethodGeneratorFactory;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypedParameterGenerationInput;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import com.squareup.javapoet.CodeBlock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@RunWith(MockitoJUnitRunner.class)
public class PrimitiveTypesMapMethodGeneratorFactoryTest {

    private PrimitiveTypesMapMethodGeneratorFactory primitiveTypesMapMethodGeneratorFactory;

    @Mock
    private Function<List<TypedParameterGenerationInput>, CodeBlock> argsToCodeBlockConverter;

    @Mock
    private MapperGenerationContext mapperGenerationContext;

    @Mock
    private TypesMapperGenerationInput typesMapperGenerationInput;

    @Mock
    private FieldsMapperGenerationInput fieldsMapperGenerationInput;

    @Before
    public void setUp() {
        primitiveTypesMapMethodGeneratorFactory = new PrimitiveTypesMapMethodGeneratorFactory(argsToCodeBlockConverter);
    }

    @Test(expected = IllegalArgumentException.class)
    public void allArgsConstructor_onNullArgsAsListPrinterFunction_throwsIllegalArgumentException() {
        new PrimitiveTypesMapMethodGeneratorFactory(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_onNullMapperGenerationContext_throwsIllegalArgumentException() {
        primitiveTypesMapMethodGeneratorFactory.create(null,
                typesMapperGenerationInput, fieldsMapperGenerationInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_onNullTypesMapperMetadata_throwsIllegalArgumentException() {
        primitiveTypesMapMethodGeneratorFactory.create(mapperGenerationContext,
                null, fieldsMapperGenerationInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_onNullFieldsMapperMetadata_throwsIllegalArgumentException() {
        primitiveTypesMapMethodGeneratorFactory.create(mapperGenerationContext,
                typesMapperGenerationInput, null);
    }

    @Test
    public void create_onNonNullParameters_returnOptionalWithPrimitiveTypesMapMethodGeneratorValue() {
        Optional<MethodProducer> result =
                primitiveTypesMapMethodGeneratorFactory.create(mapperGenerationContext, typesMapperGenerationInput, fieldsMapperGenerationInput);

        Assert.assertTrue(result.isPresent());

        result.ifPresent(methodProducer -> Assert.assertTrue(methodProducer instanceof PrimitiveTypesMapMethodGenerator));
    }

}