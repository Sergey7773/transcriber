package app.transcriber.generation.methods.map.predicates;

import app.transcriber.generation.methods.supplier.map.predicates.IsCollectionTypePredicate;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

@RunWith(MockitoJUnitRunner.class)
public class IsCollectionTypePredicateTest {

    private IsCollectionTypePredicate isCollectionTypePredicate;

    @Mock
    private MapperGenerationContext mapperGenerationContext;

    @Mock
    private FieldsMapperGenerationInput fieldsMapperGenerationInput;

    @Before
    public void setup() {
        isCollectionTypePredicate = new IsCollectionTypePredicate();

        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(Collections.emptyList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_onNullMapperGenerationContext_throwsIllegalArgumentException() {
        isCollectionTypePredicate.test(null, fieldsMapperGenerationInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_onNullFieldsMapperMetadata_throwsIllegalArgumentException() {
        isCollectionTypePredicate.test(mapperGenerationContext, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_onNullSourceFieldsList_throwsIllegalArgumentException() {
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(null);

        isCollectionTypePredicate.test(mapperGenerationContext, fieldsMapperGenerationInput);
    }
}