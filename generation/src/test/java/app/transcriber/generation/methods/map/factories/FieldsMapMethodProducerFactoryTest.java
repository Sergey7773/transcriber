package app.transcriber.generation.methods.map.factories;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.supplier.ValueSupplierMethodProducerFactory;
import app.transcriber.generation.methods.supplier.map.factories.FieldsMapMethodProducerFactory;
import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.MapperGenerationContext;
import app.transcriber.generation.model.TypesMapperGenerationInput;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiPredicate;

public class FieldsMapMethodProducerFactoryTest {

    private Map<BiPredicate<MapperGenerationContext, FieldsMapperGenerationInput>, ValueSupplierMethodProducerFactory<FieldsMapperGenerationInput>> factoriesMap;

    @Before
    public void before() {
        factoriesMap = new LinkedHashMap<>();
    }

    @Test(expected = IllegalArgumentException.class)
    public void allArgsConstructor_onNullFactoriesMap_throwsIllegalArgumentException() {
        new FieldsMapMethodProducerFactory(null);
    }

    @Test
    public void create_allPredicatesReturnFalse_returnEmptyOptional() {
        BiPredicate<MapperGenerationContext, FieldsMapperGenerationInput> firstPredicate = Mockito.mock(BiPredicate.class);
        ValueSupplierMethodProducerFactory<FieldsMapperGenerationInput> firstNestedFactory = createMockFactory(Mockito.mock(MethodProducer.class));

        BiPredicate<MapperGenerationContext, FieldsMapperGenerationInput> secondPredicate = Mockito.mock(BiPredicate.class);
        ValueSupplierMethodProducerFactory<FieldsMapperGenerationInput> secondNestedFactory = createMockFactory(Mockito.mock(MethodProducer.class));

        factoriesMap.put(firstPredicate, firstNestedFactory);
        factoriesMap.put(secondPredicate, secondNestedFactory);

        FieldsMapMethodProducerFactory factory = new FieldsMapMethodProducerFactory(factoriesMap);

        Optional<MethodProducer> result = invokeCreateWithMockArguments(factory);

        Assert.assertFalse(result.isPresent());
    }

    @Test
    public void create_predicateReturnsTrue_delegateCallToNestedFactoryOfPRedicate() {
        BiPredicate<MapperGenerationContext, FieldsMapperGenerationInput> firstPredicate = Mockito.mock(BiPredicate.class);
        Mockito.when(firstPredicate.test(Mockito.any(), Mockito.any())).thenReturn(true);

        MethodProducer expectedProducer = Mockito.mock(MethodProducer.class);
        ValueSupplierMethodProducerFactory<FieldsMapperGenerationInput> firstNestedFactory = createMockFactory(expectedProducer);

        BiPredicate<MapperGenerationContext, FieldsMapperGenerationInput> secondPredicate = Mockito.mock(BiPredicate.class);
        Mockito.when(secondPredicate.test(Mockito.any(), Mockito.any())).thenReturn(false);

        ValueSupplierMethodProducerFactory<FieldsMapperGenerationInput> secondNestedFactory = createMockFactory(Mockito.mock(MethodProducer.class));

        factoriesMap.put(firstPredicate, firstNestedFactory);
        factoriesMap.put(secondPredicate, secondNestedFactory);

        FieldsMapMethodProducerFactory factory = new FieldsMapMethodProducerFactory(factoriesMap);

        Optional<MethodProducer> result = invokeCreateWithMockArguments(factory);

        Assert.assertEquals(Optional.of(expectedProducer), result);
    }

    @Test
    public void create_multiplePredicateGroupsMatch_delegateToFactoryOfFirstMatchingPredicateGroup() {
        BiPredicate<MapperGenerationContext, FieldsMapperGenerationInput> firstPredicate = Mockito.mock(BiPredicate.class);
        Mockito.when(firstPredicate.test(Mockito.any(), Mockito.any())).thenReturn(true);

        MethodProducer expectedProducer = Mockito.mock(MethodProducer.class);
        ValueSupplierMethodProducerFactory<FieldsMapperGenerationInput> firstNestedFactory = createMockFactory(expectedProducer);

        BiPredicate<MapperGenerationContext, FieldsMapperGenerationInput> secondPredicate = Mockito.mock(BiPredicate.class);
        Mockito.when(secondPredicate.test(Mockito.any(), Mockito.any())).thenReturn(true);

        ValueSupplierMethodProducerFactory<FieldsMapperGenerationInput> secondNestedFactory = createMockFactory(Mockito.mock(MethodProducer.class));


        factoriesMap.put(firstPredicate, firstNestedFactory);
        factoriesMap.put(secondPredicate, secondNestedFactory);

        FieldsMapMethodProducerFactory factory = new FieldsMapMethodProducerFactory(factoriesMap);

        Optional<MethodProducer> result = invokeCreateWithMockArguments(factory);

        Mockito.verify(firstNestedFactory, Mockito.times(1)).create(Mockito.any(), Mockito.any(), Mockito.any());
        Mockito.verify(secondNestedFactory, Mockito.never()).create(Mockito.any(), Mockito.any(), Mockito.any());
    }

    private ValueSupplierMethodProducerFactory<FieldsMapperGenerationInput> createMockFactory(MethodProducer returnedProducer) {
        ValueSupplierMethodProducerFactory<FieldsMapperGenerationInput> result = Mockito.mock(ValueSupplierMethodProducerFactory.class);
        Mockito.when(result.create(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(Optional.of(returnedProducer));
        return result;
    }

    private Optional<MethodProducer> invokeCreateWithMockArguments(FieldsMapMethodProducerFactory factory) {
        return factory.create(Mockito.mock(MapperGenerationContext.class),
                Mockito.mock(TypesMapperGenerationInput.class),
                Mockito.mock(FieldsMapperGenerationInput.class));
    }
}
