package app.transcriber.generation.methods.supplier;

import app.transcriber.generation.methods.MethodProducer;
import app.transcriber.generation.methods.supplier.map.TemplateFieldsMapMethodsGenerator;
import app.transcriber.generation.methods.supplier.map.factories.FieldsMapMethodProducerFactory;
import app.transcriber.generation.methods.supplier.produce.ValueProducerMethodProducer;
import app.transcriber.generation.methods.supplier.produce.ValueProducerMethodProducerFactory;
import app.transcriber.generation.methods.supplier.set.ConstantSetMethodProducer;
import app.transcriber.generation.methods.supplier.set.ConstantSetMethodProducerFactory;
import app.transcriber.generation.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class SupplierMethodGeneratorTest {

    private SupplierMethodGenerator supplierMethodGenerator;

    @Mock
    private ValueProducerMethodProducerFactory valueProducerMethodProducerFactory;

    @Mock
    private ConstantSetMethodProducerFactory constantSetMethodProducerFactory;

    @Mock
    private FieldsMapMethodProducerFactory fieldsMapMethodProducerFactory;

    @Mock
    private MapperGenerationContext mapperGenerationContext;

    @Mock
    private TypesMapperGenerationInput typesMapperGenerationInput;

    @Before
    public void setup() {
        supplierMethodGenerator = new SupplierMethodGenerator(valueProducerMethodProducerFactory,
                constantSetMethodProducerFactory, fieldsMapMethodProducerFactory);

        Mockito.when(typesMapperGenerationInput.getConstantMappingsGenerationInputs()).thenReturn(Collections.emptyList());
        Mockito.when(typesMapperGenerationInput.getFieldsMappersGenerationInputs()).thenReturn(Collections.emptyList());
        Mockito.when(typesMapperGenerationInput.getFieldsValueProducersGenerationInputs()).thenReturn(Collections.emptyList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void allArgsConstructor_onNullValueProducerMethodProducerFactory_throwsIllegalArgumentException() {
        new SupplierMethodGenerator(null, constantSetMethodProducerFactory, fieldsMapMethodProducerFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void allArgsConstructor_onNullConstantSetMethodProducerFactory_throwsIllegalArgumentException() {
        new SupplierMethodGenerator(valueProducerMethodProducerFactory, null, fieldsMapMethodProducerFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void allArgsConstructor_onNullFieldsMapMethodProducerFactory_throwsIllegalArgumentException() {
        new SupplierMethodGenerator(valueProducerMethodProducerFactory, constantSetMethodProducerFactory, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void generate_onNullMapperGenerationContext_throwsIllegalArgumentException() {
        supplierMethodGenerator.generate(null, typesMapperGenerationInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void generate_onNullTypesMapperGenerationInput_throwsIllegalArgumentException() {
        supplierMethodGenerator.generate(mapperGenerationContext, null);
    }

    @Test
    public void generate_whenReturningValueProducerMethodProducer_invokeProducerOnFieldsValueProducerMetadataElements() {
        FieldsValueProducerGenerationInput generationInput1 = Mockito.mock(FieldsValueProducerGenerationInput.class);
        FieldsValueProducerGenerationInput generationInput2 = Mockito.mock(FieldsValueProducerGenerationInput.class);
        List<FieldsValueProducerGenerationInput> fieldsValueProducerGenerationInputs = Arrays.asList(generationInput1, generationInput2);
        ValueProducerMethodProducer valueProducerMethodProducer1 = Mockito.mock(ValueProducerMethodProducer.class);
        ValueProducerMethodProducer valueProducerMethodProducer2 = Mockito.mock(ValueProducerMethodProducer.class);

        Mockito.when(typesMapperGenerationInput.getFieldsValueProducersGenerationInputs()).thenReturn(fieldsValueProducerGenerationInputs);
        Mockito.when(valueProducerMethodProducerFactory.create(Mockito.any(), Mockito.any(), Mockito.eq(generationInput1))).thenReturn(Optional.of(valueProducerMethodProducer1));
        Mockito.when(valueProducerMethodProducerFactory.create(Mockito.any(), Mockito.any(), Mockito.eq(generationInput2))).thenReturn(Optional.of(valueProducerMethodProducer2));

        supplierMethodGenerator.generate(mapperGenerationContext, typesMapperGenerationInput);

        Mockito.verify(valueProducerMethodProducer1, Mockito.times(1)).get();
        Mockito.verify(valueProducerMethodProducer2, Mockito.times(1)).get();
    }

    @Test
    public void generate_whenReturningConstantSetMethodProducer_invokeProduceOnConstantMappingMetadataElements() {
        ConstantMappingGenerationInput constantMappingGenerationInput1 = Mockito.mock(ConstantMappingGenerationInput.class);
        ConstantMappingGenerationInput constantMappingGenerationInput2 = Mockito.mock(ConstantMappingGenerationInput.class);
        List<ConstantMappingGenerationInput> constantMappingGenerationInputs = Arrays.asList(constantMappingGenerationInput1, constantMappingGenerationInput2);
        ConstantSetMethodProducer constantSetMethodProducer1 = Mockito.mock(ConstantSetMethodProducer.class);
        ConstantSetMethodProducer constantSetMethodProducer2 = Mockito.mock(ConstantSetMethodProducer.class);

        Mockito.when(typesMapperGenerationInput.getConstantMappingsGenerationInputs()).thenReturn(constantMappingGenerationInputs);
        Mockito.when(constantSetMethodProducerFactory.create(Mockito.any(), Mockito.any(), Mockito.eq(constantMappingGenerationInput1))).thenReturn(Optional.of(constantSetMethodProducer1));
        Mockito.when(constantSetMethodProducerFactory.create(Mockito.any(), Mockito.any(), Mockito.eq(constantMappingGenerationInput2))).thenReturn(Optional.of(constantSetMethodProducer2));

        supplierMethodGenerator.generate(mapperGenerationContext, typesMapperGenerationInput);

        Mockito.verify(constantSetMethodProducer1, Mockito.times(1)).get();
        Mockito.verify(constantSetMethodProducer2, Mockito.times(1)).get();
    }

    @Test
    public void generate_whenReturningFieldsMapMethodProducer_invokeProduceOnFieldsMapperMetadataElements() {
        FieldsMapperGenerationInput fieldsMapperGenerationInput1 = Mockito.mock(FieldsMapperGenerationInput.class);
        FieldsMapperGenerationInput fieldsMapperGenerationInput2 = Mockito.mock(FieldsMapperGenerationInput.class);
        List<FieldsMapperGenerationInput> fieldsMapperGenerationInputs = Arrays.asList(fieldsMapperGenerationInput1, fieldsMapperGenerationInput2);
        MethodProducer methodProducer1 = Mockito.mock(TemplateFieldsMapMethodsGenerator.class);
        MethodProducer methodProducer2 = Mockito.mock(TemplateFieldsMapMethodsGenerator.class);

        Mockito.when(typesMapperGenerationInput.getFieldsMappersGenerationInputs()).thenReturn(fieldsMapperGenerationInputs);
        Mockito.when(fieldsMapMethodProducerFactory.create(Mockito.any(), Mockito.any(), Mockito.eq(fieldsMapperGenerationInput1))).thenReturn(Optional.of(methodProducer1));
        Mockito.when(fieldsMapMethodProducerFactory.create(Mockito.any(), Mockito.any(), Mockito.eq(fieldsMapperGenerationInput2))).thenReturn(Optional.of(methodProducer2));

        supplierMethodGenerator.generate(mapperGenerationContext, typesMapperGenerationInput);

        Mockito.verify(methodProducer1, Mockito.times(1)).get();
        Mockito.verify(methodProducer2, Mockito.times(1)).get();
    }

}