package app.transcriber.generation.conventions;

import app.transcriber.generation.model.adapters.FieldAdapter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;

public class GetterMethodNameConventionTest {

    private GetterMethodNameConvention getterMethodNameConvention;

    @Before
    public void setup() {
        getterMethodNameConvention = new GetterMethodNameConvention();
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onNullField_throwsIllegalArgumentException() {
        getterMethodNameConvention.apply(null);
    }

    @Test
    public void apply_onFieldWithNullName_returnsGet() {
        assertEquals("getnull", getterMethodNameConvention.apply(Mockito.mock(FieldAdapter.class)));
    }

    @Test
    public void apply_onFieldWithEmptyName_returnsGet() {
        FieldAdapter field = Mockito.mock(FieldAdapter.class);
        Mockito.when(field.getName()).thenReturn("");

        assertEquals("get", getterMethodNameConvention.apply(field));
    }

    @Test
    public void apply_onFieldWithName_returnsCapitalizedFieldNameWithPrefix() {
        FieldAdapter mockFieldAdapter = Mockito.mock(FieldAdapter.class);

        Mockito.when(mockFieldAdapter.getName()).thenReturn("fieldName");

        String result = getterMethodNameConvention.apply(mockFieldAdapter);

        assertEquals("getFieldName", result);
    }

}