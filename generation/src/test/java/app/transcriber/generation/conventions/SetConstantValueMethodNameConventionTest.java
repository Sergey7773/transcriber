package app.transcriber.generation.conventions;

import app.transcriber.generation.model.ConstantMappingGenerationInput;
import app.transcriber.generation.model.adapters.FieldAdapter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class SetConstantValueMethodNameConventionTest {

    private SetConstantValueMethodNameConvention constantValueMethodNameConvention;

    @Mock
    private ConstantMappingGenerationInput constantMappingGenerationInput;

    @Mock
    private FieldAdapter fieldAdapter;

    @Before
    public void setup() {
        constantValueMethodNameConvention = new SetConstantValueMethodNameConvention();

        Mockito.when(constantMappingGenerationInput.getTargetField()).thenReturn(fieldAdapter);
        Mockito.when(fieldAdapter.getName()).thenReturn("targetField");
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onNullConstantMappingGenerationInput_throwsIllegalArgumentException() {
        constantValueMethodNameConvention.apply(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onConstantMappingGenerationInputWithNullTargetField_throwsIllegalArgumentException() {
        Mockito.when(constantMappingGenerationInput.getTargetField()).thenReturn(null);
        constantValueMethodNameConvention.apply(constantMappingGenerationInput);
    }

    @Test
    public void apply_onConstantMappingGenerationInputWithNullTargetFieldName_returnsMethodName() {
        Mockito.when(fieldAdapter.getName()).thenReturn(null);
        assertEquals("setnullConstantValue", constantValueMethodNameConvention.apply(constantMappingGenerationInput));
    }

    @Test
    public void apply_onConstantMappingGenerationInputWithEmptyTargetFieldName_returnsMethodName() {
        Mockito.when(fieldAdapter.getName()).thenReturn("");
        assertEquals("setConstantValue", constantValueMethodNameConvention.apply(constantMappingGenerationInput));
    }

    @Test
    public void apply_onConstantMappingGenerationInputWithNonEmptyTargetFieldName_returnsMethodName() {
        assertEquals("setTargetFieldConstantValue", constantValueMethodNameConvention.apply(constantMappingGenerationInput));
    }
}