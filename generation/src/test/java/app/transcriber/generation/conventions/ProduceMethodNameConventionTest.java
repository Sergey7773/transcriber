package app.transcriber.generation.conventions;

import app.transcriber.generation.model.FieldsValueProducerGenerationInput;
import app.transcriber.generation.model.adapters.FieldAdapter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ProduceMethodNameConventionTest {

    private ProduceMethodNameConvention produceMethodNameConvention;

    @Mock
    private FieldsValueProducerGenerationInput fieldsValueProducerGenerationInput;

    @Mock
    private FieldAdapter fieldAdapter;

    @Before
    public void setup() {
        produceMethodNameConvention = new ProduceMethodNameConvention();

        Mockito.when(fieldsValueProducerGenerationInput.getTargetField()).thenReturn(fieldAdapter);
        Mockito.when(fieldAdapter.getName()).thenReturn("targetField");
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onNullFieldsValueProducerGenerationInput_throwsIllegalArgumentException() {
        produceMethodNameConvention.apply(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onFieldsValueProducerGenerationInputWithNullTargetField_throwsIllegalArgumentException() {
        Mockito.when(fieldsValueProducerGenerationInput.getTargetField()).thenReturn(null);
        produceMethodNameConvention.apply(fieldsValueProducerGenerationInput);
    }

    @Test
    public void apply_onFieldsValueProducerGenerationInputWithNullTargetFieldName_returnsMethodName() {
        Mockito.when(fieldAdapter.getName()).thenReturn(null);
        assertEquals("producenullValue", produceMethodNameConvention.apply(fieldsValueProducerGenerationInput));
    }

    @Test
    public void apply_onFieldsValueProducerGenerationInputWithEmptyTargetFieldName_returnsMethodName() {
        Mockito.when(fieldAdapter.getName()).thenReturn("");
        assertEquals("produceValue", produceMethodNameConvention.apply(fieldsValueProducerGenerationInput));
    }

    @Test
    public void apply_onFieldsValueProducerGenerationInputWithNonEmptyTargetFieldName_returnsMethodName() {
        assertEquals("produceTargetFieldValue", produceMethodNameConvention.apply(fieldsValueProducerGenerationInput));
    }
}