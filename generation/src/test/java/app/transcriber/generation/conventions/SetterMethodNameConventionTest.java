package app.transcriber.generation.conventions;

import app.transcriber.generation.model.adapters.FieldAdapter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class SetterMethodNameConventionTest {

    private SetterMethodNameConvention setterMethodNameConvention;

    @Mock
    private FieldAdapter fieldAdapter;

    @Before
    public void setup() {
        setterMethodNameConvention = new SetterMethodNameConvention();
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onNullField_throwsIllegalArgumentException() {
        setterMethodNameConvention.apply(null);
    }

    @Test
    public void apply_onFieldWithNullName_returnsSetterMethodName() {
        assertEquals("setnull", setterMethodNameConvention.apply(fieldAdapter));
    }

    @Test
    public void apply_onFieldWithEmptyName_returnsSetterMethodName() {
        Mockito.when(fieldAdapter.getName()).thenReturn("");
        assertEquals("set", setterMethodNameConvention.apply(fieldAdapter));
    }

    @Test
    public void apply_onFieldWithNonEmptyName_returnsSetterMethodName() {
        Mockito.when(fieldAdapter.getName()).thenReturn("fieldName");
        assertEquals("setFieldName", setterMethodNameConvention.apply(fieldAdapter));
    }

}