package app.transcriber.generation.conventions;

import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.adapters.FieldAdapter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MapMethodNameConventionTest {

    private MapMethodNameConvention mapMethodNameConvention;

    @Before
    public void setup() {
        mapMethodNameConvention = new MapMethodNameConvention();
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onNullFieldsMapperGenerationInput_throwsIllegalArgumentException() {
        mapMethodNameConvention.apply(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onFieldMapperGenerationInputWithNullSourceFields_throwsIllegalArgumentException() {
        FieldsMapperGenerationInput fieldsMapperGenerationInput = Mockito.mock(FieldsMapperGenerationInput.class);
        mapMethodNameConvention.apply(fieldsMapperGenerationInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onFieldMapperGenerationInputWithNullTargetField_throwsIllegalArgumentException() {
        FieldsMapperGenerationInput fieldsMapperGenerationInput = Mockito.mock(FieldsMapperGenerationInput.class);
        FieldAdapter fieldAdapter = Mockito.mock(FieldAdapter.class);
        List<FieldAdapter> fieldAdapters = Arrays.asList(fieldAdapter);

        Mockito.when(fieldAdapter.getName()).thenReturn("fieldName");
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(fieldAdapters);

        mapMethodNameConvention.apply(fieldsMapperGenerationInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onFieldMapperGenerationInputWithEmptySourceFields_throwsIllegalArgumentException() {
        FieldsMapperGenerationInput fieldsMapperGenerationInput = Mockito.mock(FieldsMapperGenerationInput.class);
        FieldAdapter fieldAdapter = Mockito.mock(FieldAdapter.class);

        Mockito.when(fieldAdapter.getName()).thenReturn("fieldName");
        Mockito.when(fieldsMapperGenerationInput.getTargetField()).thenReturn(fieldAdapter);
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(Collections.emptyList());

        mapMethodNameConvention.apply(fieldsMapperGenerationInput);
    }

    @Test
    public void apply_onEmptySourceFieldName_returnsMapperNameWithoutSourceFieldName() {
        FieldsMapperGenerationInput fieldsMapperGenerationInput = Mockito.mock(FieldsMapperGenerationInput.class);
        FieldAdapter sourceFieldAdapter = Mockito.mock(FieldAdapter.class);
        FieldAdapter targetFieldAdapter = Mockito.mock(FieldAdapter.class);

        Mockito.when(sourceFieldAdapter.getName()).thenReturn("");
        Mockito.when(targetFieldAdapter.getName()).thenReturn("targetFieldName");
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(Collections.singletonList(sourceFieldAdapter));
        Mockito.when(fieldsMapperGenerationInput.getTargetField()).thenReturn(targetFieldAdapter);

        assertEquals("mapToTargetFieldName", mapMethodNameConvention.apply(fieldsMapperGenerationInput));
    }

    @Test
    public void apply_onNullSourceFieldName_returnsMapperNameWithoutSourceFieldName() {
        FieldsMapperGenerationInput fieldsMapperGenerationInput = Mockito.mock(FieldsMapperGenerationInput.class);
        FieldAdapter sourceFieldAdapter = Mockito.mock(FieldAdapter.class);
        FieldAdapter targetFieldAdapter = Mockito.mock(FieldAdapter.class);

        Mockito.when(targetFieldAdapter.getName()).thenReturn("targetFieldName");
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(Collections.singletonList(sourceFieldAdapter));
        Mockito.when(fieldsMapperGenerationInput.getTargetField()).thenReturn(targetFieldAdapter);

        assertEquals("mapnullToTargetFieldName", mapMethodNameConvention.apply(fieldsMapperGenerationInput));
    }

    @Test
    public void apply_onEmptyTargetFieldName_returnMapperNameWithoutTargetFieldName() {
        FieldsMapperGenerationInput fieldsMapperGenerationInput = Mockito.mock(FieldsMapperGenerationInput.class);
        FieldAdapter sourceFieldAdapter = Mockito.mock(FieldAdapter.class);
        FieldAdapter targetFieldAdapter = Mockito.mock(FieldAdapter.class);

        Mockito.when(sourceFieldAdapter.getName()).thenReturn("sourceFieldName");
        Mockito.when(targetFieldAdapter.getName()).thenReturn("");
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(Collections.singletonList(sourceFieldAdapter));
        Mockito.when(fieldsMapperGenerationInput.getTargetField()).thenReturn(targetFieldAdapter);

        assertEquals("mapSourceFieldNameTo", mapMethodNameConvention.apply(fieldsMapperGenerationInput));
    }

    @Test
    public void apply_onNullTargetFieldName_returnsMapperNameWithoutTargetFieldName() {
        FieldsMapperGenerationInput fieldsMapperGenerationInput = Mockito.mock(FieldsMapperGenerationInput.class);
        FieldAdapter sourceFieldAdapter = Mockito.mock(FieldAdapter.class);
        FieldAdapter targetFieldAdapter = Mockito.mock(FieldAdapter.class);

        Mockito.when(sourceFieldAdapter.getName()).thenReturn("sourceFieldName");
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(Collections.singletonList(sourceFieldAdapter));
        Mockito.when(fieldsMapperGenerationInput.getTargetField()).thenReturn(targetFieldAdapter);

        assertEquals("mapSourceFieldNameTonull", mapMethodNameConvention.apply(fieldsMapperGenerationInput));
    }

    @Test
    public void apply_onFieldGenerationInputWithSingleSourceField_returnExpectedMapMethodName() {
        FieldsMapperGenerationInput fieldsMapperGenerationInput = Mockito.mock(FieldsMapperGenerationInput.class);
        FieldAdapter sourceFieldAdapter = Mockito.mock(FieldAdapter.class);
        FieldAdapter targetFieldAdapter = Mockito.mock(FieldAdapter.class);

        Mockito.when(sourceFieldAdapter.getName()).thenReturn("sourceFieldName");
        Mockito.when(targetFieldAdapter.getName()).thenReturn("targetFieldName");
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(Collections.singletonList(sourceFieldAdapter));
        Mockito.when(fieldsMapperGenerationInput.getTargetField()).thenReturn(targetFieldAdapter);

        String result = mapMethodNameConvention.apply(fieldsMapperGenerationInput);

        assertEquals("mapSourceFieldNameToTargetFieldName", result);
    }

    @Test
    public void apply_onFieldGenerationInputWithMultipleSourceFields_returnExpectedMapMethodName() {
        FieldsMapperGenerationInput fieldsMapperGenerationInput = Mockito.mock(FieldsMapperGenerationInput.class);
        FieldAdapter sourceFieldAdapter1 = Mockito.mock(FieldAdapter.class);
        FieldAdapter sourceFieldAdapter2 = Mockito.mock(FieldAdapter.class);
        FieldAdapter targetFieldAdapter = Mockito.mock(FieldAdapter.class);

        Mockito.when(sourceFieldAdapter1.getName()).thenReturn("sourceFieldName1");
        Mockito.when(sourceFieldAdapter2.getName()).thenReturn("sourceFieldName2");
        Mockito.when(targetFieldAdapter.getName()).thenReturn("targetFieldName");
        Mockito.when(fieldsMapperGenerationInput.getTargetField()).thenReturn(targetFieldAdapter);
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(Arrays.asList(sourceFieldAdapter1, sourceFieldAdapter2));
        Mockito.when(fieldsMapperGenerationInput.getTargetField()).thenReturn(targetFieldAdapter);

        String result = mapMethodNameConvention.apply(fieldsMapperGenerationInput);

        assertEquals("mapSourceFieldName2ToTargetFieldName", result);
    }


}