package app.transcriber.generation.conventions;

import app.transcriber.generation.model.TypesMapperGenerationInput;
import app.transcriber.generation.model.adapters.ClassAdapter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;


@RunWith(MockitoJUnitRunner.class)
public class MapperClassNameConventionTest {

    private MapperClassNameConvention mapperClassNameConvention;

    @Mock
    private TypesMapperGenerationInput typesMapperGenerationInput;

    @Mock
    private ClassAdapter sourceClassAdapter;

    @Mock
    private ClassAdapter targetClassAdapter;

    @Before
    public void setup() {
        mapperClassNameConvention = new MapperClassNameConvention();

        Mockito.when(typesMapperGenerationInput.getSourceClass()).thenReturn(sourceClassAdapter);
        Mockito.when(typesMapperGenerationInput.getTargetClass()).thenReturn(targetClassAdapter);

        Mockito.when(sourceClassAdapter.getSimpleName()).thenReturn("SourceClassName");
        Mockito.when(targetClassAdapter.getSimpleName()).thenReturn("TargetClassName");
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onNullTypesMapperGenerationInput_throwsIllegalArgumentException() {
        mapperClassNameConvention.apply(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onTypesMapperGenerationInputWithNullSourceClass_throwsIllegalArgumentException() {
        Mockito.when(typesMapperGenerationInput.getSourceClass()).thenReturn(null);

        mapperClassNameConvention.apply(typesMapperGenerationInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onTypesMapperGenerationInputWithNullTargetClass_throwsIllegalArgumentException() {
        Mockito.when(typesMapperGenerationInput.getTargetClass()).thenReturn(null);

        mapperClassNameConvention.apply(typesMapperGenerationInput);
    }

    @Test
    public void apply_onTypesMapperGenerationInputWthNullSourceClassName_returnMapperName() {
        Mockito.when(sourceClassAdapter.getSimpleName()).thenReturn(null);

        assertEquals("nullToTargetClassNameMapper", mapperClassNameConvention.apply(typesMapperGenerationInput));
    }

    @Test
    public void apply_onTypesMapperGenerationInputWithNullTargetClassName_returnMapperName() {
        Mockito.when(targetClassAdapter.getSimpleName()).thenReturn(null);

        assertEquals("SourceClassNameTonullMapper", mapperClassNameConvention.apply(typesMapperGenerationInput));
    }

    @Test
    public void apply_onTypesMapperGenerationInputWthEmptySourceClassName_returnMapperName() {
        Mockito.when(sourceClassAdapter.getSimpleName()).thenReturn("");

        assertEquals("ToTargetClassNameMapper", mapperClassNameConvention.apply(typesMapperGenerationInput));
    }

    @Test
    public void apply_onTypesMapperGenerationInputWithEmptyTargetClassName_returnMapperName() {
        Mockito.when(targetClassAdapter.getSimpleName()).thenReturn("");

        assertEquals("SourceClassNameToMapper", mapperClassNameConvention.apply(typesMapperGenerationInput));
    }

    @Test
    public void apply_onTypesMapperGenerationInputWithClassNames_returnMapperName() {
        assertEquals("SourceClassNameToTargetClassNameMapper", mapperClassNameConvention.apply(typesMapperGenerationInput));
    }


}