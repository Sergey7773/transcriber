package app.transcriber.generation.conventions;

import app.transcriber.generation.model.FieldsMapperGenerationInput;
import app.transcriber.generation.model.adapters.FieldAdapter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class SubEntityMapperVariableNameConventionTest {

    private SubEntityMapperVariableNameConvention subEntityMapperVariableNameConvention;

    @Mock
    private FieldsMapperGenerationInput fieldsMapperGenerationInput;

    @Before
    public void setup() {
        subEntityMapperVariableNameConvention = new SubEntityMapperVariableNameConvention();
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onNullFieldsMapperGenerationInput_throwsIllegalArgumentException() {
        subEntityMapperVariableNameConvention.apply(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onFieldsMapperGenerationInputWithNullTargetField_throwsIllegalArgumentException() {
        List<FieldAdapter> sourceFieldAdapters = setupFieldAdapters("f1", "f2");
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(sourceFieldAdapters);

        subEntityMapperVariableNameConvention.apply(fieldsMapperGenerationInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onFieldsMapperGenerationInputWithNullSourceFields_throwsIllegalArgumentException() {
        subEntityMapperVariableNameConvention.apply(fieldsMapperGenerationInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_onFieldsMapperGenerationInputWithEmptySourceFields_throwsIllegalArgumentException() {
        List<FieldAdapter> sourceFieldAdapters = setupFieldAdapters();
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(sourceFieldAdapters);

        subEntityMapperVariableNameConvention.apply(fieldsMapperGenerationInput);
    }

    @Test
    public void apply_onEmptyTargetFieldName_returnsMapperName() {
        List<FieldAdapter> sourceFieldAdapters = setupFieldAdapters("sourceField1", "sourceField2");
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(sourceFieldAdapters);
        FieldAdapter targetFieldAdapter = setupFieldAdapter("");
        Mockito.when(fieldsMapperGenerationInput.getTargetField()).thenReturn(targetFieldAdapter);

        assertEquals("sourceField2ToMapper", subEntityMapperVariableNameConvention.apply(fieldsMapperGenerationInput));
    }

    @Test
    public void apply_onNullTargetFieldName_returnsMapperName() {
        List<FieldAdapter> sourceFieldAdapters = setupFieldAdapters("sourceField1", "sourceField2");
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(sourceFieldAdapters);
        FieldAdapter targetFieldAdapter = setupFieldAdapter(null);
        Mockito.when(fieldsMapperGenerationInput.getTargetField()).thenReturn(targetFieldAdapter);

        assertEquals("sourceField2TonullMapper", subEntityMapperVariableNameConvention.apply(fieldsMapperGenerationInput));
    }

    @Test
    public void apply_onEmptySourceFieldName_returnsMapperName() {
        List<FieldAdapter> sourceFieldAdapters = setupFieldAdapters("sourceField1", "");
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(sourceFieldAdapters);
        FieldAdapter targetFieldAdapter = setupFieldAdapter("targetField");
        Mockito.when(fieldsMapperGenerationInput.getTargetField()).thenReturn(targetFieldAdapter);

        assertEquals("ToTargetFieldMapper", subEntityMapperVariableNameConvention.apply(fieldsMapperGenerationInput));
    }

    @Test
    public void apply_onNullSourceFieldName_returnsMapperName() {
        List<FieldAdapter> sourceFieldAdapters = setupFieldAdapters("sourceField1", null);
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(sourceFieldAdapters);
        FieldAdapter targetFieldAdapter = setupFieldAdapter("targetField");
        Mockito.when(fieldsMapperGenerationInput.getTargetField()).thenReturn(targetFieldAdapter);

        assertEquals("nullToTargetFieldMapper", subEntityMapperVariableNameConvention.apply(fieldsMapperGenerationInput));
    }

    @Test
    public void apply_onNonNullFieldNames_returnsMapperName() {
        List<FieldAdapter> sourceFieldAdapters = setupFieldAdapters("sourceField1", "sourceField2");
        Mockito.when(fieldsMapperGenerationInput.getSourceFields()).thenReturn(sourceFieldAdapters);
        FieldAdapter targetFieldAdapter = setupFieldAdapter("targetField");
        Mockito.when(fieldsMapperGenerationInput.getTargetField()).thenReturn(targetFieldAdapter);

        assertEquals("sourceField2ToTargetFieldMapper", subEntityMapperVariableNameConvention.apply(fieldsMapperGenerationInput));
    }

    private List<FieldAdapter> setupFieldAdapters(String... fieldNames) {
        List<FieldAdapter> result = new ArrayList<>(fieldNames.length);

        for (String fieldName : fieldNames) {
            result.add(setupFieldAdapter(fieldName));
        }

        return result;
    }

    private FieldAdapter setupFieldAdapter(String fieldName) {
        FieldAdapter result = Mockito.mock(FieldAdapter.class);
        if (fieldName != null) {
            Mockito.when(result.getName()).thenReturn(fieldName);
        }
        return result;
    }
}