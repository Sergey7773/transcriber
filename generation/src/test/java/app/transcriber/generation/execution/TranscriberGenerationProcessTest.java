package app.transcriber.generation.execution;

import app.transcriber.api.exceptions.GenerationInputVerificationException;
import app.transcriber.api.model.GenerationProcessPackages;
import app.transcriber.api.model.TypesMapperDefinition;
import app.transcriber.generation.creation.MapperGenerationContextFactory;
import app.transcriber.generation.mappers.MapperGenerator;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.TypeSpec;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class TranscriberGenerationProcessTest {

    private TranscriberGenerationProcess transcriberGenerationProcess;

    @Mock
    private MapperGenerationContextFactory mapperGenerationContextFactory;

    @Mock
    private MapperGenerator mapperGenerator;

    @Mock
    private GenerationProcessPackages generationProcessPackages;

    @Before
    public void setup() {
        transcriberGenerationProcess = TranscriberGenerationProcess.builder()
                .mapperGenerator(mapperGenerator)
                .mapperGenerationContextFactory(mapperGenerationContextFactory)
                .generationProcessPackages(generationProcessPackages)
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void build_onNullMapperGenerator_throwsIllegalArgumentException() {
        Field f = Mockito.mock(Field.class);

        TranscriberGenerationProcess.builder()
                .mapperGenerator(null)
                .mapperGenerationContextFactory(mapperGenerationContextFactory)
                .generationProcessPackages(generationProcessPackages)
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void build_onNullMappingMetadataCreator_throwsIllegalArgumentException() {
        TranscriberGenerationProcess.builder()
                .mapperGenerator(mapperGenerator)
                .mapperGenerationContextFactory(null)
                .generationProcessPackages(generationProcessPackages)
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void build_onNullGenerationPackages_throwsIllegalArgumentException() {
        TranscriberGenerationProcess.builder()
                .mapperGenerator(mapperGenerator)
                .mapperGenerationContextFactory(mapperGenerationContextFactory)
                .generationProcessPackages(null)
                .build();
    }

    @Test
    public void executeGeneration_onNullTypesMapperDefinitions_returnEmptyCollection() throws GenerationInputVerificationException {
        Collection<JavaFile> result = transcriberGenerationProcess.executeGeneration(null);

        assertEquals(Collections.emptyList(), result);
    }

    @Test
    public void executeGeneration_onEmptyTypesMapperDefinitions_returnEmptyCollection() throws GenerationInputVerificationException {
        Collection<JavaFile> result = transcriberGenerationProcess.executeGeneration(Collections.emptyList());

        assertEquals(Collections.emptyList(), result);
    }

    @Test
    public void executeGeneration_onNonEmptyTypesMapperDefinitions_createContextUsingGivenDefinitions() throws GenerationInputVerificationException {
        Collection<TypesMapperDefinition> typesMapperDefinitions = Arrays.asList(Mockito.mock(TypesMapperDefinition.class));
        transcriberGenerationProcess.executeGeneration(typesMapperDefinitions);

        Mockito.verify(mapperGenerationContextFactory, Mockito.times(1))
                .create(Mockito.eq(typesMapperDefinitions), Mockito.any());
    }

    @Test
    public void executeGeneration_onNonEmptyTypesMapperDefinitions_createContextUsingGivenPackages() throws GenerationInputVerificationException {
        Collection<TypesMapperDefinition> typesMapperDefinitions = Arrays.asList(Mockito.mock(TypesMapperDefinition.class));
        transcriberGenerationProcess.executeGeneration(typesMapperDefinitions);

        Mockito.verify(mapperGenerationContextFactory, Mockito.times(1))
                .create(Mockito.eq(typesMapperDefinitions), Mockito.any());
    }

    @Test
    public void executeGeneration_onNonEmptyTypesMappersDefinitions_returnResultOfMapperGenerator() throws GenerationInputVerificationException {
        JavaFile javaFile = JavaFile.builder("package", TypeSpec.classBuilder("clz").build()).build();
        List<JavaFile> expectedResult = Arrays.asList(javaFile);
        Mockito.when(mapperGenerator.generate(Mockito.any()))
                .thenReturn(expectedResult);

        Collection<TypesMapperDefinition> typesMapperDefinitions = Arrays.asList(Mockito.mock(TypesMapperDefinition.class));
        Collection<JavaFile> result = transcriberGenerationProcess.executeGeneration(typesMapperDefinitions);

        assertEquals(expectedResult, result);
    }

}